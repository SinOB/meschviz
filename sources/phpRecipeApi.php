<?php

require realpath(dirname(__FILE__).'/../').'/vendor/autoload.php';
require_once realpath(dirname(__FILE__).'/../').'/application/core/Config.php';


if ( !class_exists('phpRecipeApi') ) {
	if (session_id() == "") {
		@session_start();
	}

	class phpRecipeApi {

		public static function  CallAPI($url, $data = false) {

			$email = Config::get ( 'RECIPE_PORTLET_EMAIL' );
			$pwd =Config::get ( 'RECIPE_PORTLET_SECRET' );

			$curl = curl_init();

			// perform a post
			curl_setopt($curl, CURLOPT_POST, 1);

			if ($data){
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			}

			// Basic Authentication:
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $email.':'.$pwd);
			curl_setopt($curl, CURLOPT_FAILONERROR, true);
			//curl_setopt($curl, CURLOPT_VERBOSE, 1);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($curl);


			if(curl_errno($curl))
			{
				echo 'CURL error:' . curl_error($curl)."\n";
			}

			curl_close($curl);

			return $result;
		}

		/**
		 * Test to make sure the authoring service is available
		 */
		public static function test()
		{
			$url = "http://meschapi.suggesto.eu/api/jsonws/AuthoringService-portlet.mesch/test";
			$result = phpRecipeApi::CallAPI($url, array());
			return $result;
		}

		/**
		 * Search for all recipes for a specified organisation id
		 * @param unknown $organisationId
		 * @return unknown
		 */
		public static function search($organisationId)
		{
			// Search
			$url = "http://meschapi.suggesto.eu/api/jsonws/AuthoringService-portlet.mesch/search";
			$data_search = array("collectionName"=>"recipes",
					"filter"=>"{'organizationID': '".$organisationId."'}",
					"output"=>""
			);

			$result = phpRecipeApi::CallAPI($url, $data_search);

			// for some reason have to decode response twice
			$parsed = json_decode(json_decode($result, true),true);

			$error = json_last_error();

			if($error == 0){
				return $parsed;
			}
			else {
				echo 'Error parsing response: '.$error;
				die;
			}
		}

		public static function getOrganizations()
		{
			$url = "http://meschapi.suggesto.eu/api/jsonws/organization/get-organizations";

			$companyId=10157; //(meSch company ID)
			$parentOrganizationId=0; //(no parend organization)

			$data_search = array('companyId'=>$companyId,
                    'parentOrganizationId'=>$parentOrganizationId);
			$result = phpRecipeApi::CallAPI($url, $data_search);

			$parsed = json_decode($result,true);

			$error = json_last_error();

			if($error == 0){
				return $parsed;
			}
			else {
				echo 'Error parsing response: '.$error;
				die;
			}
		}



		public static function updateSystem()
		{
			echo "UPDATING RECIPES ON ".date('Y-m-d H:i:s')."\n";
			// process for each organisation
			$organisation_data = phpRecipeApi::getOrganizations();

			$codes = array();
			if(isset($organisation_data)){
				foreach($organisation_data as $museum){

					$museum_organisation_id = $museum['organizationId'];
					$museum_title = $museum['name'];

					echo "Processing Museum ".$museum_title."------------------------\n";

					// create or update the museum
					$museum_id = MuseumModel::createUpdateMuseum($museum_organisation_id, $museum_title);

					if(!isset($museum_id)|| $museum_id=='')
					{
						echo "\tERROR updating/creating museum ".$museum_organisation_id.". Skipping this recipe.\n";
						continue;
					}

					// get the recipe
					$response = phpRecipeApi::search($museum_organisation_id);

					// foreach exhibition recipe
					foreach( $response as $recipe ){

						// only import recipes with visibility set to public
						if($recipe['visibility'] !='public'){
							// otherwise delete from the database
							ExhibitionModel::deleteExhibitionByRecipeId($recipe["_id"]['$oid']);
							continue;
						}

						// get recipe last updated date/time
						$recipe_last_updated = $recipe["lastupdate"]."\n";

						// Get the existing record for the echibition - need this for the last stored recipe updated date
						$existing_recipe=ExhibitionModel::getExhibitionFromReipeId($recipe["_id"]['$oid']);

						if(!isset($recipe["exhibitID"]) || $recipe["exhibitID"]=='')
						{
							echo "\tERROR!!!!! unable to process recipie".$recipe["_id"]['$oid'].". No exhibit ID set. Skipping this exhibition\n";
							continue;
						}

						// 4 didit exhibition id used with passcodes
						$exhibition_short_code = $recipe["exhibitID"];

						$codes[ltrim($exhibition_short_code, '0')] = $recipe["_id"]['$oid'];


						// If recipe from authoring tool has same update date as local exhibition last updated date - dont bother updating
						if(isset($existing_recipe) && isset($existing_recipe->recipe_last_updated) && strtotime($recipe_last_updated) <= strtotime($existing_recipe->recipe_last_updated))
						{
							echo "\tSkipping recipie".$recipe["_id"]['$oid'].".(".$exhibition_short_code.") Already exists in the database\n";
							continue;
						}

						echo "\tProcessing recipe ".$recipe["_id"]['$oid']." exhibition code ".$exhibition_short_code." with title '".$recipe["title"]."'\n";

						// test if the recipe has content and that all contents contain a Target_Audience
						if(count($recipe["content"])<=0){
							echo "\tINVALID RECIPE - NO CONTENT PRESENT. ".$recipe["_id"]['$oid'].". DELETING THIS EXHIBITION.\n";
							ExhibitionModel::deleteExhibitionByRecipeId($recipe["_id"]['$oid']);
							continue;
						}

						// test that all content entries contain a Target Audience;
						$stop=0;
						foreach($recipe["content"] as $content)
						{

							if(!isset($content['categories']['Target audience']))
							{
								$stop=1;
							}
						}
						// TODO - once have a valid flag on recipe to tell if public and 'valid' - delete invalid recipies _INCLUDING EXHIBITION
						if($stop==1){
							echo "\t!!!!INVALID RECIPE - NO Target audience set on content. exhibition code:".$exhibition_short_code.". DELETING THIS EXHIBITION.\n";
							// delete the exhibition and all related content
							ExhibitionModel::deleteExhibitionByRecipeId($recipe["_id"]['$oid']);
							continue;
						}


						// get the exhibition image if available
						$exhibition_image='';
						if(isset($recipe["image"]) && count($recipe["image"])>0)
						{
							$exhibition_image=$recipe["image"];
						}

						$exhibition_url = null;
						if(isset($recipe['exhibitionurl']) && count($recipe['exhibitionurl'])>0) {
							$exhibition_url = $recipe['exhibitionurl'];
						}

						// create or update the exhibition
						$exhibition_id = ExhibitionModel::createUpdateExhibition(
								$recipe["_id"]['$oid'],
								$recipe["title"],
								isset($recipe["exhibitiondescription"])?$recipe["exhibitiondescription"]:'',
								$museum_id,
								$exhibition_short_code,
								$exhibition_image,
								$exhibition_url);

						if(!isset($exhibition_id)|| $exhibition_id=='')
						{
							echo "\tError updating/creating exhibition ".$recipe["_id"]['$oid'].". Skipping this exhibition.\n";
							continue;
						}

						$keep_contents = array();
						foreach($recipe["content"] as $content)
						{
							// Keep array of added/updated perspectives
							$keep_contents[] = $content['uidid'];

							//the url is either url cover, or if not set is url
							$url = isset($content['url'])?$content['url']:'';
							$type = $content['type'];

							// if we have a video - we don't want it - we only want the snapshot if its available
							// oherwise clear these fields
							if($content['type']=='video'){
								$url = isset($content['urlcover'])?$content['urlcover']:'';
								$type = isset($content['urlcover'])?'snapshot':''; // as in video still
							}

							// only if not already in existance
							// create content in new content table
							// As mapping between points and perspectives may vary we need to have some
							//way to track what points are used by what perspectives
							ContentModel::createUpdate(
									$content['uidid'],
									$exhibition_id,
									$content['categories']['Target audience'][0],
									isset($content['categories']['Languages'])?$content['categories']['Languages'][0]:'',
									isset($content['categories']['Themes'])?$content['categories']['Themes'][0]:'',
									isset($content['categories']['Point of interest'])?$content['categories']['Point of interest'][0]:'',
									$type,
									$content['title'],
									$url,
									isset($content['transcript'])?$content['transcript']:''
									);
						}
						ContentModel::deleteContentsForExhibition($exhibition_id, $keep_contents);


						// create or update the points of interest
						// TODO this should really be a point of interest id, with a string for each available language....
						$keep_points_of_interest = array();
						foreach ($recipe["classifications"]["Point of interest"] as $poi){
							// Keep array of added/updated points
							$keep_points_of_interest[] = $poi;
							POIModel::createUpdatePOI($poi, $exhibition_id);
						}
						// Delete all points that were not included in most recent version of recipe
						POIModel::deletePOIsForExhibition($exhibition_id, $keep_points_of_interest);

						// create or update the languages
						$keep_languages = array();
						foreach($recipe["classifications"]["Languages"] as $language)
						{
							// Keep array of added/updated languages
							$keep_languages[] = $language;
							LanguageModel::createUpdateLanguage($language, $exhibition_id);
						}
						// Delete all languages that were not included in most recent version of recipe
						LanguageModel::deleteLanguagesForExhibition($exhibition_id, $keep_languages);


						//TODO - unable to get perspective strings and langauges in different languages - may be an issue???
						// create or update the perspectives
						$keep_perspectives = array();
						foreach($recipe["classifications"]["Themes"] as $perspective)
						{
							// Keep array of added/updated perspectives
							$keep_perspectives[] = $perspective;
							PerspectiveModel::createUpdatePerspective($perspective, $exhibition_id);
						}
						PerspectiveModel::deletePerspectivesForExhibition($exhibition_id, $keep_perspectives);


						// for each POI will need to store video_length, language, perspective
						//TODO cannot do this yet as length is not included in response
						foreach($recipe["content"] as $file)
						{

							//createUpdateVideo();
						}

						// If we have made it this far everything has been updated successfully. Update the recipe_last-updated date.
						$newdate = new DateTime($recipe_last_updated);
						$sqlnewdate = date_format($newdate, "Y-m-d H:i:s");
						ExhibitionModel::updateExhibitionRecipeLastUpdated($recipe["_id"]['$oid'], $sqlnewdate);




					}

				}
			}

			// Handle out of date exhibitions no longer available on authoring tool
			$exhibitions = ExhibitionModel::getExhibitions();
			foreach($exhibitions as $exhibition){
				if(!isset($codes[$exhibition->code]) ){
					echo "Recipe ".$exhibition->recipe_id." (".$exhibition->code.") No longer on authoring tool. Deleting this exhibition.\n";
					//ExhibitionModel::deleteExhibitionByRecipeId($exhibition->recipe_id);
				}
			}


			echo "-------------------------------------------------------\n";
		}

	}
}

// if running from the command line - update the system
if (php_sapi_name() == "cli") {
	phpRecipeApi::updateSystem();
}