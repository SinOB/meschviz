<?php

/** authoring tool super api to
 * Hope will eventually replace existing API's
 *
 * 1. provide search of content at europeana and musea
 * 2. provide search of visit log
 * 3. provide recipie info
 */

// TODO
// TODO get musea endpoint
// TODO merge in code from VisitLog


if ( !class_exists('phpSuperAPI') ) {
	if (session_id() == "") {
		@session_start();
	}

	class phpSuperAPI {

		var $endpoint = 'http://meschapi.suggesto.eu/api/jsonws/suggestocs-portlet.suggestorecommender/search';

		/**
		 * Get user visit loginfo based on supplied passcode
		 *
		 * @param unknown $passcode
		 */
		function getVisitLogByPasscode($passcode)
		{
			// First call service to get session id from passcode
			$result = $this->getVisitSessionByPasscode( $passcode );

			if (! isset( $result ))
			{
				return;
			}
			$session_id = $result->session;
			$recipe_id = $result->exhibitID;

			if (isset( $session_id ) && $session_id != '' && isset( $recipe_id ) && $recipe_id != '')
			{
				// Now get visit log based on session id
				$result = $this->getVisitLog( $session_id, $recipe_id );
				return $result;
			}
			return;
		}


		/**
		 * Get visit log based on session_id and exhibit id
		 *
		 * @param unknown $session_id
		 */
		function getVisitLog($session_id, $recipe_id)
		{

			$params = array (
					'groupId' => '0',
					'serviceName' => 'meschLogES',
					'collection' => '',
					'queryString' => 'session:' . $session_id . ' AND exhibitID:' . $recipe_id,
					'sort' => '',
					'skipRows' => 0,
					'numberOfItems' => 1000
			); // Set to 1000 as this should be more actions than in a single visit session. Ideally this should be unlimited.


			$results = $this->request( $this->endpoint, $params );


			$array = array ();
			for($i = 0; $i < count( $results ); $i ++){
				// Only include results that don't contain an "code" field.
				//This makes sure that the Passcode entry is not included as a log entry
				if(!isset($results[$i]->_source->code)){
					$array [] = $results [$i]->_source;
				}
			}

			// sort the array based on the timestamp, most recent entries first.
			usort( $array, function ($a, $b) {
				return $a->timestamp > $b->timestamp ? - 1 : 1;
			} );

			return $array;
			}


		/**
		 * Get session id and exhibit id from passcode
		 * http://elasticsearch.meschproject.webfactional.com/logs/passcode/_search?q=code:HQK-979-0017
		 *
		 * @param string $passcode
		 */
		function getVisitSessionByPasscode($passcode)
		{

			$params = array (
					'groupId' => '0',
					'serviceName' => 'meschLogES',
					'collection' => '',
					'queryString' => 'code:"' . $passcode . '"', // double quotes required to force search for exact passcode
					'sort' => '',
					'skipRows' => 0,
					'numberOfItems' => 1
			); // There should only be 1 session for a given passcode

			$result = $this->request( $this->endpoint, $params );

			if (count( $result ) < 1)
			{
				return;
			}

			return $result[0]->_source;
		}

		/**
		 * Test that visit log API is available
		 */
		function test_logs()
		{
			return $this->getVisitSessionByPasscode("CEJ-228-0017");
		}

		function test_UoA_Europeana()
		{
			// Hadi's server wet down - created a new one so point test to this
			//$endpoint = "http://e.hum.uva.nl:8080/europeana_edm_v1.5/object/_search";
			$endpoint = "http://rijsbergen.hum.uva.nl:8080/europeana_edm_v1.5/object/_search";
			$params = '{ "query" : {"filtered": {"filter" : {"bool": {"must": [{  "terms": { "dc_description": ["castle"] }},{  "term": { "edm_type": "IMAGE" }}]}}}}}';

			$curl = curl_init();

			// perform a post - set this before setting the header as CURLOPY_POSTFIELDS changes the header
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

			// Set to UTF-8
			curl_setopt($curl, CURLOPT_HTTPHEADER, array (
					"Content-Type: application/x-www-form-urlencoded;charset=UTF-8"
			));

			//The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,5);
			//The maximum number of seconds to allow cURL functions to execute.
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);

			curl_setopt($curl, CURLOPT_URL, $endpoint);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			//curl_setopt($curl, CURLOPT_VERBOSE, 1);
			curl_setopt($curl, CURLOPT_FAILONERROR, true);

			$result = curl_exec($curl);
			curl_close($curl);

			return $result;

		}

		/**
		 * Search europeana based on string, language and reusability
		 *
		 * @param unknown $string
		 * @param unknown $count
		 * @param unknown $language
		 * @param unknown $reusability
		 * @return unknown
		 */
		function getEuropeanaSearch($string, $count, $language, $reusability){
			$endpoint = $this->endpoint;

			// convert the string to a list of comma separated quoted strings
			$string = '"' . implode('", "', explode(' ',strtolower($string))) . '"';

			// if neither language nor reusability fall back on basic search
			// whomever designd elastic search for searching should be shot
			if((isset($reusability) && $reusability!='') || (isset($language) && $language!='')){

				$reusability_filter = '';
				$language_filter = '';

				// filter based on general reusability parameter (open, restricted or permission)
				if(isset($reusability) && $reusability!=''){
					$reusability_filter .= '{  "term": { "edm_rights_aggregated": "'.$reusability.'" }},';
				}

				if(isset($language) && $language!=''){
					$language_filter .= '{  "term": { "edm_language": "'.$language.'" }},';
				}

				$filter = rtrim($language_filter.$reusability_filter, ',');
				// filter based on dynamic language
				// this is giving less results but not limited to language italian
				$query_string = '{
					"POST_body":{
						"from" : 0,
						"size" : '.$count.',
						"query" : {
							"filtered": {
								"filter" : {
									"bool": {
										"must": [
											{  "terms": { "dc_title": ['.$string.'] }},
											{  "term": { "edm_type": "IMAGE" }},'.
											$filter.'
										]
									}
								}
							}
						}
					}}';

			}
			else {

				$query_string = '{"POST_body":{'.
						'"from" : 0,'.
						'"size" : '.$count.','.
						'"query" : {'.
							'"filtered": {'.
								'"filter" : {'.
									'"bool": {'.
										'"must": ['.
											'{  "terms": { "dc_title": ['.$string.'] }},'.
											'{  "term": { "edm_type": "IMAGE" }}'.
										']'.
									'}'.
								'}'.
							'}'.
						'}'.
					'}}';

			}

			$params = array(
					'groupId'=>'0',
					'serviceName'=>'europeanaFacetedSearch',
					'collection'=>'',
					'queryString'=>$query_string,
					'sort'=>'',
					'skipRows'=>0,
					'numberOfItems'=>$count
			);


			$result = $this->request($endpoint, $params);

			return $result;
		}

		/*Get results from Museia via the mesch api*/
		// TODO need to add filter for language and resuability!!!!!
		function getMuseiaSearch($string, $count, $language, $reusability){
			$endpoint = $this->endpoint;

			// Hack for missing image tag - believe we have images for images 1-15000 but not reliable
			//$query_string ="Nome_File:/<1-15000>/ AND ".strtolower($string);

			// Use HasImage flag to only return results with an image
			$query_string ="HasImage:1 AND ".strtolower($string);
			$params = array(
					'groupId'=>'0',
					'serviceName'=>'meschMuseiaES',
					'collection'=>'',
					'queryString'=>$query_string,
					'sort'=>'',
					'skipRows'=>0,
					'numberOfItems'=>$count,
					'HasImage'=>1
			);

			$result = $this->request($endpoint, $params);
			return $result;
		}


		// Get object suggestions from UoA via ectrl based on user visit log only
		function getObjectSuggestion($session_id, $count, $exhibition_code)
		{
			$endpoint = $this->endpoint;

			switch($exhibition_code){
				case "17":
					$exhibition_id= "0017";//Atlantikwall
					break;
				case "1013":
					$exhibition_id= "1013";//APM Roman Gallery
					break;
				default:
					echo "<b>No object searchable database available for this exhibition.</b><br><br>";
					return;
					break;
			}

			$query_string= '{"sessionID":"'.$session_id.'",
					"exhibitionID":"'.$exhibition_id.'",
					"size":"'.$count.'",
					"user":"uva",
					"pass":"mesch",
					"v":"1.0"}';

			$params = array(
				'groupId'=>'0',
				'serviceName'=>'europeanaPersonalizedSuggestion',
				'collection'=>'',
				'queryString'=>$query_string,
				'sort'=>'',
				'skipRows'=>0,
				'numberOfItems'=>0,
			);

			//echo "Params passed are: ";
			//print_r($params);
			//echo "<br/><br/>";

			$result = $this->request($endpoint, $params);

			//print_r($result);
			return $result;
		}

		function request ($endpoint, $args = array())
		{
			$this->response = $this->post($endpoint, $args);

			$result = json_decode($this->response);

			if(isset($result->data->error)){
				error_log('Error in response from API suggestocs-portlet.suggestorecommender/search');
				error_log($result->data->error);
				return;
			}

			$error = json_last_error();
			if (json_last_error()){
				$this->parsed_response = 'Error parsing json received ('.$error.')';
			}
			else if(isset($result->data->hits->hits)){
				// all is well
				$this->parsed_response = $result->data->hits->hits;
			} else {
				error_log('Error in call to API');
				return;
			}
			return $this->parsed_response;
		}

		function post ($endpoint, $data) {

			$email = Config::get ( 'MESCH_SUPER_API_PORTLET_EMAIL' );
			$pwd =Config::get ( 'MESCH_SUPER_API_PORTLET_SECRET' );

			$url = $endpoint;

			if ( function_exists('curl_init') ) {

				$curl = curl_init();

				// convert the parameters to a string and make sure UTF-8
				$params = array();
				foreach ($data as $key => $value) {
					$params[] = $key . '=' . iconv(mb_detect_encoding($value),'UTF-8', $value);
				}



				$data_string= implode('&', $params);
				//print_r($data_string);

				// perform a post - set this before setting the header as CURLOPY_POSTFIELDS changes the header
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

				// Set to UTF-8
				curl_setopt($curl, CURLOPT_HTTPHEADER, array (
						"Content-Type: application/x-www-form-urlencoded;charset=UTF-8"
				));

				//The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,5);
				//The maximum number of seconds to allow cURL functions to execute.
				curl_setopt($curl, CURLOPT_TIMEOUT, 10);

				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($curl, CURLOPT_VERBOSE, 1);
				curl_setopt($curl, CURLOPT_FAILONERROR, true);

				// Basic Authentication:
				curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_setopt($curl, CURLOPT_USERPWD, $email.':'.$pwd);

				$result = curl_exec($curl);
				curl_close($curl);

			} else {
				die('Unable to find curl module');
			}
			return $result;
		}
	}
}