<?php
require realpath(dirname(__FILE__).'/../').'/vendor/autoload.php';
require realpath(dirname(__FILE__).'/../').'/application/core/Config.php';
require realpath(dirname(__FILE__).'/../').'/sources/phpSuperAPI.php';

/*
 * Basic script to suggest content based on visit.
 * Requires a visit passcount and count of results to return.
 * Language and reusability parameter are optional
 */


// Getting content based on visit
//$passcode = 'FYA-662-0017'; // English
//$passcode = 'YYK-607-0017'; // Dutch

$passcode = 'AAA-003-0016'; // Italian visit to review exhibition With special characters - explodes

$count=3; // Number of results to return
$language = null;
$reusability = null;

// Get exhibition
$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);

// Get points viewed
$viewedPoints = VisitLogModel::getVisitPointsByPasscode($passcode);

// initialise empty array
$tags = [];

// set words to ignore for each language
$ignore_nl = ['door','veel','niet','werden','vanuit','de','en','in','een','de','na','ln','van','het','voor','te','on','werd','was','die','ook','zij','naar','nog', 'uit','hun','met','aan','als'];
$ignore_en = ['to','a','from','with','for','too','if','where','when','there','of','in','on','was','in','and','by','the','this','be','is','were','be','that','but','or','within','also','at','all','whereas','its','how','had','as','used','it'];

// Collect an array of all words found and how often found
foreach($viewedPoints as $point){
	$content = ContentModel::getContentWorkaround(
			$exhibition->exhibition_id,
			$point->audience,
			$point->language,
			$point->perspective,
			$point->id);

	// Make sure we have some text to work with
	if(!isset($content->text) || $content->text==''){
		continue;
	}

	// first remove any newlines
	$words =preg_replace('/\s+/', ' ', $content->text);
	// remove any forward slashes (used in phrases such as and/or)
	$words =preg_replace('|/|', ' ', $words);
	// remove all round brackets
	$words =preg_replace('/\(|\)|:/',' ', $words);

	// now split based on spaces into words
	$words = explode(" ", $words);

	foreach($words as $word){
		$word = trim($word, ',');
		$word = trim($word, '.');
		$word = trim($word, "‘"); // Gets special single quote at start
		$word = trim($word, "’"); // Gets special single quote at end
		$word = trim($word, "'"); // Gets regular single quote
		$word = preg_replace("/\"/", "", $word);
		$word = preg_replace("/\”/", "", $word);
		$word = preg_replace("/\“/", "", $word);
		$word = strtolower($word);

		if( strlen($word)<=3) {
			continue;
		}

		if(strtolower($point->language)=='english'){
			if(in_array($word, $ignore_en) ){
				continue;
			}
		}
		else if(strtolower($point->language)=='dutch'){
			if(in_array($word, $ignore_nl)){
				continue;
			}
		}

		if(isset($tags[$word]))
		{
			$tags[$word]++;
		}else {
			$tags[$word]=1;
		}
	}

}
// sort the words by popularity - most popular first
arsort($tags);

// Get 3 most popular words
$subset = array_slice($tags,0,3,1);

print_r($subset);

$string = implode(' ',array_keys($subset));
//$string = "guerra più quotidiana"; // testing with special characters
echo "Searching on string '".$string."'\n";



// Call the Europeana ES api to get some suggestions based on the words found
$f = new phpSuperAPI();
$data = $f->getEuropeanaSearch ($string,
		$count,
		$language,
		$reusability);

echo "\n".count($data)." results returned from Europeana\n";
//print_r($data);
if(isset($data)) {
	foreach($data as $suggestion){
		echo "\t".$suggestion->_id." = >".$suggestion->_source->dc_title."\n";
	}
}


?>

