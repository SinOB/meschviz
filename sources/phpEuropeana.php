<?php

if ( !class_exists('phpEuropeana') ) {
	if (session_id() == "") {
		@session_start();
	}

	class phpEuropeana {
		var $api_key;

		var $rest_endpoint = "http://www.europeana.eu/api/v2/search.json";

		function phpEuropeana ($api_key, $die_on_error = false) {
			//The API Key must be set before any calls can be made.
			$this->api_key = $api_key;
			$this->die_on_error = $die_on_error;
		}


		function search ($string, $count, $language, $reusability) {
			$args = array("query"=>$string,
					"start"=>"1",
					"rows"=>$count,
			);

			// if we received an language to search on - use this
			if(isset($language)&& $language!=''){
				$args["qf"] = "LANGUAGE:".$language;
			}

			// if we received a usability parameter to search on - use this
			if(isset($reusability)&& $reusability!=''){
				$args["reusability"] = $reusability;
			}

			$result = $this->request( $args);
			return ($this->parsed_response);
		}

		/* May need this down the line (dont forget to change the endpoint and update the url for the search in that function "search.json")
		function getRecord($id){
			// http://europeana.eu/api/v2/record/[recordID].json
			$result = $this->request('record/'.$id.'.json', array());
			return ($this->parsed_response);
		}*/


		function request ($args = array())
		{
			// Process arguments
			$args = array_merge(array(
					"wskey" => $this->api_key,
					"thumbnail"=>true // only return results with a thumbnail
					),
					$args);


			$this->response = $this->post($args);
			$this->parsed_response = json_decode($this->response);

			$error = json_last_error();
			if (json_last_error()){
				$this->parsed_response = 'Error parsing json received ('.$error.')';
			}

			return $this->response;
		}



		function post ($data) {
			$url = $this->rest_endpoint;

			if ( !preg_match("|http://(.*?)(/.*)|", $url, $matches) ) {
				die('There was some problem figuring out your endpoint');
			}

			if($data)
			{
				$string = '?'.http_build_query($data, '', '&');
				$url .= $string;
				$url = urldecode($url);
			}

			if ( function_exists('curl_init') ) {
				// Has curl. Use it!
				$curl = curl_init($url);

				//The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,5);
				//The maximum number of seconds to allow cURL functions to execute.
				curl_setopt($curl, CURLOPT_TIMEOUT, 10);

				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($curl, CURLOPT_VERBOSE, 1);
				curl_setopt($curl, CURLOPT_FAILONERROR, true);
				$response = curl_exec($curl);
				curl_close($curl);

			} else {
				die('Unable to find curl module');
			}
			return $response;
		}
	}
}
?>
