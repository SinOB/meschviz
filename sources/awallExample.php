<?php
class awallExample {

	// Use your own credentials for these fields
	var $email = 'xxxxx';
	var $password = 'xxxxx';

	var $log_endpoint = 'http://meschapi.suggesto.eu/api/jsonws/suggestocs-portlet.suggestorecommender/search';
	var $recipe_endpoint = 'http://meschapi.suggesto.eu/api/jsonws/AuthoringService-portlet.mesch/search';


	public function __construct() {	}

	/**
	 * Get visit log based on session id and exhibit id
	 * @param unknown $sessionId
	 * @param unknown $exhibitId - 4 digit exhibit id e.g. for atlantikwall is 0017
	 */
	function getVisitLog($sessionId, $exhibitId)
	{

		$params = array (
				'groupId' => '0',
				'serviceName' => 'meschLogES',
				'collection' => '',
				'queryString' => 'session:' . $sessionId . ' AND exhibitID:' . $exhibitId,
				'sort' => '',
				'skipRows' => 0,
				'numberOfItems' => 1000
		) // Set to 1000 as this should be more actions than in a single visit session. Ideally this should be unlimited.
		;

		$result = $this->request( $this->log_endpoint, $params );
		return $result;
	}


	/**
	 * Search for a specific recipe based on exhibitId
	 * @param unknown $recipeId
	 * @return unknown
	 */
	function getRecipe($exhibitId)
	{
		// Search
		$url = "http://meschapi.suggesto.eu/api/jsonws/AuthoringService-portlet.mesch/search";
		$params = array(
				"collectionName"=>"recipes",
				"filter"=>"{'exhibitID': '".$exhibitId."'}",
				"output"=>""
		);

		$result = json_decode($this->request( $this->recipe_endpoint, $params ));
		return $result[0];
	}


	/**
	 * Perform the request
	 * @param unknown $endpoint - url for api
	 * @param unknown $args
	 */
	function request ($endpoint, $args = array())
	{
		$this->response = $this->post($endpoint, $args);

		$result= json_decode($this->response);

		$error = json_last_error();
		if (json_last_error()){
			$this->parsed_response = 'Error parsing json received ('.$error.')';
		}
		else{
			$this->parsed_response = $result;
		}
		return $this->parsed_response;
	}

	/**
	 * Perform the post
	 * @param unknown $endpoint
	 * @param unknown $data
	 * @return unknown
	 */
	function post ($endpoint, $data) {
		$url = $endpoint;

		if ( function_exists('curl_init') ) {

			$curl = curl_init();

			// perform a post
			curl_setopt($curl, CURLOPT_POST, 1);

			if ($data){
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			}

			// Basic Authentication:
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->email.':'.$this->password);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_FAILONERROR, true);

			$result = curl_exec($curl);

			curl_close($curl);

		} else {
			die('Unable to find curl module');
		}
		return $result;
	}
}

$awallExample = new awallExample;
// Get visit log based on sessionId and exhibitId
$log = $awallExample->getVisitLog('5675', '0017');
print_r($log->data->hits->hits);

// Get recipe based on 4 digit exhibitId
$recipe = $awallExample->getRecipe('0017');
print_r($recipe);
