
# MeSchviz - Online post-visit experience

Online post-visit experience for visitors to museum exhibitions. Allows users to visualise their visit in different ways. Also allows exhibition curators to create and maintain visualisations based on exhibitions.

## Quick-Index 

+ [General Info](#markdown-header-general-info)
+ [Requirements](#markdown-header-requirements)
+ [Installation](#markdown-header-installation)
+ [Testing with demo user](#markdown-header-users)
+ [Todo](#markdown-header-todo)

Or leave it empty and use the [link text itself].

## General Info
* Core system is based on PHP HUGE, formerly php-login : http://www.php-login.net
* users can register, login, logout (with username, email, password)
* password-forget / reset
* account verification via mail
* captcha
* failed-login-throttling (**untested**)
* user profiles
* supports local avatars and remote Gravatars
* supports native mail and SMTP sending (via PHPMailer and other tools)
* uses PDO for database access for sure, has nice DatabaseFactory (in case your project goes big) 
* uses URL rewriting ("beautiful URLs")
* uses Composer to load external dependencies (PHPMailer, Captcha-Generator, etc.)
* Live version of site available at http://viz.meschproject.webfactional.com (However live version of site does not have any logging api available - so is largly useless for the moment).


## Requirements

* **PHP 5.6**
* **MySQL 5.5+** database 
* installed PHP extensions: pdo, gd, openssl
* installed tools on your server: git, curl, composer
* for professional mail sending: an SMTP account
* activated mod_rewrite on your server
* **Curl, php5-curl, openssl, php5-gd**
* **Composer**


## Installation
### 1) Clone the repo to your server
> 	git remote add origin https://SinOB@bitbucket.org/SinOB/meschviz.git
>
> 	git pull origin master

### 2) Create the database
> Create a MySql database on your server - update the config file accordingly.
>
> 	CREATE DATABASE meschviz;
>
> Pull in the schema from the empty_db.sql file in the repo.
>
> 	mysql -u username -p meschviz < empty_db.sql
>
> Create the demo users
> 	mysql -u username -p meschviz < users.sql
>
> By default there are now three demo users, a super admin user, a curator user and a normal user.
>
> Super admin user: default username is admin, password is 12345678. This user is already activated. **It is strongly advised that this account name and password both be altered for security**
>
> Curator user (can create visualisations ): username is curator, password is 12345678
>
> Normal user: Username is basic, password is 12345678. The user is already activated.
>   
>

### 3) Create and modify the config file
> Edit the base .htaccess file to set the APPLICATION_ENV variable (If not on shared hosting this can be set in apache).
> 
> 	SetEnv APPLICATION_ENV Production
>
> If this does not work this can be changed in the code. In /application/core/Environment.php change fallback from 'development' to 'production'.
>
> Navigate to /application/config/ and copy sample.config.production.php to config.production.php.
>
> Work through your new configuration file (config.production.php) settings the necessary settings to match your server.
>
> Pay particular attention to
> 
> * Database and system settings
>
> * Php settings
>
> * FLICKR_API_KEY
>
> * FLICKR_API_SECRET
>
> * FLICKR_CACHE_DIR
>
> * EUROPEANA_API_KEY
>
> * EUROPEANA_SECRET
>
> * RECIPE_PORTLET_EMAIL = email of account to use to access AuthoringService-portlet
>
> * RECIPE_PORTLET_SECRET = password of account to use to access AuthoringService-portlet
>
> * MESCH_API_KEY
>
> * MESCH_SUPER_API_PORTLET_EMAIL = email of account to use to access suggestocs-portlet
>
> * MESCH_SUPER_API_PORTLET_SECRET = password of account to use to access suggestocs-portlet
 	

### 4) Install php packages via composer
> 	php composer.phar install
>
> If this is successful the vendor directory should not contain directories for twig, composer, phpmailer and gregwar

### 5) Population of Recipe info from recipe api
> Navigate to /sources/ directory and run the api script
>
> 	php phpRecipeApi.php

### 6) Set up the recipe update cron job
> Set up cron job to regularly run the script in /sources/phpAuthoringToolAPI.php

### 7) Create and set permissions on cache file
> From the root directory create the cache file
>
> 	mkdir cache
>
> Modify the file permisisons
>
> 	chmod -R 0755 cache
>
> 	cd cache
>
> 	mkdir phpFlickrCache
>
> 	chmod -R 0777 phpFlickrCache
	
### 8) Connect to Logging API
> No setup required. Currently connected with Marks elastic search api - this service needs to be manually started by Mark at the moment



## Users

By default there are three demo users on the current live site, a super admin, a curator user and a normal user.

Super admin user: (responsible for altering regular accounts to make them curators). Details are private.
Curator user (can create visualisations ): username is curator, password is 12345678.
Normal user: Username is basic, password is 12345678. The user is already activated.

### Internal API Use
 * Pass this on to ECTRL if/when ask for this (don't forget to give them the API key for live).
 * NB - to improve security for user management API lock down to only allow from certain ip address/origin.
 * We have an api that will allow the creation of an account for a curator, the updating of an account to curator (including updating the fields listed below) and the revoking of curator permissions. All calls require the inclusion of apiKey for authentication.

> Create curator account example (Curl)
>
> 	curl -v -H POST --data 'apiKey=12345&user_name=frank2&user_email=there2@here.com&user_pwd=12345&user_museum_id=23876' http://localhost/mags/api/createUser
>
> 	response will be an array containing HTTP status code and a message if success or error. If success response will also include the newly created users_id. This id will be required by calling system should they ever want to make an update to the account (e.g. change emal_address)
>
> Update curator account example (Curl)
>
> 	curl -v -H POST --data 'apiKey=12345&user_id=14&user_name=frank3&user_email=there2@here.com&user_pwd=111111&user_museum_id=23876' http://localhost/mags/api/updateUser
> 	response will be an array containing HTTP status code and a message if success or error.
>
> Revoke curator account permissions example (Curl)
>
> 	curl -v -H POST --data 'apiKey=12345&user_id=14' http://localhost/mags/api/revokeCuratorPermissions
>
> 	response will be an array containing HTTP status code and a message if success or error.



## Outstanding Issues
 * Set up an email address specifically for the viz tool and update configuration settings on site to use this.
 * Need to give consideration to sharing of visualisations across social media. - currently visualisations only available to logged in members.
 * Current authoring tool only handles one "type" or recipe - free visit - pull this in so can add limitations further down the line (lie what visualisations are available to what "type" of exhibition)
 * Get rid of code for grid visualisation and associated database table.(Hidden from view at the moment)
 * UoA europeana search does not return the correct item identifier under the field _id - no longer relying on data from UoA for basic search. Using direct Europeana search instead
 * Bug - when old recipies are pulled and have new exhibition ids allocated due to messing about on authoring tool - link between previously added passcodes to exhibitions is broken. Can we add a workaround ? Maybe update passcode table after pull recipies to update exhibitions_id for passcodes based on codes...???
 * Personalised search results based on visit for Europeana, Museia and any other - **debugging UoA results** Had access to API from 25th Feb. 
 * Bubbles and SVG char both display an svg character but using different technologies (one is d3.js, the other is svg.js). This has caused some discrepencies. Chose one and make both work the same. 


### Authoring tool Recipe API Info (Missing Features but all related to larger issues)
* Points of interest are provided in one language only with no id - would like in all languages if available. **Requested by Mark on video call Fri Aug 07**
* Perspectives are provided in only one language and with no id - would like in all languages if available.**Requested by Mark on video call Fri Aug 07**
* Foreach point of interest- still from media file length for each language/perspective.**Requested by Mark on video call Fri Aug 07** - also on github
Strings should not be used as unique identifiers to match across these separate systems. **Requested via github**

## Updating international strings (po/mo files)
Language files are stored in the locale directry.
Each locale has its own separate directory.
Every time a string on the website is added or altered the correcponding string must be updated in the messages.po files.
For the system to see these changes the .po files must be converted to .mo files. At present this can be done via the command line using
> 
>msgfmt -cv -o locale/nl_NL/LC_MESSAGES/messages.mo locale/nl_NL/LC_MESSAGES/messages.po
>
>msgfmt -cv -o locale/it_IT/LC_MESSAGES/messages.mo locale/it_IT/LC_MESSAGES/messages.po
>
>msgfmt -cv -o locale/en_GB/LC_MESSAGES/messages.mo locale/en_GB/LC_MESSAGES/messages.po
> 
Editing of string should be done in UTF-8 only. If unsure use poedit tool. Otherwise risk corrupting the file.
Strings must match EXACTLY. They are case sensitive and any spaces present in msgid must be present in call to gettext _() in php.
If there is an error, it will be reported on the command line. If working locally (localhost) an apache restart is required. On the current live site (webfaction) their servers seem to see the change instantly so no restart is required.
