
/**
 * If flickr search
 * 1. Disable language search
 * 3. Enable all resuability options
 * 2. Disable reusability search by all rights reserved - this not working with flickr api
 */
function flickr_enable_disable(){
	$('input#language').attr('checked', false);
	$("input#language").attr("disabled", true);

	$('input:checkbox[name="reusability"]').removeAttr("disabled");
	
	$('input:checkbox[name="reusability"][value="permission"]').attr('checked', false);
	$('input:checkbox[name="reusability"][value="permission"]').prop("disabled", true);
}

/**
 * If museia search
 * 1. set language to italian by default - since this is all thats available
 * 2. Uncheck all reusability options
 * 3. Turn off reusability options
 */
function museia_enable_disable()
{
	$("input#language").removeAttr("disabled");
	$('input:radio[name="language"][value="it"]').prop("checked", true);
	$("input#language").attr("disabled", true);
	$('input:checkbox[name="reusability"]').attr('checked', false);
	$('input:checkbox[name="reusability"]').prop("disabled", true);
}

/**
 * If europeana search
 * 1. enable language
 * 2. uncheck any language
 * 3. make resuability available for all
 */
function europeana_enable_disable(){
	$("input#language").removeAttr("disabled");
	$('input:radio[name="language"]').prop("checked", false);
	$('input:checkbox[name="reusability"]').removeAttr("disabled");
}

/**
 * Used by timeline point editing page to add 1 single image to a timeline
 * @param $type
 * @param $id
 * @param $source
 * @param $license
 */
function updateTimelinePointImage($type, $id, $source, $license)
{
	$('#source_type').val($type);
	$('#source_id').val($id);
	$('#external_source').val($source);
	$('#license_id').val($license);

	document.getElementById("updateImage").submit();
}

function updateTimelinePointImageUrl()
{
	var source = $('#url').val();
	var license_id =  $('#license_id_url').val();

	updateTimelinePointImage('external', '', source, license_id);
}

function addComponent($type, $id, $source, $license)
{

	$('#source_type').val($type);
	$('#source_id').val($id);
	$('#external_source').val($source);
	$('#license_id').val($license);
	
	var article_id = $('#article_id').val();

    // add the component to the database
	var form = $('form[name="createComponent"]');
	
	var formData = form.serialize();
	var component_id='';
   // process the form
	$.ajax({
		type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
		url         : form.attr('action'), // the url where we want to POST
		data        : formData, // our data object
		dataType    : 'json', // what type of data do we expect back from the server
		encode      : true,
		success: function(data) {
            // get the component_id returned
            component_id = data;
            
            // remove the image from the suggested content on the page
            $("#suggested_content img").each(function() {
         		if($(this).attr("src") == $source ) {
         			$(this).remove();
         		}
         	});
         	
            // add image to actual content section on page

        	var new_div = "<div class='img article_component'>";
        	new_div += "<img id='"+$id +"' alt='' src='"+$source +"'>";
        	new_div += "<div class='overlay' onclick='deleteComponent("+component_id+","+article_id+")'>";
        	new_div += "<span>-</span></div></div>";
        	$("#actual_content").append(new_div);
        	
        	$("#actual_content .img").mouseenter(function(){
        		$(this).addClass("hover");
        	})
        	// handle the mouseleave functionality
        	.mouseleave(function(){
        		$(this).removeClass("hover");
        	});
        
        	return true;
        },
	})
   
    return false;
}



function deleteComponent($component_id, $article_id)
{
	$('#delete_component_component_id').val($component_id);
	$('#delete_component_article_id').val($article_id);
	
	document.getElementById("deleteComponentForm").submit();
}


function addComponentText()
{
	var text = $('textarea#add_component_text').val();
	var license_id =  $('#license_id_text').val(); 
	$('#source_type').val('text');
	$('#component_text').val(text);
	$('#license_id').val(license_id);

	document.getElementById("createComponent").submit();
}

function addComponentUrl()
{
	var source = $('#url').val();
	var license_id =  $('#license_id_url').val();
	$('#source_type').val('external');
	$('#external_source').val(source);
	$('#license_id').val(license_id);

	document.getElementById("createComponent").submit();
}
