<?php
$results = PersonalisationModel::getVisitPersonalisation($this->passcode, 20, null, null);
$licenses = LicenseModel::getLicensesAsArray();
?>
<script>
$(document).ready(function(){

	$('.inline').click(function(e) {
		id = this.id;
		loc = "<?php echo Config::get('URL'); ?>grid/pop/"+id+"/<?php echo isset($this->passcode)?$this->passcode:''; ?>";
		$('#inline_content').load(loc);
	});

	// set box to correct size and reposition scrollbar onclose
	$(".inline").colorbox({
		inline:true,
		fixed:true,
		width:"50%",
		height: "80%",

		onCleanup: function(){
			// Scroll to the top - to prevent bug where loading next box at same point as closed previous
			$("#cboxLoadedContent").scrollTop(0);
			// empty content so don't display briefly when view next
			$('.grid_content').empty();

		},

    });


});

</script>

<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Inspired by your visit'); ?>
</div>
<div class="container">

<?php echo _('We have searched the Europeana collection based on what you have seen during your visit. This is what we found. Feel free to explore.'); ?>

<!-- Based on your visit the three most popular words were:<h2><?php echo isset($results)?$results->search_string:'---'; ?> -->
<br/><br/><br/>
<section id="grid">
	<div class='grid_image'>
	<?php if(isset($results) && isset($results->suggestions)){

	foreach($results->suggestions as $suggestion) {
		if(isset($suggestion['shown_at'])){
		?>
		<a target="_blank" href='<?php echo $suggestion['shown_at']; ?>'>
			<img src="<?php echo $suggestion['external_source']; ?>" title='<?php echo $licenses[$suggestion['license_id']]->title; ?>'>
		</a>
		<?php } else {?>
		<img src="<?php echo $suggestion['external_source']; ?>" title='<?php echo $licenses[$suggestion['license_id']]->title; ?>'>
	<?php }
	}} ?>
	</div>
</section>
</div>