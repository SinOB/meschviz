<script src="<?php echo Config::get('URL'); ?>javascript/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/colorbox.css" />

<script type="text/javascript">

function clickForContent(audience, language, perspective, point)
{
	uid=0;
	loc = "<?php echo Config::get('URL'); ?>content/display_by_tags/<?php echo $this->exhibition->exhibition_id;?>/"+audience+"/"+language+"/"+perspective+"/"+point+"/"+uid;

    $.get(loc, function(data) {
        $.colorbox({
            html: '<div class="passbook_content">'+ data + '</div>',
            width: '1000px',
            height: 'auto',
            href: loc
        });
    });
}

</script>
<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Visit Passport'); ?>
</div>


<div class="container">
	<div id="book">
	<?php for($i = 0; $i < 10; $i ++)  {
		if(!isset($this->viewedPoints[$i])){
			break;
		}?>
		 <img onclick="clickForContent('<?php echo $this->viewedPoints[$i]->audience;?>','<?php echo $this->viewedPoints[$i]->language;?>','<?php echo $this->viewedPoints[$i]->perspective;?>','<?php echo $this->viewedPoints[$i]->id;?>')" class='stamp<?php echo $i+1?>' src="<?php echo Config::get('URL').'/images/stamps/'.$this->exhibition->code.'_'.$this->viewedPoints[$i]->id.'.png' ?>">
	<?php }?>
	</div>
	<figcaption>
		<h1><?php echo _('Your visit passport'); ?></h1>
		<span><?php echo $this->passcode;?></span>
		<p><?php echo _('These are the stamps you collected during your visit. Click on the stamps to view details of the points you have seen during your visit.');?></p>
	</figcaption>

</div>
