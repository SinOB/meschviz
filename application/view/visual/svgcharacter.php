
<script src="<?php echo Config::get('URL'); ?>javascript/svg.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo Config::get('URL'); ?>javascript/jquery-1.11.2.min.js"></script>

<style media="screen" type="text/css">
.palette {
	float: left;
	height: 50px;
	width: 100px;
}

</style>

<script type="text/javascript">
var colors = ["#231F20", "#95578B", "#d82432", "#bb7285", "#b25538", "#2eb4af","#88b67d","#a1a3a2","#eb9346","#f1cc4e"];


function drawLetter(letter, draw)
{
	var letterSVG
	switch(letter){
		case 'A':
			letterSVG = draw.rect(36.612, 36.612).attr({ fill: '#000' });
			break;
		case 'B':
			letterSVG = draw.polygon('27.077,0.036 17.822,9.291 8.568,0.036 0.2,8.404 9.455,17.658 0.2,26.912 8.567,35.28 17.822,26.025 27.077,35.28 35.444,26.912 26.189,17.658 35.444,8.404');
			break;
		case 'C':
			letterSVG = draw.polygon('-0.168,0 -0.168,23.906 13.731,23.906 13.731,15.674 23.206,15.674 23.206,23.906 37.104,23.906 37.104,0');
			break;
		case 'D':
			letterSVG = draw.polygon('11.84,14.616 0.507,29.232 40.225,14.616 0.507,0');
			break;
		case 'E':
			letterSVG = draw.polygon('0,33.439 19.591,0 39.676,33.439');
			break;
		case 'F':
			letterSVG = draw.polygon('286.598,209.725 259.312,209.725 242.425,234.422 272.954,224.569 303.484,234.422');
			break;
		case 'G':
			letterSVG = draw.polygon('0,38.174 0,0 19.59,0 39.676,38.174');
			break;
		case 'H':
			letterSVG = draw.polygon('0,23.906 20.984,23.906 20.984,15.675 30.459,15.675 30.459,23.906 51.443,23.906 25.722,0');
			break;
		case 'I':
			letterSVG = draw.circle(38.446);
			break;
		case 'J':
			letterSVG = draw.polygon('29.49,14.583 18.632,14.583 14.907,14.583 14.907,0 0,0 0,29.167 0.323,29.167 0.323,29.49 18.632,29.49 18.632,43.75 33.539,43.75 33.539,14.583');
			break;
		case 'K':
			letterSVG = draw.polygon('35.939,45.898 0,45.898 8.502,22.949 0,0 35.939,0 27.438,22.949');
			break;
		case 'L':
			letterSVG = draw.path('M11.766,23.659c0-10.107,4.177-18.916,10.364-23.571C21.568,0.034,21.001,0,20.427,0 C9.146,0,0,10.592,0,23.659s9.146,23.66,20.427,23.66c0.574,0,1.141-0.034,1.702-0.088C15.943,42.575,11.766,33.766,11.766,23.659z');
			break;
		case 'M':
			letterSVG = draw.polygon('0,41.464 28.407,41.464 28.407,0');
			break;
		case 'N':
			letterSVG = draw.polygon('12.699,41.375 0,41.375 24.956,0 37.655,0');
			break;
		case 'O':
			letterSVG = draw.polygon('19.083,46.669 0,23.334 19.083,0 38.167,23.334');
			break;
		case 'P':
			letterSVG = draw.polygon('37.867,24.761 0,24.761 0,0 9.467,12.38 18.933,0 28.4,12.259 37.867,0');
			break;
		case 'Q':
			letterSVG = draw.path('M19.628,39.256C8.787,39.256,0,30.468,0,19.628C0,8.788,8.787,0,19.628,0V39.256z');
			break;
		case 'R':
			letterSVG = draw.rect(13.28,52.895);
			break;
		case 'S':
			letterSVG = draw.polygon('22.531,41.099 0,12.899 12.898,0 22.531,11.992 32.165,0.001 45.062,12.899');
			break;
		case 'T':
			letterSVG = draw.path('M19.831,0.093c-10.65,0-19.285,8.634-19.285,19.284S9.18,38.661,19.831,38.661 c10.65,0,19.285-8.634,19.285-19.284H19.831V0.093z');
			break;
		case 'U':
			letterSVG = draw.polygon('0,18.684 10.787,0 32.36,0 43.146,18.684 32.36,37.367 10.787,37.367');
			break;
		case 'V':
			letterSVG = draw.path('M48.389,0H0v13.428h14.614c0.09,5.218,4.341,9.421,9.581,9.421s9.489-4.203,9.581-9.421h14.613V0z');
			break;
		case 'W':
			letterSVG = draw.polygon('0,29.361 20.467,0 40.936,29.361 30.701,47.087 10.233,47.087');
			break;
		case 'X':
			letterSVG = draw.polygon('50.711,0 19.452,0 -0.001,19.452 70.163,19.452');
			break;
		case 'Y':
			letterSVG = draw.polygon('19.235,15.524 -0.001,0 -0.001,29.599 38.473,29.599 38.473,0');
			break;
		case 'Z':
			letterSVG = draw.polygon('42.416,17.975 21.207,42.143 0,17.975 0,0 42.416,0');
			break;
	}
	return letterSVG;
}

function drawEyes(draw)
{
	var group  = draw.group().attr('class', 'my-group')
	var right  = group.circle(20).attr('class', 'my-element').attr({ fill: '#231F20' }).cx(36.508).cy(10.593)
	var pplright = group.circle(6).attr('class', 'my-element').attr({ fill: '#ffffff' }).cx(36.507).cy(10.592)
	var left  = group.circle(20).attr('class', 'my-element').attr({ fill: '#231F20' }).cx(10.592).cy(10.593)
	var ppleft = group.circle(6).attr('class', 'my-element').attr({ fill: '#ffffff' }).cx(10.592).cy(10.592)
	return group;
}


$(document).ready(function(){

	//26 letters in english aplpabet
	// 17,576 possible combinations of shapes
	var code = "<?php echo substr (strtoupper($this->passcode), 0, 7 ) ; ?>";
	var draw = SVG('drawing')


	// make the group bigger for the current passcode
	draw.size(250, 280);

	var character = draw.group()//.size

	//split the supplied passcode into parts of the character
	hat = code[0];
	face = code[1];
	chin = code[2];

	// split the passcode into colours for parts of the character
	var hat_colour = colors[code[4]];
	var face_colour = colors[code[5]];
	var chin_colour = colors[code[6]];

	var myFace = drawLetter(face, draw);
	var myHat = drawLetter(hat, draw);
	var myChin = drawLetter(chin, draw);
	var myEyes = drawEyes(draw);

	myFace.center(125, 50);
	myHat.center(125,20);
	myChin.center(125, 80);
	myEyes.center(125,45)

	character.add(myFace)
	character.add(myHat)
	character.add(myChin)
	character.add(myEyes)
	character.back()

	myFace.attr({ fill: face_colour })
	myHat.attr({ fill: hat_colour })
	myChin.attr({ fill: chin_colour })


	// special case for circle and rectangle when transformed
	// these do not scale like rest of shapes
	myChin.transform({scaleX:0.75, scaleY:0.75})
	var chinRotate= 50;
	switch(chin)
	{
		// special case for rectangle
		case "A":
		case "R":
			// rotate
			myChin.transform({rotation: chinRotate});
			// reposition after scale and rotation
			myChin.center(130, 80);
			break;
			// special case for circle after scale - do not angle
		case "I":
			myChin.center(125, 80);
			break;
		default:
			//rotate chin
			myChin.transform({rotation: chinRotate});
			myChin.center(162, 98);
	}

	myHat.transform({scaleX:0.75, scaleY:0.75})
	switch(hat)
	{
		// special case for rectangle
		case "A":
		case "R":
			myHat.center(130, 20);
			break;
			// special case for circle
		case "I":
			myHat.center(125, 20);
			break;
		default:
			myHat.center(163, 30);
	}


	//lets say we want to increase the group - can do this (if viewable area big enough)
	character.scale(2.2,2.2);
	character.center(-90,100);

});
</script>
	<div id="drawing"></div>

