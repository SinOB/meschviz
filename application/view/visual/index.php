<div class="container">
<h1><?php echo _('My Exhibitions');?></h1>
<p><?php echo _("Click on the exhibition name to enable/disable static visualisations.");?></p>
<?php foreach($this->exhibitions as $exhibition) { ?>
<a href="<?php echo Config::get('URL').'exhibition/edit/'.$exhibition->exhibition_id; ?>"><?php echo $exhibition->title?></a>
<br/><br/>
<?php }?>

<h1><?php echo _('My Dynamic Visualisations');?></h1>

<p><?php echo _("Click on the links below to create/edit/delete your dynamic visualisations. You can create multiple instances of each of these types of visualisation.");?></p>
<a href="<?php echo Config::get('URL'); ?>magazine/index"><?php echo _("My magazines");?></a>
<br/><br/>
<a href="<?php echo Config::get('URL'); ?>timeline/index"><?php echo _("My timelines")?></a>
<br/><br/>
<!-- <a href="<?php echo Config::get('URL'); ?>grid/index"><?php echo _("My grid visualisations")?></a>
<br/><br/> -->
<!-- <a href="<?php echo Config::get('URL'); ?>map/index"><?php echo _("My map visualisations");?></a>
<br/><br/> -->
<a href="<?php echo Config::get('URL'); ?>news/index"><?php echo _("News items");?></a>
<br/><br/>
<br/><br/>
</div>
