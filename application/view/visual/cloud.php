<script src="<?php echo Config::get('URL'); ?>javascript/jquery-1.11.2.min.js"></script>
<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/jqcloud.min.css" />
<script src="<?php echo Config::get('URL'); ?>/javascript/jqcloud.min.js"></script>

<?php
$viewedPoints = VisitLogModel::getVisitPointsByPasscode($this->passcode);

$tags = [];
$ignore_nl = ['door','veel','niet','werden','vanuit','de','en','in','een','de','na','ln','van','het','voor','te','on','werd','was','die','ook','zij','naar','nog', 'uit','hun','met','aan','als'];
$ignore_en = ['to','a','from','with','for','too','if','where','when','there','of','in','on','was','in','and','by','the','this','be','is','were','be','that','but','or','within','also','at','all','whereas','its','how','had','as','used','it','they','like','have','because','it\'s','them','here','what','their','your','will','dont'];

if(isset($viewedPoints)){
foreach($viewedPoints as $point){
	$content = ContentModel::getContentWorkaround($this->exhibition->exhibition_id, $point->audience, $point->language, $point->perspective, $point->id);

	// Make sure we have some text to work with
	if(!isset($content->text) || $content->text==''){
		continue;
	}
	// first remove any newlines
	$words =preg_replace('/\s+/', ' ', $content->text);
	// now split based on spaces into words
	$words = explode(" ", $words);

	foreach($words as $word){
		$word = trim($word, ' ');
		$word = trim($word, ',');
		$word = trim($word, "\.");
		$word = trim($word, '‘');
		$word = trim($word, ':');
		$word = preg_replace("/\./","", $word);
		$word = preg_replace("/\"/", "", $word);
		$word = preg_replace("/\”/", "", $word);
		$word = preg_replace("/\“/", "", $word);
		$word = preg_replace("/\’/", "", $word);
		$word = strtolower($word);

		if( strlen($word)<=3) {
			continue;
		}

		if(strtolower($point->language)=='english'){
			if(in_array($word, $ignore_en) ){
				continue;
			}
		}
		else if(strtolower($point->language)=='dutch'){
			if(in_array($word, $ignore_nl)){
				continue;
			}
		}

		if(isset($tags[$word]))
		{
			$tags[$word]++;
		}else {
			$tags[$word]=1;
		}
	}

}
arsort($tags);
} else {
	$tags["No points found. Suspect API is down."]=1;
}
?>
<script type="text/javascript">
var basic_words = [
 	<?php foreach($tags as $key=>$value) {?>
	{text: "<?php echo $key; ?>", weight: <?php echo $value; ?>},
	<?php }?>
	];

$( document ).ready(function() {
	$('#demo-simple').jQCloud(basic_words, {
	      width: 600,
	      height: 280
	    });
});
</script>

<div id="demo-simple" class="cloud" ></div>
