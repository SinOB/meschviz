<?php
error_reporting(E_ALL);
$licenses = LicenseModel::getLicensesAsUrlArray();
?>
<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('UoA recommender'); ?>
</div>
<div class="container">
<h2>UoA object search page - get 20 results for passcode <?php echo $this->passcode?> (<?php echo $this->exhibition->title?>)</h2>

<?php $results = PersonalisationModel::getObjectSuggestion($this->passcode, 20, null, null); ?>
<section id="grid">
	<div class='grid_image'>
<?php
if(isset($results) && count($results)>0){
	foreach($results as $suggestion) { ?>
		<a target="_blank" href='<?php echo $suggestion->_source ->edm_isShownAt; ?>'>
		<img src="<?php echo $suggestion->_source ->edm_preview; ?>" title='<?php echo $licenses[$suggestion->_source ->edm_rights_url]->title;?>' >
		</a>
		<?php
	}
} ?>

	</div>
</section>
