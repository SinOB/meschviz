<?php
$viewedPoints = VisitLogModel::getVisitPointsByPasscode( $this->passcode );
$allPoints = ContentModel::GetContentByExhibition($this->exhibition->exhibition_id);

?>
<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Things you missed'); ?>
</div>


<div class="container">

<?php

if(isset($viewedPoints)) {
	$language = $viewedPoints[0]->language;
	$audience = $viewedPoints[0]->audience;

	if(isset($allPoints))
	foreach($allPoints as $available_point)
	{

		// skip all points that do not match the visitors language
		if($available_point->language !=$language)
		{
			continue;
		}

		// skip all points that do not match the visitors audience
		if($available_point->target_audience !=$audience)
		{
			continue;
		}


		$found=0;
		foreach($viewedPoints as $viewed)
		{
			if($viewed->id==$available_point->point_of_interest && $viewed->perspective==$available_point->perspective)
			{
				$found=1;
				continue;
			}
		}



	// If point not found in visited then display it as an unseen point
	if($found==0)
	{
?>
		<div class='point_content'>
			<h2><?php echo $available_point->point_of_interest;?> (<?php echo $available_point->perspective ?>): <?php echo $available_point->title;?> </h2>
			<?php if (isset($available_point->url) && ($available_point->type=='image'||$available_point->type=='snapshot')) {?>
			<img src='<?php echo $available_point->url; ?>' class='unseen_image'><br/>
			<?php } ?>
			<?php echo $available_point->text; ?>
		</div>
		<br/><br/>
		<?php
	}
} }
?>
</div>


