<?php
$favorites = VisitLogModel::getFavoritePoints($this->passcode, 3);
?>
<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Your Favourites'); ?>
</div>


<div class="container">
<h1><?php echo _('You spent the most time with the following items:'); ?></h1>
<?php

if(isset($favorites) && count($favorites)>0){
foreach ($favorites as $favorite){
	$content = ContentModel::getContentWorkaround($this->exhibition->exhibition_id, $favorite->audience, $favorite->language, $favorite->perspective, $favorite->id);
?>
	<br/><br/>
	<div class='point_content'>
		<h2><?php echo $content->title;?> (<?php echo $content->perspective;?>). <?php
		echo sprintf(_('You spent %s seconds with this item.'),$favorite->duration);
	?></h2>
		<?php if (isset($content->url) && ($content->type=='image'||$content->type=='snapshot')) {?>
			<img src='<?php echo $content->url; ?>' class='unseen_image'><br/>
		<?php } ?>
		<p><?php echo $content->text;?></p>
	</div>
<?php }} ?>
</div>