<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

<script type="text/javascript">
	var cnvHeight;
	var cnvWidth;
	var c;
	var ctx;

	var img;
	var imgHeight;
	var imgWidth;

	var htmlPalette =[];
	var colors = new Array();

	oFReader = new FileReader(),
	rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

	oFReader.onload = function(oFREvent) {
		document.getElementById("slika").src = oFREvent.target.result;
	};

	function loadImageFile() {
		if (document.getElementById("uploadImage").files.length === 0) {
			return;
		}
		var oFile = document.getElementById("uploadImage").files[0];
		if (!rFilter.test(oFile.type)) {
			alert("<?php echo _("You must select a valid image file!"); ?>");
			return;
		}
		oFReader.readAsDataURL(oFile);
	}



	function setPalette() {
		colors = new Array();
		ctx.width = img.width;
		ctx.height = img.height;

		// once loaded, plop the image on the canvas
		ctx.drawImage(img, 0, 0);

		// load the image data into an array of pixel data (R, G, B, a)
		var imgd = ctx.getImageData(0, 0, $('#imageHolder').width(), $('#imageHolder').height());
		var pix = imgd.data;


		// build an array of each color value and respective number of pixels using it
		for (var i = 0; i < pix.length; i += 4) {
			var colorString = pix[i] + ',' + pix[i + 1] + ',' + pix[i + 2] + ',' + pix[i + 3];
			if (colors[colorString]) {
				colors[colorString]++;
			} else {
				colors[colorString] = 1;
			}
		}
		// sort the array with greatest count at top
		var sortedColors = sortAssoc(colors);
		var ctr = 0;
		// change this to 30 to get more colours but closer in similarity to one another
		var devVal = 30;
		var usedColors = []; // used to see if close color is already seen
		htmlPalette = [];
		var isValid = false;

		for ( var clr in sortedColors) {
			// weed out colors close to those already seen
			var colorValues = clr.split(',');
			var hexVal = '';
			for (var i = 0; i < 3; i++) {
				hexVal += decimalToHex(colorValues[i], 2);
			}
			isValid = true;

			for ( var usedColor in usedColors) {
				var colorDevTtl = 0;
				var rgbaVals = usedColors[usedColor].split(',');
				for (var i = 0; i <= 3; i++) {
					colorDevTtl += Math.abs(colorValues[i] - rgbaVals[i]);
				}
				if (colorDevTtl / 4 < devVal) {
					isValid = false;
					break;
				}
			}

			if (isValid) {
				// if we already have this color in the palette - skip on
				if(htmlPalette.indexOf(hexVal) > -1) {
					continue;
				}

				htmlPalette.push(hexVal);
				var whtTxtStyle = '';
				if (hexVal == 'ffffff') {
					whtTxtStyle = '; color: #666';
				}
				$('#swatches').append('<div class="swatch" style="background: #' + hexVal + whtTxtStyle + ';">'+ hexVal + '</div>');
				usedColors.push(clr);
				ctr++;
			}
		}
	}

	// ref: http://bytes.com/topic/javascript/answers/153019-sorting-associative-array-keys-based-values
	function sortAssoc(aInput) {
		var aTemp = [];
		for ( var sKey in aInput)
			aTemp.push([ sKey, aInput[sKey] ]);
		aTemp.sort(function() {
			return arguments[1][1] - arguments[0][1]
		});
		var aOutput = [];
		for (var nIndex = 0; nIndex < aTemp.length; nIndex++) {
			aOutput[aTemp[nIndex][0]] = aTemp[nIndex][1];
		}
		return aOutput;
	}

	// ref: http://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
	function decimalToHex(d, padding) {
		var hex = Number(d).toString(16);
		padding = typeof (padding) === "undefined" || padding === null ? padding = 2: padding;
		while (hex.length < padding) {
			hex = "0" + hex;
		}
		return hex;
	}

	// parse image
	function parseImg() {

		cnvWidth = 470;
		cnvHeight = 300;

		c = document.getElementById("imageHolder");
		ctx = c.getContext("2d");


		$('div.swatch').remove();
		img = document.getElementById("slika");
		setPalette();

		imgHeight = img.height;
		imgWidth = img.width;

		if (imgHeight < cnvHeight && imgWidth < cnvWidth) {
			ctx.mozImageSmoothingEnabled = false;
			ctx.webkitImageSmoothingEnabled = false;
		}

		if ((imgWidth / imgHeight) < 1.56667) {
			cnvWidth = imgWidth / imgHeight * cnvHeight;
		} else {
			cnvHeight = cnvWidth / (imgWidth / imgHeight);
		}
		ctx.clearRect(0, 0, c.width, c.height);
		ctx.drawImage(img, 0, 0, cnvWidth, cnvHeight);

		htmlPalette.reverse();
		drawLegoPerson('#'+htmlPalette[0],
				'#'+htmlPalette[1],
				'#'+htmlPalette[2],
				'#'+htmlPalette[3],
				'#'+htmlPalette[4]);
	}

	// acepts in colours for body parts
	function drawLegoPerson(head_color, torso_color, arms_color, legs_color, feet_color)
	{
		d3.select("svg").remove();

		var vis_height = 634;
		var vis_width = 490;

		// create the window container
		var svg = d3.select('.character')
		    .append('svg')
		    .attr('width', vis_width)
		    .attr('height', vis_height)
		    .attr("viewBox", "0 0 490.000000 634.000000")
  			.attr("preserveAspectRatio", "xMidYMid meet");

		var body = svg.append("g")
		.attr("transform", "translate(0.000000,634.000000) scale(0.100000,-0.100000)");
		var head = body.append("svg:path")
		.attr("d","M2205 5939 c-27 -4 -76 -13 -107 -20 l-58 -12 0 -98 0 -99 -57 0 c-78 -1 -134 -27 -170 -81 l-28 -43 0 -385 c0 -358 1 -389 19 -424 23 -47 87 -86 155 -94 l51 -6 0 -48 0 -49 395 0 395 0 0 50 c0 50 0 50 33 50 50 0 103 18 137 47 61 51 60 45 60 470 0 435 2 420 -75 478 -30 23 -49 29 -110 33 l-75 5 0 98 0 98 -56 11 c-144 30 -387 39 -509 19z")
		.style('stroke', 'black')
		.style('stroke-width', '1.8')
		.attr("fill", head_color);

		var torso = body.append("svg:path")
		.attr("d", "M1731 4378 c-17 -90 -78 -422 -136 -738 -58 -316 -108 -587 -111 -602 l-6 -28 931 0 c880 0 931 1 931 18 0 9 -15 103 -34 207 -45 250 -95 536 -106 605 -5 30 -24 141 -43 245 -20 105 -46 250 -59 322 l-23 133 -656 0 -657 0 -31 -162z")
		.attr("fill", torso_color);

		var arms1 = body.append("svg:path")
		.attr("d", "M1533 4409 c-41 -12 -133 -96 -171 -157 -26 -42 -336 -549 -383 -627 -10 -17 -37 -93 -59 -170 -23 -77 -45 -150 -51 -163 -17 -42 -10 -89 17 -121 26 -32 25 -51 -3 -51 -55 0 -156 -80 -196 -155 -31 -57 -31 -183 0 -241 25 -47 112 -129 124 -117 14 14 13 166 -2 211 -13 41 -12 46 11 89 30 56 76 83 143 83 139 0 208 -143 117 -245 -12 -13 -21 -45 -26 -94 l-7 -74 29 7 c45 11 106 58 141 110 75 110 62 247 -34 348 -36 37 -37 58 -3 58 43 0 67 41 126 220 55 167 62 183 179 375 67 110 128 210 134 223 10 18 91 439 91 471 0 25 -115 38 -177 20z")
		.style('stroke', 'black')
		.style('stroke-width', '1.8')
		.attr("fill", arms_color);

		var arms2 = body.append("svg:path")
			.attr("d", "M3153 4383 c-29 -6 -29 -18 1 -179 13 -71 30 -165 36 -209 11 -76 17 -89 128 -270 63 -104 124 -202 134 -217 10 -15 44 -103 74 -195 30 -92 60 -179 66 -192 7 -13 33 -32 57 -43 l43 -18 -40 -44 c-65 -70 -77 -100 -77 -191 0 -64 5 -89 24 -124 34 -65 89 -112 166 -141 l26 -10 -7 78 c-5 62 -11 82 -30 103 -87 93 -15 239 118 239 87 0 158 -66 158 -144 -1 -23 -6 -51 -13 -64 -9 -15 -11 -40 -6 -80 4 -31 8 -67 8 -79 1 -35 19 -28 77 30 87 87 107 205 52 314 -29 58 -76 99 -155 135 -66 30 -76 45 -43 65 38 24 34 68 -20 248 -56 186 -36 148 -371 698 -151 248 -206 298 -325 296 -32 -1 -69 -4 -81 -6z")
			.style('stroke', 'black')
			.style('stroke-width', '1.8')
			.attr("fill", arms_color);

		var legs = body.append("svg:path")
		.attr("d", "M1495 2968 c-3 -7 -4 -364 -3 -793 l3 -780 438 -3 437 -2 0 294 c0 191 4 296 10 298 6 2 31 2 55 0 l45 -4 0 -294 0 -294 440 0 440 0 0 795 0 795 -930 0 c-735 0 -932 -3 -935 -12z")
		.style('stroke', 'black')
		.style('stroke-width', '1.8')
		.attr("fill", legs_color);

		var foot1 = body.append("svg:path")
		.attr("d", "M1490 1250 l0 -100 440 0 440 0 0 100 0 100 -440 0 -440 0 0 -100z")
		.style('stroke', 'black')
		.style('stroke-width', '1.8')
		.attr("fill", feet_color);

		var foot2 = body.append("svg:path")
		.attr("d", "M2480 1250 l0 -100 438 2 437 3 3 98 3 97 -441 0 -440 0 0 -100z")
		.style('stroke', 'black')
		.style('stroke-width', '1.8')
		.attr("fill", feet_color);

	}


</script>


<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Photo Person'); ?>
</div>

<div class="container">
<h1><?php echo _('Photo Person'); ?></h1>

<p><?php echo _('Upload an image to extract its color palette and apply it to a lego character.'); ?> </p>

	<div id="colorcodes">
		<div id="insertcolor" style="width: 612px;">
			<input style="float: left;" id="uploadImage" name="myPhoto" onchange="loadImageFile();" type="file">
			<input type="button" onclick="parseImg();" value='<?php echo _('Get palette');?>' autocomplete="off" class='button'/>

		</div>
		<div style="width: 622px; height: 302px;">
			<canvas id="imageHolder" width="622px" height="302px">
			<?php echo _('Your browser does not support the HTML5 canvas tag.'); ?></canvas>
		</div>
		<p><?php echo _('Extracted Color Palette'); ?></p>
		<div id="swatches"></div>
		<img id="slika" src="" alt="<?php echo _('Image preview');?>" style="display: none">

	</div>
	<div id="character" class='character'></div>

</div>