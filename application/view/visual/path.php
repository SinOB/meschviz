<script src="http://cytoscape.github.io/cytoscape.js/api/cytoscape.js-latest/cytoscape.min.js"></script>

<script src="<?php echo Config::get('URL'); ?>javascript/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/colorbox.css" />

<script>

$(function(){ // on dom ready

var cy = cytoscape({
	container: $('#animal_path')[0],
	layout: {
		name: 'grid'
	},
	style: cytoscape.stylesheet()
		.selector('node')
			.css({
				'content': 'data(id)',
				'font-size': '20',
		        'background-color': 'data(faveColor)',
		        'color': '#000'
			})
		.selector(':selected')
			.css({
		    	'border-width': 3,
		        'border-color': '#333'
			})
		.selector('edge')
			.css({
		        'opacity': 0.666,
		        'width': 'mapData(strength, 70, 100, 2, 6)',
		        'target-arrow-shape': 'triangle',
		        'source-arrow-shape': 'circle',
		        'line-color': 'data(faveColor)',
		        'source-arrow-color': 'data(faveColor)',
		        'target-arrow-color': 'data(faveColor)',
		        'curve-style': 'unbundled-bezier',
		    }),
	elements: {
		// each point of interest
	    nodes: [
	            <?php for ($i = 0; $i < count($this->content); $i++) {

	            	if($this->content[$i]->language==$this->points[0]->language && $this->content[$i]->target_audience==$this->points[0]->audience){
	            	?>
		            	{ data: { id: '<?php echo addslashes(str_replace('_', ' ',$this->content[$i]->point_of_interest)); ?>', uid:'<?php echo $this->content[$i]->uid; ?>', name: '', weight: 65, faveColor: '#16a68e', } },
	            <?php
					}

	       	 }?>
	    ],
	    // map paths start point to end point
	    edges: [
	        <?php for($i = 0; $i < count( $this->points )-1; $i ++) {?>
	        { data: { source: '<?php echo addslashes(str_replace('_', ' ',$this->points[$i]->id)); ?>', target: '<?php echo  addslashes(str_replace('_', ' ',$this->points[$i+1]->id)); ?>', faveColor: '#f52039', strength: 800 } },
	        <?php }?>

	     ]
	}
});


// Show a pop-up for the point
//cy.on('select', 'node', function(e){
//	var node = this;
//});




});
</script>
<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Your visit as an animal'); ?>
</div>

<div class="container">
<h1><?php echo _('Your visit as an animal'); ?></h1>


<?php if (isset($this->animal)){
	echo sprintf(_('Your closest matching animal is a %s'),_($this->animal));
} ?>
&nbsp;</span>
<div id="animal_path"></div>

</div>