<?php
$js_perspectives = json_encode($this->perspectives);
$js_viewed = json_encode($this->viewedPoints);
$js_points = json_encode($this->content);
?>
<script src="<?php echo Config::get('URL'); ?>javascript/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/colorbox.css" />

<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('MeSch Bubbles'); ?>
</div>

<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script><script>

$(function(){ // on dom ready

// Draw the body part related to the appropriate character provided
function drawLetter(letter, body, colour)
{
	var letterSVG
	switch(letter){
		case 'A':
			var shape = body.append("rect")
			.attr("width", 36.612)
			.attr("height", 36.612)
			.attr("fill", colour);
			break;
		case 'B':
			var shape = body.append("svg:path")
    		.attr("d",'M27.077,0.036 17.822,9.291 8.568,0.036 0.2,8.404 9.455,17.658 0.2,26.912 8.567,35.28 17.822,26.025 27.077,35.28 35.444,26.912 26.189,17.658 35.444,8.404')
    		.attr("fill", colour);
			break;
		case 'C':
			var shape = body.append("svg:path")
    		.attr("d",'M-0.168,0 -0.168,23.906 13.731,23.906 13.731,15.674 23.206,15.674 23.206,23.906 37.104,23.906 37.104,0')
    		.attr("fill", colour);
			break;
		case 'D':
			var shape = body.append("svg:path")
    		.attr("d",'M11.84,14.616 0.507,29.232 40.225,14.616 0.507,0')
    		.attr("fill", colour);
			break;
		case 'E':
			var shape = body.append("svg:path")
    		.attr("d",'M0,33.439 19.591,0 39.676,33.439')
    		.attr("fill", colour);
			break;
		case 'F':
			var shape = body.append("svg:path")
    		.attr("d",'M286.598,209.725 259.312,209.725 242.425,234.422 272.954,224.569 303.484,234.422')
    		.attr("fill", colour);
			break;
		case 'G':
			var shape = body.append("svg:path")
    		.attr("d",'M0,38.174 0,0 19.59,0 39.676,38.174')
    		.attr("fill", colour);
			break;
		case 'H':
			var shape = body.append("svg:path")
    		.attr("d",'M0,23.906 20.984,23.906 20.984,15.675 30.459,15.675 30.459,23.906 51.443,23.906 25.722,0')
    		.attr("fill", colour);
			break;
		case 'I':
			var shape = body.append('circle')
            .attr('r', 38.446/2)
            .attr("fill", colour);
			break;
		case 'J':
			var shape = body.append("svg:path")
    		.attr("d",'M29.49,14.583 18.632,14.583 14.907,14.583 14.907,0 0,0 0,29.167 0.323,29.167 0.323,29.49 18.632,29.49 18.632,43.75 33.539,43.75 33.539,14.583')
    		.attr("fill", colour);
			break;
		case 'K':
			var shape = body.append("svg:path")
    		.attr("d",'M35.939,45.898 0,45.898 8.502,22.949 0,0 35.939,0 27.438,22.949z')
    		.attr("fill", colour);
			break;
		case 'L':
			var shape = body.append("svg:path")
    		.attr("d",'M11.766,23.659c0-10.107,4.177-18.916,10.364-23.571C21.568,0.034,21.001,0,20.427,0 C9.146,0,0,10.592,0,23.659s9.146,23.66,20.427,23.66c0.574,0,1.141-0.034,1.702-0.088C15.943,42.575,11.766,33.766,11.766,23.659z')
    		.attr("fill", colour);
			break;
		case 'M':
			var shape = body.append("svg:path")
    		.attr("d",'M0,41.464 28.407,41.464 28.407,0')
    		.attr("fill", colour);
			break;
		case 'N':
			var shape = body.append("svg:path")
    		.attr("d",'M12.699,41.375 0,41.375 24.956,0 37.655,0')
    		.attr("fill", colour);
			break;
		case 'O':
			var shape = body.append("svg:path")
    		.attr("d",'M19.083,46.669 0,23.334 19.083,0 38.167,23.334')
    		.attr("fill", colour);
			break;
		case 'P':
			var shape = body.append("svg:path")
    		.attr("d",'M37.867,24.761 0,24.761 0,0 9.467,12.38 18.933,0 28.4,12.259 37.867,0')
    		.attr("fill", colour);
			break;
		case 'Q':
			var shape = body.append("svg:path")
    		.attr("d",'M19.628,39.256C8.787,39.256,0,30.468,0,19.628C0,8.788,8.787,0,19.628,0V39.256z')
    		.attr("fill", colour);
			break;
		case 'R':
			var shape = body.append("rect")
			.attr("width", 13.28)
			.attr("height", 52.895)
			.attr("fill", colour);
			break;
		case 'S':
			var shape = body.append("svg:path")
    		.attr("d",'M22.531,41.099 0,12.899 12.898,0 22.531,11.992 32.165,0.001 45.062,12.899')
    		.attr("fill", colour);
			break;
		case 'T':
			var shape = body.append("svg:path")
    		.attr("d",'M19.831,0.093c-10.65,0-19.285,8.634-19.285,19.284S9.18,38.661,19.831,38.661 c10.65,0,19.285-8.634,19.285-19.284H19.831V0.093z')
    		.attr("fill", colour);
			break;
		case 'U':
			var shape = body.append("svg:path")
    		.attr("d",'M0,18.684 10.787,0 32.36,0 43.146,18.684 32.36,37.367 10.787,37.367')
    		.attr("fill", colour);
			break;
		case 'V':
			var shape = body.append("svg:path")
    		.attr("d","M48.389,0H0v13.428h14.614c0.09,5.218,4.341,9.421,9.581,9.421s9.489-4.203,9.581-9.421h14.613V0z")
    		.attr("fill", colour);
			break;
		case 'W':
			var shape = body.append("svg:path")
    		.attr("d",'M0,29.361 20.467,0 40.936,29.361 30.701,47.087 10.233,47.087')
    		.attr("fill", colour);
			break;
		case 'X':
			var shape = body.append("svg:path")
    		.attr("d",'M50.711,0 19.452,0 -0.001,19.452 70.163,19.452')
    		.attr("fill", colour);
			break;
		case 'Y':
			var shape = body.append("svg:path")
			.attr("d", 'M19.235,15.524 -0.001,0 -0.001,29.599 38.473,29.599 38.473,0z')
			.attr("fill", colour);
			break;
		case 'Z':
			var shape = body.append("svg:path")
			.attr("d",'M42.416,17.975 21.207,42.143 0,17.975 0,0 42.416,0z')
			.attr("fill", colour);
			break;
	}
	return shape;
}

// Draw the eyes
function drawEyes(body)
{
	var group = body.append("g");
	var right = group.append('circle').attr('r', 20/2).attr("fill", '#231F20').attr("cx", 36.508).attr("cy",10.593);
	var pplright = group.append('circle').attr('r', 6/2).attr({ fill: '#ffffff' }).attr("cx", 36.507).attr("cy",10.592);
	var left = group.append('circle').attr('r', 20/2).attr("fill", '#231F20').attr("cx", 10.592).attr("cy",10.593);
	var pplleft = group.append('circle').attr('r', 6/2).attr({ fill: '#ffffff' }).attr("cx", 10.592).attr("cy",10.592);

	return group;
}

function clickPOI(d)
{

	string = "<?php echo _('You DID NOT visit this point.'); ?>";
	for (i = 0; i < viewed.length; i++) {
		if(viewed[i]['id']==d.point_of_interest && viewed[i]['perspective']==d.perspective){
	    	string ="<?php echo _('You DID visit this point.'); ?>";
	    	break;
		}
	}

	loc = "<?php echo Config::get('URL'); ?>content/display/"+d.uid+"/"+<?php echo $this->exhibition->exhibition_id; ?>;

    $.get(loc, function(data) {
        $.colorbox({
            html: '<div class="bubble_content">'+ string + '<br/><br/>' + data + '</div>',
            width: '1000px',
            height: 'auto',
            href: loc
        });
    });
}

var viewed = <?php echo $js_viewed; ?>;
var all_points = <?php echo $js_points; ?>;

// Get the language to be pulled for the content - base on the first point viewed for the moment
function getLanguage()
{
	if(viewed.length>0){
		return viewed[0]['language'];
	}
	return all_points[0]['language'];
}
var language = getLanguage();

function getTargetAudience()
{
	if(viewed.length>0){
		return viewed[0]['audience'];
	}
	return all_points[0]['target_audience'];
}
var audience = getTargetAudience();


function setOpacity(d)
{
	for (i = 0; i < viewed.length; i++) {
	    if(viewed[i]['id']==d.point_of_interest && viewed[i]['perspective']==d.perspective){
		    return 1
		}
	}
	return 0.2;
}


// Cause output when user clicks on body
function clickBody(d)
{
        $.colorbox({
            html: '<div class="bubble_content" style="text-align:center">That tickles!!</div>',
           	width: '200px',
            height: 'auto',
        });
}

// Cet central point [x,y] of item passed in
function getCentroid(item)
{
	var pathEl = item.node();
	var BBox = pathEl.getBBox();
	center = [BBox.x + BBox.width/2, BBox.y + BBox.height/2];
	return center;
}



// Perspectives available on this exhibition
var perspectives = <?php echo $js_perspectives; ?>;

// Colours for the perspectives - based on the colour codes as supplied by Museon for Atlantikwall
var perspective_colours = ["#FF3300","#355F18","#647966","#DEDEB1"];


// the visualisation window size
var vis_height = 550;
var vis_width = 800;
var rad = Math.PI/180;

var r = 200;

// create the window container
var svg = d3.select('.container')
    .append('svg')
    .attr('width', 920)
    .attr('height', 587)
    .append('g')
    .attr('transform', 'translate(' + vis_width/2 + ',' + vis_height/2 + ')'); // this moves the grid to the visual center


// ------------------------
// Draw the circles
// ------------------------

for (j = 0; j < perspectives.length; j++) {

	var data =[];
	for(i=0; i<all_points.length; i++){
		if(all_points[i]['perspective'] == perspectives[j].perspective && all_points[i]['language'] == language && all_points[i]['target_audience'] == audience){
			data.push(all_points[i]);
		}
	}

	var w = r*(3 + (j* 0.5)),
	h = w;


	var interval = 360/data.length;

	if(data.length > 10){
		radius = w/(360/data.length);
	} else {
		radius = w/35;
	}

	// loop through perspective colors if have more perspectives than defined colors
	var circles = svg.selectAll("g")
	.data(data)
	.enter()
	.append('circle')
	.attr('fill', perspective_colours[j%perspective_colours.length])
	.attr('r', radius)
	.style("opacity", function(d,i){return setOpacity(d)})
	.attr('transform', function (d, i) {
		return "translate(" + ((w/2-r) * Math.cos((interval*i) * Math.PI/180)) + "," + ((w/2-r) * Math.sin((interval*i) * Math.PI/180)) + ")";
		});

	// Add function to pop-up some info on the POI
	circles.on("click", function(d){clickPOI(d)});
}


// -----------------------------
// Draw the character based on the supplied passcode
//-----------------------------
var code = '<?php echo $this->passcode; ?>';
var hat = code[0];
var face = code[1];
var chin = code[2];

// Make face use colors to match persoective colors
var hat_colour = perspective_colours[0];
var face_colour = perspective_colours[2];
var chin_colour = perspective_colours[3];

var body = svg.append("g");

// call function to draw part of body and set color
var myFace = drawLetter(face, body, face_colour);
var myHat = drawLetter(hat, body, hat_colour);
var myChin = drawLetter(chin, body, chin_colour);

var eyes = drawEyes(body);
body.on("click", clickBody);

// position body parts
var centerItem = getCentroid(myFace);
myFace.attr("transform", 'translate(-'+ centerItem[0] +', -'+centerItem[1]+')');

var centerItem = getCentroid(myHat);
var movey = centerItem[1]+30;
myHat.attr("transform", 'translate(-'+ centerItem[0] +', -'+movey+')');

var centerItem = getCentroid(myChin);
var movey = centerItem[1];
// special case for circle letter 'C' - apply a different transform
if(chin=='I'){
	var movey = centerItem[1]+35;
}
myChin.attr("transform", 'translate(-'+ centerItem[0] +', '+movey+')');

var centerItem = getCentroid(eyes);
eyes.attr("transform", 'translate(-'+ centerItem[0] +', -'+centerItem[1]+')');

});

</script>

<div class='container'>
	<figcaption>
		<h1><?php echo _('MeSch Bubbles'); ?></h1>
		<span><?php echo $this->passcode;?></span>
		<p><?php echo _('The character in the centre represents your visit and the dots around it the content of the exhibition. Click on the dots to discover what you have seen and what you missed.');?></p>
	</figcaption>

</div>
