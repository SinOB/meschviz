<div class="container">
	<h1><?php echo _('My Grid Visualisations');?></h1>
    <div>
        <h2><?php echo _('From here you can create a new grid visualisation or edit an existing one.');?></h2>
            <form method="post" action="<?php echo Config::get('URL');?>grid/create">
                <label for='title'><?php echo _('Title');?>: </label>
                <input id='title' type="text" name="title" />
				<br/>
                <label for='exhibition_id'><?php echo _('Exhibition');?>: </label>
                <?php  if (isset($this->exhibitions)){ ?>
                <select id='exhibition_id' name='exhibition_id'>
                	<option value=""></option>
                	<?php foreach ($this->exhibitions as $exhibition) {?>
						<option value="<?php echo $exhibition->exhibition_id; ?>"><?php echo $exhibition->title; ?></option>
					<?php } ?>

				</select>
				<?php } ?>
				<br/>

                <input type="submit" value='<?php echo _('Create this visualisation');?>' autocomplete="off" class='button'/>
            </form>
    </div>
   <?php
    if (isset($this->grids) && count($this->grids) >0){
		    foreach ($this->grids as $grid) {
		    	$images = GridModel::getGridImages($grid->grid_id);
		    	?>
		    	<div class="visualisations_container">
		    		<div class="visualisations">
		    			<div class="box text  load post text image_on title_on" >
		    				<div class="box_inner">
		    					<a href="<?= Config::get('URL') . 'grid/edit/' . $grid->grid_id; ?>" alt='<?php echo _('edit');?>'>
		    					<h2 class="text_title"><?php echo htmlspecialchars($grid->title, ENT_COMPAT, 'UTF-8');?></h2>
	    						<img class="box_single_image centered-and-cropped" src="<?php echo isset($images[0])?$images[0]->url:Config::get('IMAGES_URL').'blank_cover.jpeg' ; ?>" >
	    						</a>
	    					</div>
	    					<div class="info_bar">
	    						<div class="buttons">
	    								<div class='box_passcode'></div>
	    								<div class='box_date'>
	    								<a href="<?= Config::get('URL') . 'grid/view/' . $grid->grid_id; ?>"><?php echo _('View');?></a>
										&nbsp;&nbsp;&nbsp;
										<a href="<?= Config::get('URL') . 'grid/delete/' . $grid->grid_id; ?>"><?php echo _('Delete');?></a>
	    								</div>
	    						</div>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
<?php }
		} else {
		?>
       <div><?php echo _('No grid visualisations found.');?></div>
<?php } ?>

</div>