<?php

$favorite = VisitLogModel::getFavoritePoint($this->passcode);
$content = ContentModel::getContentWorkaround($this->exhibition->exhibition_id, $favorite->audience, $favorite->language, $favorite->perspective, $favorite->id);

?>
<div class="grid_content">
<?php
echo sprintf(_('The item you spent the most time with was %s.'),$favorite->id);?>
&nbsp;
<?php
echo sprintf(_('You spent %s seconds with this item.'),$favorite->duration);
?>
<br/><br/>
<div class='point_content'>
<h2><?php echo $content->title;?> (<?php echo $content->perspective;?>)</h2>

<?php echo $content->text; ?>
</div>
</div>