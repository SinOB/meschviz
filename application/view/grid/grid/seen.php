<?php

$points = VisitLogModel::getVisitPointsByPasscode($this->passcode);
?>
<div class="grid_content">
<h1><?php echo _('You viewed the following points:'); ?></h1>
<?php
foreach($points as $point)
{

	$content = ContentModel::getContentWorkaround($this->exhibition->exhibition_id, $point->audience, $point->language, $point->perspective, $point->id);
	?>

	<div class='point_content'>
		<h2><?php echo $point->id;?>: <?php echo $content->title;?> (<?php echo $point->duration;?> seconds)</h2>
		<?php echo $content->text; ?>
	</div>
	<br/><br/>
	<?php
}
?>
</div>



