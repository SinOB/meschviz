<?php
?>

<img src="<?php echo (isset($this->exhibition->image)&& $this->exhibition->image!='' ? $this->exhibition->image : Config::get('IMAGES_URL').'blank_cover.jpeg'); ?>" alt="<?php echo $this->exhibition->description;?>">
<br/>
<b><?php echo _("Exhibition Description")?>:</b>
<?php echo $this->exhibition->description; ?><br/>
<b><?php echo _("Museum website");?>:</b>	a link to the museums site and (recipe - if/when made available via recipe)<br/>
<?php if(isset($this->exhibition->exhibition_url)) {?>
<b><?php echo _("Exhibition webpage")?>:</b>	<a href='<?php echo $this->exhibition->exhibition_url; ?>'><?php echo $this->exhibition->exhibition_url; ?></a><br/>
<?php }?>