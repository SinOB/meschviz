<?php $results = PersonalisationModel::getVisitPersonalisation($this->passcode, 5, null, null); ?>
<div class="grid_content">
Based on the content you viewed during your visit the three most popular words were:<h2><?php echo $results->search_string; ?></h2><br/>
<?php

echo "Here are ".count($results->suggestions)." results returned from Europeana based on these words.<br/><br/><br/>";
if(isset($results->suggestions)){
	foreach($results->suggestions as $suggestion) {
		//echo "<h3>".htmlspecialchars($suggestion->_source->dc_title)."</h3><br/>";
		echo "<img src='".$suggestion['external_source']."' /><br/>";
		//echo "Subject:".htmlspecialchars($suggestion->_source->dc_subject)."<br/>";
		//echo "Description:".htmlspecialchars($suggestion->_source->dc_description)."<br/><br/><br/><br/>";
	}
}
?>
</div>