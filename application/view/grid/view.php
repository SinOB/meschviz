<script src="<?php echo Config::get('URL'); ?>javascript/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/colorbox.css" />
<script>
$(document).ready(function(){

	$('.inline').click(function(e) {
		id = this.id;
		loc = "<?php echo Config::get('URL'); ?>grid/pop/"+id+"/<?php echo isset($this->passcode)?$this->passcode:''; ?>";
		$('#inline_content').load(loc);
	});

	// set box to correct size and reposition scrollbar onclose
	$(".inline").colorbox({
		inline:true,
		fixed:true,
		width:"50%",
		height: "80%",

		onCleanup: function(){
			// Scroll to the top - to prevent bug where loading next box at same point as closed previous
			$("#cboxLoadedContent").scrollTop(0);
			// empty content so don't display briefly when view next
			$('.grid_content').empty();

		},

    });


});

</script>

<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Grid'); ?>
</div>
<!-- This contains the hidden content for inline calls -->
<div style='display:none'>
	<div class='popup' id='inline_content'>&nbsp;
	</div>
</div>

<section id="grid">
	<div class='grid_image'>
		<img src="<?php echo (isset($this->images)&& isset($this->images[0]))?$this->images[0]->url:'';?>" >
	</div>


	<div class='grid_image'>
		<a class='inline' href="#inline_content" id='exhibition'>
			<img src="<?php echo (isset($this->exhibition->image)&& $this->exhibition->image!='' ? $this->exhibition->image : Config::get('IMAGES_URL').'blank_cover.jpeg'); ?>" alt="">
			<h2><span><?php echo _('The Exhibition'); ?></span></h2>
		</a>
	</div>

	<div class='grid_image'>
		<img src="<?php echo (isset($this->images)&& isset($this->images[1]))?$this->images[1]->url:'';?>" >
	</div>

	<div class='grid_image'>
		<a class='inline' href="#inline_content" id='unseen'>
			<img src="<?php echo (isset($this->images)&& isset($this->images[2]))?$this->images[2]->url:'';?>" >
			<h2><span><?php echo _('Unseen'); ?></span></h2>
		</a>
	</div>

	<div class='grid_image'>
		<img src="<?php echo  (isset($this->images)&& isset($this->images[3]))?$this->images[3]->url:'';?>" >
	</div>

	<div class='grid_image'>
		<img src="<?php echo  (isset($this->images)&& isset($this->images[4]))?$this->images[4]->url:'';?>" >
	</div>

	<div class='grid_image'>
		<a class='inline' href="#inline_content" id='seen'>
		<img src="<?php echo  (isset($this->images)&& isset($this->images[5]))?$this->images[5]->url:'';?>" >
			<h2><span><?php echo _('Seen'); ?></a></h2>
		</span>
	</div>

	<div class='grid_image'>
		<img src="<?php echo  (isset($this->images)&& isset($this->images[6]))?$this->images[6]->url:'';?>" >
	</div>

	<div class='grid_image' >
		<a class='inline' href="#inline_content" id='you'>
			<img class="image_fillall centered-and-cropped" src='http://www.wallnerok.com/sitebuilder/images/35mmPatrikWallnerChina_ShenzhenChinesePersonLOWQ-730x487.jpg' >
			<h2><span><?php echo _('You'); ?></span></h2>
		</a>
	</div>

	<div class='grid_image'>
		<img src="<?php echo  (isset($this->images)&& isset($this->images[7]))?$this->images[7]->url:'';?>" >
	</div>

	<div class='grid_image'>
		<a class='inline' href="#inline_content" id='favorite'>
			<img src="<?php echo  (isset($this->images)&& isset($this->images[8]))?$this->images[8]->url:'';?>" >
			<h2><span><?php echo _('Your Favorite'); ?></span></h2>
		</a>
	</div>

	<div class='grid_image'>
		<img src="<?php echo  (isset($this->images)&& isset($this->images[9]))?$this->images[9]->url:'';?>" >
	</div>

	<div class='grid_image'>
		<img src="<?php echo  (isset($this->images)&& isset($this->images[10]))?$this->images[10]->url:'';?>" >
	</div>

	<div class='grid_image'>
		<a class='inline' href="#inline_content" id='inspiration'>
		<img src="<?php echo (isset($this->images)&& isset($this->images[11]))?$this->images[11]->url:'';?>" >
			<h2><span><?php echo _('Inspiration'); ?></span></h2>
		</a>
	</div>

	<?php for($i=12; $i<count($this->images);$i++) {?>
	<div class='grid_image'>
		<img src="<?php echo $this->images[$i]->url;?>" >
	</div>
	<?php }?>
</section>