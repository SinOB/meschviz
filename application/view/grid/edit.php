<div class="container">
	<h1><?php echo _('Edit grid visualisation');?> "<?php echo htmlspecialchars($this->grid->title, ENT_QUOTES, 'UTF-8'); ?>"</h1>

    <form method='post' action="<?php echo Config::get('URL'); ?>grid/editSave">
		<label for='grid_title'><?php echo _('Title');?>:</label>
		<input type='hidden' name='grid_id' value='<?php echo $this->grid->grid_id; ?>'>
		<input id='grid_title' type='text' name='title' value='<?php echo htmlspecialchars($this->grid->title, ENT_QUOTES, 'UTF-8'); ?>'>
		<br/>
		<label for='status'><?php echo _('Status');?>:</label>
		 <select id='status' name='status'>
           	<option value=""></option>
			<option value="draft" <?php echo ($this->grid->status=='draft')?'selected':''?> ><?php echo _("draft");?></option>
			<option value="<?php echo Config::get('PUBLIC_STATUS')?>" <?php echo ($this->grid->status==Config::get('PUBLIC_STATUS'))?'selected':''?>><?php echo _(Config::get('PUBLIC_STATUS'));?></option>
		</select>
		<br/>
		<?php echo _('Exhibition:');?> <?php echo $this->exhibition->title; ?>
		<br/>
		<input class='button' type='submit' value='<?php echo _('Update visualisation');?>'>

		<a href='<?= Config::get('URL') . 'grid/view/' . $this->grid->grid_id; ?>'><input class='button right' type='button' value='<?php echo _('Preview');?>'></a>
	</form>



	<h2><?php echo _("Images")?></h2>
	<form class='add_images' method='post' id='addImage' action="<?php echo Config::get('URL'); ?>grid/addImage">
		<input type='hidden' name='grid_id' value='<?php echo $this->grid->grid_id; ?>'>
		<label for='url'><?php echo _("Image Url");?>:</label>
		<input id='url' type='text' name='url' value="">
		<label for='license_id'><?php echo _('Copyright license');?>:</label><br/>
		<select name="license_id" id='license_id' class='copyright'>
	    	<?php for($i=0; $i < count($this->licenses); $i++){?>
	  			<option value="<?php echo $this->licenses[$i]->license_id; ?>" ><?php echo $this->licenses[$i]->title; ?></option>
	  		<?php }?>
		</select>
		<input type='submit' value='' class='add'>
		<br/>

		<?php foreach($this->images as $image){ ?>
			<div class='grid_edit_image' >
				<img src='<?php echo htmlspecialchars($image->url, ENT_COMPAT, 'UTF-8') ; ?>' alt='image for grid visualisation'><br/>
				<a href='<?php echo $this->licenses[$image->license_id]->url; ?>'><?php echo $this->licenses[$image->license_id]->title; ?></a>

				<a href="<?php echo Config::get('URL'); ?>grid/imageDelete/<?php echo $image->image_id; ?>/<?php echo $this->grid->grid_id; ?>">

				<input type='button' value='' class='delete'>
				</a>
			</div>

		<?php  } ?>
	</form>
</div>

