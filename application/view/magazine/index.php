
<div class="container">
    <h1><?php echo _('My magazines');?></h1>
    <div >
        <h2><?php echo _('From here you can create a new magazine or edit an existing one.');?></h2>

            <form method="post" action="<?php echo Config::get('URL');?>magazine/create">
                <label for='magazine_title'><?php echo _('Title of new magazine:');?> </label>
                <input id='magazine_title' type="text" name="magazine_text" />
                <br/>
                <label for='exhibition'><?php echo _('Exhibition:');?> </label>
                <?php  if (isset($this->exhibitions)){ ?>
                <select id='exhibition' name='exhibition_id'>
                	<option value=""></option>
                	<?php foreach ($this->exhibitions as $exhibition) {?>
						<option value="<?php echo $exhibition->exhibition_id; ?>"><?php echo $exhibition->title; ?></option>
					<?php }?>

				</select>
				<?php } ?>
				<br/>

                <input type="submit" value='<?php echo _('Create this magazine');?>' autocomplete="off" class='button'/>
            </form>
    </div>
<?php
        if (isset($this->magazines)){

		    foreach ($this->magazines as $key => $value) {?>
		    	<div class="visualisations_container">
		    		<div class="visualisations">
		    			<div class="box magazine text load post text image_on title_on" >
		    				<div class="box_inner">
		    					<a href="<?= Config::get('URL') . 'magazine/edit/' . $value->magazine_id; ?>" alt='<?php echo _('edit');?>'>
						    		<h2 class="text_title"><?php echo htmlspecialchars($value->magazine_title, ENT_COMPAT, 'UTF-8');?></h2>
		    						<img class="box_single_image centered-and-cropped" src="<?php echo isset($value->front_image)? $value->front_image: Config::get('IMAGES_URL').'blank_cover.jpeg';?>" alt="<?php echo $value->magazine_title;?>">
		    					</a>
		    				</div>
		    				<div class="info_bar">
		    					<div class="buttons">
		    						<div class='box_passcode'></div>
		    						<div class='box_date'>
		    						<a href="<?= Config::get('URL') . 'magazine/view/' . $value->magazine_id; ?>" title='<?php echo _('View')." ".$value->magazine_title;?>'><?php echo _('View');?></a>
		    						&nbsp;&nbsp;&nbsp;
		    						<a href="<?= Config::get('URL') . 'magazine/delete/' . $value->magazine_id; ?>" title='<?php echo _('Delete')." ".$value->magazine_title;?>'><?php echo _('Delete');?></a>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
<?php }
		} else {
?>
       <div><?php echo _('No magazines yet. Create some!');?></div>
<?php } ?>


</div>
