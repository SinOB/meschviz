<div class="container">
	<h1><?php echo _('Edit magazine');?> "<?php echo htmlspecialchars($this->magazine->magazine_title, ENT_QUOTES, 'UTF-8'); ?>"</h1>

    <form method='post' action="<?php echo Config::get('URL'); ?>magazine/editSave">
    	<label for='magazine_title'><?php echo _('Magazine Title:');?></label>
    	<input type='hidden' name='magazine_id' value='<?php echo $this->magazine->magazine_id; ?>'>
		<input id='magazine_title'  type='text' name='magazine_title' value='<?php echo htmlspecialchars($this->magazine->magazine_title, ENT_QUOTES, 'UTF-8'); ?>'>
		<br/>
		<?php echo _('Exhibition:');?> <?php echo $this->exhibition->title; ?>
		<br/>
		<label for='status'><?php echo _('Status');?>:</label>
		 <select id='status' name='status'>
           	<option value=""></option>
			<option value="draft" <?php echo ($this->magazine->status=='draft')?'selected':''?> ><?php echo _("draft");?></option>
			<option value="<?php echo Config::get('PUBLIC_STATUS')?>" <?php echo ($this->magazine->status==Config::get('PUBLIC_STATUS'))?'selected':''?>><?php echo _(Config::get('PUBLIC_STATUS'));?></option>
		</select>

		<input class='button' type='submit' value='<?php echo _('Update magazine');?>'>
		<a href='<?= Config::get('URL') . 'magazine/view/' . $this->magazine->magazine_id; ?>'><input class='button right' type='button' value='<?php echo _('Preview');?>'></a>
	</form>




	<h2><?php echo _("Articles")?></h2>
	<form class='edit_article' method='post' id='createArticle' action="<?php echo Config::get('URL'); ?>article/create">
	<table class='create_articles'>
		<tr>
			<th><?php echo _("Article Title");?></th>
			<th><?php echo _("Add/Edit/Delete");?></th>
		</tr>
		<tr>
			<td>
				<label for='article_title' class="screenreader">New Article Title</label>
				<input type='hidden' name='magazine_id' value='<?php echo $this->magazine->magazine_id; ?>'>
				<input id='article_title' type='text' name='article_title' value="" >
			</td>
			<td>
				<input type='submit' value='' class='add'>

			</td>
		</tr>

		<?php foreach($this->articles as $article){ ?>
		<tr>
			<td>
				<?php echo htmlspecialchars($article->article_title, ENT_COMPAT, 'UTF-8') ; ?>
			</td>
			<td>
				<a href="<?php echo Config::get('URL'); ?>article/edit/<?php echo $article->article_id; ?>">
				<input type='button' value='<?php echo _('Edit');?>' class='edit' title="<?php echo _('Edit').' '.$article->article_title;?>" />
				</a>

				<a href="<?php echo Config::get('URL'); ?>article/delete/<?php echo $article->article_id; ?>/<?php echo $this->magazine->magazine_id; ?>">
				<input type='button' value='' class='delete'>
				</a>
			</td>
		</tr>
		<?php  } ?>
	</table>
	</form>
</div>

