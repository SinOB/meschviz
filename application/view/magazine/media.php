<?php
if(isset($this->search_results)){
	$js_results = json_encode($this->search_results);
}
?>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
});


$(document).ready(function(){

	// make overlays hovver functionality work
	$(" .img").mouseenter(function(){
		$(this).addClass("hover");
	})
	// handle the mouseleave functionality
	.mouseleave(function(){
		$(this).removeClass("hover");
	});

	// code to load more search results
	var start=0;
	var each_time=5;
	var current=0;
	var results = <?php echo isset($js_results)?$js_results:'""'; ?>;

	function loadImages(){
		for(current=start; current < results.length; current++){
			// if have results set the background color on the div
			$("#suggested_content_wrapper").css('background','#7f3f98');

			// if not in expected range - do not display any
			if(current===start+each_time){
				start=current;
				next = current+each_time;
				break;
			}
			// within range - create the html and append to the div
			var html = '';

			// create the div
			var new_div = "<div class='img'>";
			new_div += results[current]['html'];
			new_div += "<div class='overlay' onclick='addComponent(\""+results[current]['source']+"\", \""+results[current]['id']+"\", \""+results[current]['external_source']+"\", \""+results[current]['license_id']+"\");'>";
			new_div += "<span>+</span>"
			new_div += "</div>"
			new_div += "</div>"

			// append the new div
			$("#suggested_content").append(new_div);

			// make overlays hovver functionality work
			$("#suggested_content .img").mouseenter(function(){
				$(this).addClass("hover");
			})
			// handle the mouseleave functionality
			.mouseleave(function(){
				$(this).removeClass("hover");
			});

			// We are on the last avaliable result - hide the button
			if(current === results.length-1)
			{
				$('#loadMorePhotos').remove();
			}
		}
	}

	$('#loadMorePhotos').click(function(){
		loadImages();
	})

	// initial call to load x images
	loadImages();


	// add text counter
	var text_max = 500;
    $('#textarea_feedback').html(text_max + ' characters remaining');

    $('#add_component_text').keyup(function() {
        var text_length = $('#add_component_text').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' characters remaining');
    });


});



</script>


<!-- start tabs -->
<div class="tabs">
<ul class="tab-links">
<li class="active"><a href="#tab1"><?php echo _('Search for images');?></a></li>
<li><a href="#tab2"><?php echo _('Upload');?></a></li>
<li><a href="#tab3"><?php echo _('Link');?></a></li>
<li><a href="#tab4"><?php echo _('Add Text');?></a></li>
</ul>

<div class="tab-content">
<div id="tab1" class="tab active">
	<!-- START SEARCH FOR ARTICLE COMPONENTS -->
	<form method="post">
		<div class='search_sources'>
			<h2><?php echo _('Search for images');?></h2>
				<ul class="left">
					<h3><label for="sources_search"><?php echo _("Search"); ?></label></h3>
					<input id='sources_search' type="text" value="" name="sources_search"><br/>
				</ul>
				<ul class="left">
					<h3><label for="search_source"><?php echo _("Media Source"); ?></label></h3>
					<input id='search_source' type="radio" name="search_source" value="flickr" onclick="flickr_enable_disable();"><?php echo _('Flickr');?><br/>
					<input id='search_source' type="radio" name="search_source" value="europeana_direct" onclick="europeana_enable_disable();" checked><?php echo _('Europeana Direct');?><br/>
					<span style="color:red"><input id='search_source' type="radio" name="search_source" value="europeana" onclick="europeana_enable_disable();" ><?php echo _('Mesch Europeana');?> (Personalisation)</span><br/>
					<input id='search_source' type="radio" name="search_source" value="museia" onclick="museia_enable_disable();"><?php echo _('Mesch Museia');?><br/>
				</ul>
				<ul class="left">
					<h3><label for="language"><?php echo _("Language"); ?></label></h3>
						<li><input type="radio" value="en" name='language' id='language'> <?php echo _('English');?></li>
						<li><input type="radio" value="nl" name='language' id='language'> <?php echo _('Dutch');?></li>
						<li><input type="radio" value="it" name='language' id='language'> <?php echo _('Italian');?></li>
				</ul>
				<ul class="left" style="width:600px" style="color:red">
					<h3><?php echo _("Data Reusability"); ?>  (N/A)</h3>
					<li><input id='reusability_open' name='reusability' type="checkbox" value='open'><label for='reusability_open'> <?php echo _("Yes. (open) The records are freely reusable. The licenses in this category are Public Domain, CC-ZERO, CC BY, CC BY-SA");?></label></li>
					<li><input id='reusability_restricted' name='reusability'type="checkbox" value='restricted'><label for='reusability_restricted'> <?php echo _("Yes, (restricted) with restrictions. The records are reusable, but with restrictions. The licenses in this category are CC-BY-NC, CC BY-NC-SA, CC-BY-NC-ND, CC BY-ND, OOC-NC");?></label></li>
					<li><input id='reusability_permission' name='reusability'type="checkbox" value='permission' ><label for='reusability_permission'> <?php echo _("Only with permission.(permission) You can reuse the records only with explicit permission.");?></label></li>
				</ul>
				<div style="clear: both;"></div>
			</div>
		<input type="submit" value='<?php echo _('Search Sources');?>' class='button'/>
	</form>
	<!-- END SEARCH FOR ARTICLE COMPONENTS -->

	<div style="clear:both"></div>

	<div id='suggested_content_wrapper'>
		<div id="suggested_content" class="effects">
		</div>
	<?php if (isset($this->search_results) && count($this->search_results)>5) { ?>
	 	<button id='loadMorePhotos' class='button'><?php echo _("Load more");?></button>
	 <?php }?>
	</div>
	<div style="clear:both"></div>
</div>

<div id="tab2" class="tab">
	<div class='upload_image'>
		<form method="post" name='upload_image' enctype="multipart/form-data" action="<?php echo Config::get('URL'); ?>component/upload" >
			<h2><label for='file_upload'><?php echo _('Upload Image');?></label></h2>
			<?php echo _('Select image to upload');?>:
	    	<input type="file" name="file_upload" id="file_upload"><br/><br/>

	    	<input type='hidden' name='article_id' id='article_id' value='<?php echo $this->article->article_id; ?>'>
	    	<label for='license_id_upload'><?php echo _('Copyright license');?>:</label><br/>
	    	<select id='license_id_upload' name="license_id" class='copyright'>
	    	<?php for($i=0; $i < count($this->licenses); $i++){?>
	  			<option value="<?php echo $this->licenses[$i]->license_id; ?>" ><?php echo $this->licenses[$i]->title; ?></option>
	  		<?php }?>
				</select><br/>
	    	<input type="submit" value="<?php echo _("Upload Image");?>" name="submit"  class='button'>
		</form>
	</div>
</div>

<div id="tab3" class="tab">
	<h2><label for='file_upload'><?php echo _('Link from web');?></label></h2>
		<?php echo _('URL');?>:
    	<input type="text" name="url" id="url"><br/><br/>
    	<input type='hidden' name='article_id_url' id='article_id_url' value='<?php echo $this->article->article_id; ?>'>
    	<label for='license_id_url'><?php echo _('Copyright license');?>:</label><br/>
    	<select id='license_id_url' name="license_id_url" class='copyright'>
    	<?php for($i=0; $i < count($this->licenses); $i++){?>
  			<option value="<?php echo $this->licenses[$i]->license_id; ?>" ><?php echo $this->licenses[$i]->title; ?></option>
  		<?php }?>
		</select><br/>
    	<input type="button" value="<?php echo _("Link Image");?>" name="submit"  class='button' onclick="addComponentUrl();">
</div>

<div id="tab4" class="tab">
<div class='add_text'>
	<h2><label for='add_component_text'><?php echo _('Add Text');?></label></h2>
	<textarea name='add_component_text' rows="4" cols="50" maxlength='500' id='add_component_text'></textarea>
	<div id="textarea_feedback"></div><br/><br/>

	<label for='license_id_text'><?php echo _('Copyright license');?>:</label><br/>
	<select name="license_id_text" id='license_id_text' class='copyright'>
    	<?php for($i=0; $i < count($this->licenses); $i++){?>
  			<option value="<?php echo $this->licenses[$i]->license_id; ?>" ><?php echo $this->licenses[$i]->title; ?></option>
  		<?php }?>
	</select>
	<br/>
	<input type='button' value='<?php echo _('Add Text To Article');?>' class='button' onclick="addComponentText();">
</div>

</div>
</div>
</div>
<!-- end tabs -->