<script src="<?php echo Config::get('URL'); ?>javascript/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/colorbox.css" />
<script>
$(document).ready(function(){
	$(".group1").colorbox({transition:'none',photo:true, maxWidth:"90%", maxHeight:"90%"});


	$('.inline').click(function(e) {
		blob = $(this).html();
		$('#inline_content').html( blob );

	    });

	$(".inline").colorbox({inline:true, width:"50%"});

	// Make left swipe cause horisontal scrolling on page
	$(function() {
		// event.pageY is not available (set to 0) on Samsung Galaxy Tab with android chrome 40.0.2214.109
		// as work around use global variable viewPageY and set when basic jquery tocuh events are triggered
		var viewPageY = 0;
		if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
		   $('html').on("touchstart touchmove touchend", function(e){
		       viewPageY = e.originalEvent.changedTouches[0].pageY;
		   });
		}else{
		   $('html').mousemove(function(e){
		       viewPageY = e.pageY;
		   });
		}

		// Use touchswipe plugin to register a left swipe event and trigger a downwards scroll
	    $(".panels").swipe( {swipeLeft:swipeLeft, allowPageScroll:'vertical'} );
	    function swipeLeft(event, direction, distance, duration, fingerCount) {
			var newPosition = viewPageY + distance;
	        $('html, body').animate({ scrollTop: newPosition });
	    }


	});

});
</script>

<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo $this->magazine->magazine_title; ?>
</div>
<!-- This contains the hidden content for inline calls -->
<div style='display:none'>
	<div class='popup' id='inline_content' >
	</div>
</div>
<div class='panels'>
<h1><?php if(isset($this->magazine) && isset( $this->magazine->magazine_title)) {echo $this->magazine->magazine_title;} ?></h1>
<?php
$loader = new Twig_Loader_Filesystem('../application/view/magazine/panels');
$twig = new Twig_Environment($loader);
$twig->addExtension(new Twig_Extensions_Extension_Text());
$twig->addExtension(new Twig_Extensions_Extension_I18n());
//can be used in twig files as follows {% trans "Hello World" %}

$max_per_panel=5;
$licenses = LicenseModel::getLicensesAsArray();

if (isset($this->articles)){
	foreach($this->articles as $article){
		$count_total=count($article->components);

		// if an empty article - skip
		if($count_total == 0)
		{
			continue;
		}

		$contents=array();
		$contents['title'] =  $article->article_title;


		$i=0; // count all components
		$p=0; // count components in current panel

		// start with current template set to template 5
		$current = $max_per_panel;

		foreach($article->components as $component)
		{
			$i++;
			$p++;

			if($component->source_type=='text')
			{
				$contents['text'.$p]=$component->text;
			}
			else
			{
				$contents['image'.$p]=$component->external_source;
			}

			// set the copyright text
			if(isset($component->license_id))
			{
				if(isset($licenses[$component->license_id]->url) && $licenses[$component->license_id]->url!='')
				{
					$contents['license'.$p]= '&#169; <a href="'.$licenses[$component->license_id]->url.'">'.$licenses[$component->license_id]->title.'</a>';
				} else
				{
					$contents['license'.$p]= '&#169; '.$licenses[$component->license_id]->title;
				}
			} else {
				$contents['license'.$p] = '';
			}
			$contents['title'] =$article->article_title;

			if(
					// If more than 5 components in this article loop through templates in
					// order template5, template 4, template3 ... template1, template5, teplate4...
					$p == $current ||
					// If on last component but not enough content to loop as above
					($i==$count_total && $p!=0)
			){
				$template = 'panel_'.$p.'_1.html';
				echo $twig->render($template, $contents);

				// reduce current template by 1 or if have reached end of available - reset current template to 5
				if($current<=0){
					$current=5;
				} else {
					$current--;
				}

				// reset contents count and contents array
				$contents=array();
				$p=0;
			}

		}

    }
}
// Make it obvious that this is suggested content
if(isset($this->passcode)){
	$results = PersonalisationModel::getVisitPersonalisation($this->passcode, 3, null, null);
	$p =0;


	if(isset($results) && isset($results->suggestions) && count($results->suggestions)>0){

		$suggestion_contents=array();
		$suggestion_contents['title'] =_('Based on your visit').': '.$results->search_string;
		$suggestion_contents['suggested']=1;


		foreach($results->suggestions as $suggestion)
		{
			$p++;
			$suggestion_contents['image'.$p]=$suggestion['external_source'];

			// set the copyright text
			if(isset($suggestion['license_id']))
			{
				if(isset($licenses[$suggestion['license_id']]->url) && $licenses[$suggestion['license_id']]->url!='')
				{
					$suggestion_contents['license'.$p]= '&#169; <a href="'.$licenses[$suggestion['license_id']]->url.'">'.$licenses[$suggestion['license_id']]->title.'</a>';
				} else
				{
					$suggestion_contents['license'.$p]= '&#169; '.$licenses[$suggestion['license_id']]->title;
				}
			} else {
				$suggestion_contents['license'.$p] = '';
			}
		}
		$template = 'panel_'.$p.'_1.html';

		echo $twig->render($template, $suggestion_contents);
	}

}

?>
</div>
