<div class="container outside">
    <div class='login-logo'></div>
    <div class="login-page-box">
	    <div class=" login-box">
	     <h1><?php echo _('Request a password reset'); ?></h1>

	        <!-- request password reset form box -->
	        <form method="post" action="<?php echo Config::get('URL'); ?>login/requestPasswordReset_action">
	        	<p>
	            <label for="user_name_or_email"><?php echo _("Enter your username or email and you'll get a mail with instructions:");?></label>
	            <br/>
	            <input id='user_name_or_email' type="text" name="user_name_or_email" required />

	            </p>
	            <input type="submit" value="<?php echo _('Send me a password-reset mail'); ?>" />
	        </form>
		</div>
    </div>
</div>
