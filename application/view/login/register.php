<div class="container outside">

	<div class='login-logo'></div>
    <div class="login-page-box" style="width:500px;">
    <div class=" login-box">
        <h1><?php echo _('Register a new account'); ?></h1>

        <!-- register form -->
        <form method="post" action="<?php echo Config::get('URL'); ?>login/register_action">
            <!-- the user name input field uses a HTML5 pattern check -->
            <p>
            	<label for="user_name"><?php echo _('Username (letters/numbers, 2-64 chars)');?></label>
            	<br />
            	<input id='user_name' type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" required />

            </p>
            <p>
            	<label for="user_email"><?php echo _('Email address (a real address)');?></label>
            	<br />
            	<input id='user_email' type="text" name="user_email"  required />

            </p>
            <p>
				<label for="user_password_new"><?php echo _('Password (6+ characters)');?></label>
				<br />
               	<input id='user_password_new' type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" />

            </p>
            <p>
            	<label for='user_password_repeat'><?php echo _('Repeat your password');?></label>
            	<br />
            	<input id='user_password_repeat' type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" />

			</p>
            <!-- show the captcha by calling the login/showCaptcha-method in the src attribute of the img tag -->
            <img id="captcha" src="<?php echo Config::get('URL'); ?>login/showCaptcha" alt='captcha'/>
            <p>
            	<label for='captcha_input'><?php echo _('Please enter above characters');?></label>
            	<br />
            	<input id="captcha_input" type="text" name="captcha"  required />

			</p>
            <!-- quick & dirty captcha reloader -->
            <a href="#" style="display: block; font-size: 11px; margin: 5px 0 15px 0; text-align: center"
               onclick="document.getElementById('captcha').src = '<?php echo Config::get('URL'); ?>login/showCaptcha?' + Math.random(); return false"><?php echo _('Reload Captcha');?></a>
			<br/>
			<?php echo sprintf(_('By clicking Register, you agree that you have read and agree with our %sTerms & Conditions%s and our %sCookie Use%s policy.'), '<a href="'.Config::get('URL').'login/terms">','</a>','<a href="'.Config::get('URL').'login/cookies">','</a>')?>


			<br/><br/>

            <input type="submit" value="<?php echo _('Register'); ?>" />
        </form>
    </div>
</div>
</div>
