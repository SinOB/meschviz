<?php

// Server image by Maxim Basinski. Listed as free for commercial use
// found here: https://www.iconfinder.com/icons/473631/array_hosting_network_rack_server_storage_system_icon#size=128

//if (isDomainAvailible('http://www.css-tricks.com'))
?>
<div class="container">
<h1>Server statuses:</h1>



<table>
	<tr>
		<td>
			<h2>Recipe search API</h2>

			<?php if(RecipeModel::test()!=null){?>
	            <img src="<?php  echo Config::get('IMAGES_URL').'server_green.png';?>">
			<?php } else {?>
				<img src="<?php  echo Config::get('IMAGES_URL').'server_red.png';?>">
			<?php } ?>

		</td>
		<td>
			<h2>Flickr</h2>

			<?php if(FlickrModel::test()!=null) {?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_green.png';?>">
			<?php } else {?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_red.png';?>">
			<?php }?>
		</td>
	</tr>
	<tr>
		<td>
			<h2>Hadi's europeana API</h2>
			<?php if(SuperApiModel::test_UoA_Europeana()!=null){ ?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_green.png';?>">
			<?php } else {?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_red.png';?>">
			<?php }?>

		</td>
		<td>
			<h2>Visitor logs API (meschLogES)</h2>
			<?php if(SuperApiModel::test_logs()!=null){ ?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_green.png';?>">
			<?php } else {?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_red.png';?>">
			<?php }?>

		</td>
	</tr>
	<tr>
		<td>
			<h2>Europeana search on ECTRL (europeanaFactedSearch)</h2>
			<?php if(SuperApiModel::test_europeana()!=null){ ?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_green.png';?>">
			<?php } else { ?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_red.png';?>">
			<?php } ?>

		</td>
		<td>
			<h2>Mesch search (meschMusiaES)</h2>
			<?php if(SuperApiModel::test_museia()!=null){ ?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_green.png';?>">
			<?php } else {?>
			<img src="<?php  echo Config::get('IMAGES_URL').'server_red.png';?>">
			<?php }?>

		</td>
	</tr>
</table>
</div>
