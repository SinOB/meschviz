<div class="container">

     <div class="login-page-box">
     <div class=" login-box">
        <h2><?php echo _('Set new password');?></h2>

        <!-- new password form box -->
        <form method="post" action="<?php echo Config::get('URL'); ?>login/setNewPassword" name="new_password_form">
            <input type='hidden' name='user_name' value='<?php echo $this->user_name; ?>' />
            <input type='hidden' name='user_password_reset_hash' value='<?php echo $this->user_password_reset_hash; ?>' />
            <label for="reset_input_password_new"><?php echo _('New password (min. 6 characters)');?></label>
            <input id="reset_input_password_new" class="reset_input" type="password"
                   name="user_password_new" pattern=".{6,}" required autocomplete="off" />
            <label for="reset_input_password_repeat"><?php echo _('Repeat new password');?></label>
            <input id="reset_input_password_repeat" class="reset_input" type="password"
                   name="user_password_repeat" pattern=".{6,}" required autocomplete="off" />
                   <br/>
            <input type="submit"  name="submit_new_password" value="<?php echo _('Submit new password');?>" />
        </form>

        <a href="<?php echo Config::get('URL'); ?>login/index"><?php echo _('Back to Login Page');?></a>
        </div>
    </div>
</div>
