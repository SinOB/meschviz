<div class="container">
    <h1><?php echo _('Upload an Avatar');?></h1>

    <div>
        <form action="<?php echo Config::get('URL'); ?>login/uploadAvatar_action" method="post" enctype="multipart/form-data">
            <label for="avatar_file"><?php echo _('Select an avatar image from your hard-disk. Once uploaded the image will be scaled to 44x44 px. We only accept .jpg images currently:');?></label>
            <input id='avatar_file' type="file" name="avatar_file" required />
            <!-- max size 5 MB (as many people directly upload high res pictures from their digital cameras) -->
            <input type="hidden" name="MAX_FILE_SIZE" value="5000000" /><br/>
            <input class='button' type="submit" value="<?php echo _('Upload image');?>" />
        </form>

        <div class="feedback info">
            <?php echo _("If you still see the old picture after uploading a new one: Hard-Reload the page with F5! Your browser doesn't realize there's a new image as new and old one have the same filename.");?>
        </div>
    </div>
	<br/><br/><br/>
    <div>
        <h1><?php echo _('Delete your avatar');?></h1>
        <p><?php echo _('Click this button to delete your avatar:');?> <br/><a href="<?php echo Config::get('URL'); ?>login/deleteAvatar_action">
        <input class='button' type="submit" value="<?php echo _('Delete your avatar');?>" />
        </a>
    </div>
</div>
