<div class="container outside">
	<div class='login-logo'></div>

	<div class="login-page-box">
    <div class="login-box">
		<h1><?php echo _('Enter Passcode'); ?></h1>
		<form action="<?php echo Config::get('URL'); ?>exhibition/view_public" method="get">
		<input id='passcode' type="text" name="passcode" required  oninvalid="this.setCustomValidity('<?php echo _("Please fill out this field"); ?> ')"/>
		<input type="submit" class="login-submit-button" value="<?php echo _('View visit'); ?>"/>
		</form>
    </div>
    </div>

	<div class="login-page-box">
            <!-- login box on left side -->
            <div class="login-box">
				<h1><?php echo _('Login'); ?></h1>
                <form action="<?php echo Config::get('URL'); ?>login/login" method="post">
                	<p>
                		<label for="user_name"><?php echo _('Username or email'); ?></label>
                		<br />
                   		<input id='user_name' type="text" name="user_name" required  oninvalid="this.setCustomValidity('<?php echo _("Please fill out this field"); ?> ')"/>

                    </p>
                    <p>
                    	<label for="user_password"><?php echo _('Password');?></label>
                    	<br />
                    	<input id='user_password' type="password" name="user_password" required oninvalid="this.setCustomValidity('<?php echo _("Please fill out this field"); ?> ')"/>

                    </p>
                    <input type="submit" class="login-submit-button" value="<?php echo _('Log in'); ?>"/>
                    <div class="link-forgot-my-password">
                    	<a href="<?php echo Config::get('URL'); ?>login/requestPasswordReset"><?php echo _('Forgot your password?'); ?></a>
                	</div>
                </form>


            </div>

    </div>
    <div class="login-page-box">
     <div class="login-box">
            <?php echo _('New to MeSch Magazine?') ?>
            <a href="<?php echo Config::get('URL'); ?>login/register" class='button'>
           <?php echo _('Sign Up'); ?>
            </a>
            </div>
    </div>
</div>
