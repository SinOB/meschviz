<div class="container">
    <h1><?php echo _('My profile');?></h1>

    <div>
        <div><?php echo _('My username:');?> <?= $this->user_name; ?></div>
        <div><?php echo _('My email:');?> <?= $this->user_email; ?></div>
        <div><?php echo _('My avatar image:');?>
            <?php if (Config::get('USE_GRAVATAR')) { ?>
                <img src='<?= $this->user_gravatar_image_url; ?>' alt='my gravatar image'/>
            <?php } else { ?>
                <img src='<?= $this->user_avatar_file; ?>' alt='my avatar image'/>
            <?php } ?>
        </div>
    </div>
	<br/><br/><br/>
	<h1><?php echo _('Edit my profile');?></h1>
    <ul class='profile'>
        <?php if (Session::userIsLoggedIn()) : ?>

            <li <?php if (View::checkForActiveController($filename, "login")) { echo ' class="active" '; } ?> >
                <a href="<?php echo Config::get('URL'); ?>login/editAvatar"><?php echo _('Edit my avatar');?></a>
            </li>
            <li <?php if (View::checkForActiveController($filename, "login")) { echo ' class="active" '; } ?> >
                <a href="<?php echo Config::get('URL'); ?>login/editusername"><?php echo _('Edit my username');?></a>
            </li>
            <li <?php if (View::checkForActiveController($filename, "login")) { echo ' class="active" '; } ?> >
                <a href="<?php echo Config::get('URL'); ?>login/edituseremail"><?php echo _('Edit my email');?></a>
            </li>
		<?php endif;  ?>
	</ul>
</div>
