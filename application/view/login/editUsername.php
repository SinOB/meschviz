<div class="container">
    <h1><?php echo _('Change your username');?></h1>

    <div>
        <form action="<?php echo Config::get('URL'); ?>login/editUserName_action" method="post">
            <!-- btw http://stackoverflow.com/questions/774054/should-i-put-input-tag-inside-label-tag -->
            <label>
                <?php echo _('New username:');?> <input type="text" name="user_name" required />
            </label>
            <input class='button' type="submit" value="<?php echo _('Submit');?>" />
        </form>
    </div>
</div>
