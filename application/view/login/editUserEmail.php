<div class="container">
    <h1><?php echo _('Change your email address');?></h1>

    <div>
        <form action="<?php echo Config::get('URL'); ?>login/editUserEmail_action" method="post">
            <label>
                <?php echo _('New email address:')?> <input type="text" name="user_email" required />
            </label>
            <input class='button' type="submit" value="<?php echo _('Submit');?>" />
        </form>
    </div>
</div>
