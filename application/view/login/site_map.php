<div class="container">
	<h1><?php echo _('Site Map');?></h1>

	<div class='site_map'>
	<?php if (Session::userIsLoggedIn()) { ?>
		<a href='<?php echo Config::get('URL'); ?>exhibition/index'><?php echo _('My Visits');?></a><br/>
		<a href='<?php echo Config::get('URL'); ?>login/showprofile'><?php echo _('Settings');?></a><br/>
		<a href='<?php echo Config::get('URL'); ?>login/statuses'><?php echo _('Statuses');?></a><br/>
		<a href='<?php echo Config::get('URL'); ?>login/logout'><?php echo _('Logout');?></a><br/>

		<?php if(UserModel::isCurator()) { ?>
			<a href='<?php echo Config::get('URL'); ?>visualisation/index'><?php echo _('My Visualisations');?></a><br/>
			<ul>
			<li><a href='<?php echo Config::get('URL'); ?>magazine/index'><?php echo _('My magazines');?></a></li>
			<li><a href='<?php echo Config::get('URL'); ?>timeline/index'><?php echo _('My timelines');?></a></li>
			<li><a href='<?php echo Config::get('URL'); ?>grid/index'><?php echo _('My grid visualisations');?></a></li>
			<li><a href='<?php echo Config::get('URL'); ?>map/index'><?php echo _('My map visualisations');?></a></li>
			<li><a href='<?php echo Config::get('URL'); ?>news/index'><?php echo _('News items');?></a></li>
			</ul>
		<?php } ?>

	<?php } else {?>
		<a href='<?php echo Config::get('URL'); ?>login/index'><?php echo _('Login');?></a><br/>
		<a href='<?php echo Config::get('URL'); ?>login/register'><?php echo _('Register');?></a><br/>

	<?php } ?>

		<a href="<?php echo Config::get('URL'); ?>login/cookies"><?php echo _("Privacy Policy");?></a><br/>
		<a href='<?php echo Config::get('URL'); ?>login/site_map'><?php echo _("Site Map");?></a><br/>
		<a href="<?php echo Config::get('URL'); ?>login/terms"><?php echo _("Terms & Conditions");?></a><br/>
	</div>
</div>