<div class='breadcrumbs'>
	<a href='<?php echo Config::get('URL'); ?>magazine'><?php echo _('Back to magazines');?></a> <<
	<a href='<?php echo Config::get('URL'); ?>magazine/edit/<?php echo $this->article->magazine_id; ?>'><?php echo _('Back to articles');?></a>
</div>

<div class="container">

	<h1><?php echo _('Add components to article');?> "<?php echo htmlspecialchars($this->article->article_title, ENT_QUOTES, 'UTF-8'); ?>"</h1>

    <form method='post' action="<?php echo Config::get('URL'); ?>article/editSave">
    	<label for="article_title"><?php echo _('Article Title:');?></label>
		<input type='hidden' name='article_id' value='<?php echo $this->article->article_id; ?>'>
		<input id='article_title' type='text' name='article_title' value='<?php echo htmlspecialchars($this->article->article_title, ENT_QUOTES, 'UTF-8'); ?>'>
		<input class='button' type='submit' value='<?php echo _('Update article title');?>'>

		<a href='<?= Config::get('URL') . 'magazine/view/' . $this->magazine->magazine_id; ?>'><input class='button right' type='button' value='<?php echo _('Preview');?>'></a>
	</form>


	<?php require Config::get('PATH_VIEW') .'magazine/media.php';?>


	<!-- START SHOW EXISTING COMPONENTS -->
	<h2><?php echo _('Current article contents');?></h2>
	<div id="actual_content" class="effects article_edit_single">
	<?php if (isset($this->components) && count($this->components)>0){ ?>
	<?php
    foreach($this->components as $component){
    	$result = ComponentModel::getComponentHtmlFromSource($component->source_type, $component->source_id, $component->text, $component->external_source);
    	?>
    	<div class='img article_component'>
    		<?php echo $result;?>
	    	<div class="overlay" onclick="deleteComponent('<?php echo $component->component_id; ?>','<?php echo $this->article->article_id; ?>');">
	    		<span>-</span>
	    	</div>
	    </div>

	<?php } } ?>
	</div>
	<!-- END SHOW EXISTING COMPONENTS -->


</div>

	<form method='post' id='createComponent' name='createComponent' action="<?php echo Config::get('URL'); ?>component/create">
		<input type='hidden' name='magazine_id' value='<?php echo $this->magazine->magazine_id; ?>'>
		<input type='hidden' name='article_id' id='article_id' value='<?php echo $this->article->article_id; ?>'>
		<input type='hidden' name='component_text' id ='component_text' value=''>
		<input type='hidden' name='license_id' id ='license_id' value=''>
		<input type='hidden' id='source_type' name='source_type' value="">
		<input type='hidden' id='source_id' name='source_id' value="">
		<input type='hidden' id='external_source' name='external_source' value="">
	</form>

	<form method='post' id='deleteComponentForm' name='deleteComponentForm' action="<?php echo Config::get('URL'); ?>component/delete">
		<input type='hidden' name='delete_component_component_id' id='delete_component_component_id' value=''>
		<input type='hidden' name='delete_component_article_id' id='delete_component_article_id' value=''>
	</form>


<br/><br/>



