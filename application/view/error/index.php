<div class="container">
    <h1><?php echo _('Page not found');?></h1>
    <div>
        <p class="red-text"><?php echo _('This page does not exist.');?></p>
    </div>
</div>
