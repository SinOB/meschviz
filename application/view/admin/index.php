<script type="text/javascript">

$(document).ready(function () {

	// Onload - for each table row - show or hide museum_id field depending on account type
	$("tr").each(function(){
		type = $(this).find('#user_account_type_id').val();
		// if user account id is set and has a type and that type is curator
		if(type != 'undefined' && type!='' && type!=2){
			// hide the div containing the input
			$(this).find("#curator_only_input").hide();
		}
	});

	// On change show or hide museum_id field depending on account type
	$('.account_type').on('change', function() {
		type = this.value;
		var row = $(this).parent().parent();

		if(type!=2)
		{
			// hide the div
 			row.find("#curator_only_input").hide();
 			// set the select input value to empty
 			row.find("#museum_id").val(0);
 		}else{
 	 		// show the div
			row.find("#curator_only_input").show();
		}
	});
});
</script>


<div class="container">
<h1><?php echo _("User Management");?></h1>
<div >

        <div>
        	<?php echo _("From here you can change a user account type to one of three levels; normal, curator and super admin.");?>
            <?php echo _("For curator level users it is possible to select the museum they may create visuaisations for."); ?>
        </div>
        <div>
            <table class='user_management'>
                <tr>
                    <td><?php echo _("Id");?></td>
                    <td><?php echo _("Avatar");?></td>
                    <td><?php echo _("Username");?></td>
                    <td><?php echo _("User's email");?></td>
                    <td><?php echo _("Activated");?></td>
                    <td><label for='user_account_type_id'><?php echo _("User Type");?></label></td>
                    <td><label for='museum_id'><?php echo _("Museum Access Level (Curator level only)");?></label></td>
                    <td><?php echo _("Update");?></td>
                </tr>
                <?php foreach ($this->users as $user) { ?>
					<tr>
	                    <form class='users' action="<?= config::get("URL"); ?>admin/updateAccountSettings" method="post">
	                        <td><?= $user->user_id; ?></td>
	                        <td class="avatar">
	                            <?php if (isset($user->user_avatar_link)) { ?>
	                                <img src="<?= $user->user_avatar_link; ?>" alt='Avatar for <?= $user->user_name; ?>'/>
	                            <?php } ?>
	                        </td>
	                        <td><?= $user->user_name; ?></td>
	                        <td><?= $user->user_email; ?></td>
	                        <td><?= ($user->user_active == 0 ? _('No') : _('Yes')); ?></td>


                        	<td>
                        	<?php  if (isset($this->user_account_types)){ ?>
                				<select name='user_account_type_id' class='account_type' id='user_account_type_id'>
                					<option value=""></option>
                					<?php foreach ($this->user_account_types as $account_type) {?>
										<option value="<?= $account_type->user_account_type_id; ?>" <?php echo $user->user_account_type==$account_type->user_account_type_id? "selected":""?> ><?php echo $account_type->description; ?></option>
									<?php }?>

								</select>
							<?php } ?>
							</td>
                        	<td >
                        		<div id='curator_only_input'>
	                        	<?php  if (isset($this->museums)){ ?>
	                				<select name='museum_id' id='museum_id'>
	                					<option value=""></option>
	                					<?php foreach ($this->museums as $museum) {?>
											<option value="<?php echo $museum->museum_id; ?>" <?php echo $user->user_museum==$museum->museum_id?"selected":"" ?>><?php echo $museum->description; ?></option>
										<?php }?>

									</select>
								<?php } ?>
								</div>
							</td>
                            <td>
                                <input type="hidden" name="user_id" value="<?= $user->user_id; ?>" />
                                <input class='button' type="submit" value='<?php echo _("Update");?>'/>
                            </td>
                        </form>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
