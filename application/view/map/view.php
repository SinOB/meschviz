<?php
list($width, $height) = getimagesize(Config::get( 'IMAGES_URL' ).'maps/'.$this->map->background_image);

$data =  $this->map->point_data;

$point_positions = json_decode ( $data, true);

$point_mapping = [];
	// get an array of start point dimensions
	for($i = 0; $i < count( $this->points )-1; $i ++) {
		foreach($point_positions as $point){
			if($point['title']==$this->points[$i]->id){
				$point_mapping[] = array(
					'source'=> $this->points[$i]->id,
					'target'=> $this->points[$i+1]->id,
					'x1'=>$point['x'],
					'y1'=>$point['y'],
				);
				continue;
			}
		}
	}

	// update with end point positions
	for($i = 0; $i < count($point_mapping); $i ++ ){
		foreach($point_positions as $point){
			if($point['title']==$point_mapping[$i]['target']){
				$point_mapping[$i]['x2'] = $point['x'];
				$point_mapping[$i]['y2'] = $point['y'];
				continue;
			}
		}
	}

?>

<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

<script>

$(function(){ // on dom ready

var height =587;
var width = 900;
var data = <?php echo $data; ?>;
var box_size = '0 0 '+width+' '+height

// Create the SVG
var svg = d3.select('#map')
	.append('svg')
	.attr('width', width)
	.attr('height', height)
	.attr('viewBox', box_size)
	.attr("preserveAspectRatio", "xMaxYMax meet")


svg.append("svg:image")
	.attr('x',0)
    .attr('y',0)
    .attr('height',height)
	.attr('width',width)
	.attr('xlink:href','<?php echo Config::get( 'IMAGES_URL' ).'maps/'.$this->map->background_image; ?>')

var elem = svg.selectAll("g")
	.data(data)



// Draw lines for the path the user has taken
<?php foreach($point_mapping as $point) {?>

    var line = svg.append("line")
    .style("stroke", "black")
    .attr("x1", <?php echo $point['x1'];?>)
    .attr("y1", <?php echo $point['y1'];?>)
    .attr("x2", <?php echo $point['x2'];?>)
    .attr("y2", <?php echo $point['y2'];?>)
    .attr('stroke-width', 5);

<?php }?>

/* Create and place the "blocks" containing the circle and the text */
var elemEnter = elem.enter()
 .append("g")
 .attr("transform", function(d){return "translate("+d.x+","+d.y+")"})
 .style("cursor", "pointer")


var w = 125;
var interval = 360/9;

var circle = elemEnter.append("circle")
.attr("r", 20 )
.attr("fill", "#FF4000")

elemEnter.append("text")
    .text(function(d){return d.title})
    .style('fill', '#000')

});

</script>

<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo _('Your visit mapped'); ?>
</div>
<div class="container">
<h1><?php echo _('Your visit mapped'); ?></h1>
<div id="map" ></div>
</div>
