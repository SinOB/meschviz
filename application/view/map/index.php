<div class="container">
    <h1><?php echo _('Maps');?></h1>
    <div>

        <h2><?php echo _('From here you can create a new map or edit an existing one.');?></h2>

            <form method="post" action="<?php echo Config::get('URL');?>map/create" enctype="multipart/form-data">
                <label for='title'><?php echo _('Title:');?> </label>
                <input id='title' type="text" name="title" />
                <br/>
                <label for='exhibition'><?php echo _('Exhibition:');?> </label>
                <?php  if (isset($this->exhibitions)){ ?>
                <select id='exhibition' name='exhibition_id'>
                	<option value=""></option>
                	<?php foreach ($this->exhibitions as $exhibition) {?>
						<option value="<?php echo $exhibition->exhibition_id; ?>"><?php echo $exhibition->title; ?></option>
					<?php }?>

				</select>
				<?php } ?>
				<br/>
				<label for='file_upload'><?php echo _('Map Image');?>:</label>
	    		<input type="file" name="file_upload" id="file_upload">

                <input type="submit" value='<?php echo _('Create');?>' autocomplete="off" class='button'/>
            </form>

    </div>

    <?php
        if (isset($this->maps) && count($this->maps)>0){
			$i = 0;
			$len = count($this->maps);

		    foreach ($this->maps as $map) {
		    	$i++;
		    	if ($i == 1) {
		    		?>
		    		<div class="shelf ">
						<ul class="books clearfix">
					<?php
		    	}
				?>
				<li id="mag_issue" class="book">
					<div class="front_image">
						<a href="<?= Config::get('URL') . 'map/edit/' . $map->map_id; ?>" alt='<?php echo _('edit');?>'>

						<img class="front centered-and-cropped" src="<?php echo Config::get( 'IMAGES_URL' ).'maps/'.$map->background_image;?>" alt="<?php echo $map->title;?>">
						<h2><span><?php echo htmlspecialchars($map->title, ENT_COMPAT, 'UTF-8');?> </span></h2>
						</a>
					</div>
					<a href="<?= Config::get('URL') . 'map/view/' . $map->map_id; ?>" title='<?php echo _('View')." ".$map->title;?>'><?php echo _('View');?></a>
					&nbsp;&nbsp;&nbsp;
					<a href="<?= Config::get('URL') . 'map/delete/' . $map->map_id; ?>" title='<?php echo _('Delete')." ".$map->title;?>'><?php echo _('Delete');?></a>
				</li>
				<?php if ($i == $len) { ?>
		    		</ul>
				</div>
				<?php
		      	}
		      	else if($i % 5==0)
		      	{
		      		// new shelf
		      		echo "</ul></div><div class='shelf'><ul class='books clearfix'>";
		      	}
		    }
		}
?>
</div>