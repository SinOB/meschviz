<?php

list($width, $height) = getimagesize(Config::get( 'IMAGES_URL' ).'maps/'.$this->map->background_image);
if(isset($this->map->point_data)) {
	$data =  $this->map->point_data;
}else{
	$points = array();
	foreach($this->content as $content){
		if(!isset($points[$content->point_of_interest])){
			$points[$content->point_of_interest] =1;
		}
	}
	$i=0;
	$data = '';

	foreach(array_keys($points) as $key){
		$i++;
		$y=(75 * $i);
		$x = 120;
		if ($i > 7){
			$y = 75 * ($i%7==0?7:$i%7);
			$x = ceil($i/7)*$x;
		}
		$data .="{title:'".$key."', x:".$x.",y:".$y."},";
	}

	$data = "[".$data."]";
}


?>
<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

<script>

$(function(){ // on dom ready

var height =587;
var width = 900;
var data = <?php echo $data; ?>;
var box_size = '0 0 '+width+' '+height

// Create the SVG
var svg = d3.select('#map')
	.append('svg')
	.attr('width', width)
	.attr('height', height)
	.attr('viewBox', box_size)
   .attr("preserveAspectRatio", "xMaxYMax meet")


svg.append("svg:image")
   .attr('x',0)
   .attr('y',0)
  .attr('height',height)
  .attr('width',width)
   .attr('xlink:href','<?php echo Config::get( 'IMAGES_URL' ).'maps/'.$this->map->background_image; ?>')


//Define drag beavior
var drag = d3.behavior.drag()
 .on("drag", dragmove);

var elem = svg.selectAll("g")
	.data(data)

/* Create and place the "blocks" containing the circle and the text */
var elemEnter = elem.enter()
 .append("g")
 .attr("transform", function(d){return "translate("+d.x+","+d.y+")"})
 .style("cursor", "pointer")
 .call(drag)

var w = 125;
var interval = 360/9;


var circle = elemEnter.append("circle")
.attr("r", 20 )
.attr("fill", "#FF4000")

elemEnter.append("text")
    //.attr("dx", function(d){return -20})
    .text(function(d){return d.title})
    .style('fill', '#000')

function dragmove(d) {
	  var x = d3.event.x;
	  var y = d3.event.y;
	  d3.select(this).attr("transform", "translate(" + x + "," + y + ")");
}

});

function submit_with_data(){

	var new_data = []

	d3.selectAll("g").each( function(d, i){
			var t =  d3.transform(d3.select(this).attr("transform"))
		    x = t.translate[0],
		    y = t.translate[1];
		    text = d3.select(this).text();
		    new_data.push({title:text, x:Math.round(x),y:Math.round(y)});
	})

	// submit the form
	var form = document.getElementById("map_form");
	json = JSON.stringify(new_data)
	form['point_data'].value = json ;
	form.submit();
}
</script>

<div class="container">

	<h1><?php echo sprintf(_("Edit map '%s'"),htmlspecialchars($this->map->title, ENT_QUOTES, 'UTF-8'));?> </h1>

    <form method='post' id='map_form' action="<?php echo Config::get('URL'); ?>map/update">
    	<label for='title'><?php echo _('Title:');?></label>
    	<input type='hidden' name='map_id' value='<?php echo $this->map->map_id; ?>'>
		<input id='title'  type='text' name='title' value='<?php echo htmlspecialchars($this->map->title, ENT_QUOTES, 'UTF-8'); ?>'>
		<br/>
		<?php echo _('Exhibition:');?> <?php echo $this->exhibition->title; ?>
		<br/><br/>
		<label for='status'><?php echo _('Status');?>:</label>
		 <select id='status' name='status'>
           	<option value=""></option>
			<option value="draft" <?php echo ($this->map->status=='draft')?'selected':''?> ><?php echo _("draft");?></option>
			<option value="<?php echo Config::get('PUBLIC_STATUS')?>" <?php echo ($this->map->status==Config::get('PUBLIC_STATUS'))?'selected':''?>><?php echo _(Config::get('PUBLIC_STATUS'));?></option>
		</select>
		<input type='hidden' value='' name='point_data' id='point_data'>
		<br/>
		<input class='button' type='button' onclick='submit_with_data()' value='<?php echo _('Update');?>'>
		<a href='<?= Config::get('URL') . 'map/view/' . $this->map->map_id; ?>'><input class='button right' type='button' value='<?php echo _('View');?>'></a>
		<br/><?php echo _("Drag the points with your mouse to where they should be on the map. Hit 'Update' to save.");?>
		<div id="map" style='border:1px solid orange;'></div>
		<input class='button' type='button' onclick='submit_with_data()' value='<?php echo _('Update');?>'>
	</form>
</div>






