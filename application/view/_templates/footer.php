<div style="clear:both"></div>


<div class="footer">
<a href="<?php echo Config::get('URL'); ?>login/cookies"><?php echo _("Privacy Policy");?></a> |
<a href='<?php echo Config::get('URL'); ?>login/site_map'><?php echo _("Site Map");?></a> |
<a href="<?php echo Config::get('URL'); ?>login/terms"><?php echo _("Terms & Conditions");?></a>
<?php if (Session::userIsLoggedIn()) { ?>
 | <a href="<?php echo Config::get('URL'); ?>login/statuses"><?php echo _('Statuses');?></a>
<?php } ?>
<br/><br/>

<?php echo _("meSch receives funding from the European Community's 7FP ICT for access to cultural resources GA 600851."); ?>&nbsp;
<?php echo _("Copyright &copy; meSch Project 2015"); ?> </div>
    </div><!-- close class="wrapper" -->
</body>
</html>