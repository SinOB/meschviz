<!doctype html>
<html lang="<?php echo setlocale( LC_MESSAGES, '0')=="C"?"en":substr (setlocale( LC_MESSAGES, '0'),0,2); ?>">
<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
	<meta name="description" content="meSch is a 4-year EU funded project. This website is developed as part of meSch and provides examples of post-visit visualisations for exhibition visitors.">

    <!-- CSS -->
    <?php if (Session::userIsLoggedIn() && (UserModel::isCurator() || UserModel::isAdmin())) { ?>
    	 <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/build.css" />
    <?php } ?>
     <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/main.css" />
 	<!-- plugin to allow pop-up on view pages-->



    <script src="<?php echo Config::get('URL'); ?>javascript/jquery-1.11.2.min.js"></script>
	<script src="<?php echo Config::get('URL'); ?>javascript/jquery.touchSwipe.min.js"></script>
    <script src="<?php echo Config::get('URL'); ?>javascript/main.js"></script>

    <title><?php echo _('MeSchViz');?></title>
</head>
<body>
	<a class='move_offscreen' href="#maincontent">Skip to main content</a>


        <!-- navigation -->
        <nav class="navigation ">
	        <ul class="left">
	            <?php if (Session::userIsLoggedIn()) { ?>
	             	<li <?php if (View::checkForActiveController($filename, "passcode")) { echo ' class="active" '; } ?> >
	                    <a href="<?php echo Config::get('URL'); ?>exhibition/index"><span class="icon-mesch">&nbsp;&nbsp;&nbsp;&nbsp;</span><?php echo _("My Visits");?></a>
	                </li>
	            	<?php if(UserModel::isCurator()) { ?>
	                 <li <?php if (View::checkForActiveController($filename, "visualisation")) { echo ' class="active" '; } ?> >
	                    <a href="<?php echo Config::get('URL'); ?>visualisation/index"><?php echo _("Manage Visualisations");?></a>
	                </li>
	                <?php }?>
	                <?php if(UserModel::isAdmin()) { ?>
	                 <li <?php if (View::checkForActiveController($filename, "admin")) { echo ' class="active" '; } ?> >
	                    <a href="<?php echo Config::get('URL'); ?>admin/index"><?php echo _("Manage Users");?></a>
	                </li>
	                 <li <?php if (View::checkForActiveController($filename, "admin")) { echo ' class="active" '; } ?> >
	                    <a href="<?php echo Config::get('URL'); ?>passcode/index"><?php echo _("Manage Passcodes");?></a>
	                </li>
	                <?php }?>

	                <li <?php if (View::checkForActiveController($filename, "login")) { echo ' class="active" '; } ?> >
                        <a href="<?php echo Config::get('URL'); ?>login/logout"><?php echo _('Logout');?></a>
                    </li>
	            <?php } else { ?>
	                <!-- for not logged in users -->
	                <?php // hide login button on login page
	                if (!View::checkForActiveControllerAndAction($filename, "login/index")) {
	                ?>
	                <li >
	                    <a href="<?php echo Config::get('URL'); ?>login/index"><?php echo _('Login');?></a>
	                </li>
					<?php } ?>
	            <?php } ?>

			</ul>

			<div class='language'>
				<form method='post' action="<?php echo Config::get('URL'); ?>language/change">
				        <label for="language_dropdown" class='hidden'><?php echo _('Language');?></label>
				        <select name='lang' id="language_dropdown" onkeypress='this.form.submit()' onchange='this.form.submit()'>
				        <option value=""  disabled="disabled" ><?php echo _('Language');?></option>
				    		<option value="en_GB" <?php echo (setlocale( LC_MESSAGES, '0')=='en_GB' ? 'selected':''); ?>>English</option>
				    		<option value="nl_NL" <?php echo (setlocale( LC_MESSAGES, '0')=='nl_NL' ? 'selected':''); ?>>Nederlands</option>
				    		<option value="it_IT" <?php echo (setlocale( LC_MESSAGES, '0')=='it_IT' ? 'selected':''); ?>>Italiano</option>
						</select>
				</form>
			</div>
			<?php if (Session::userIsLoggedIn()) { ?>
				<ul class="right">
					<li>
                        <a href="<?php echo Config::get('URL'); ?>login/showprofile"><?php echo _('Settings');?></a>
                    </li>
				</ul>
			<?php } ?>
		</nav>
<div id="maincontent"></div>
<div style="clear:both"></div>
	<div>
<?php $this->renderFeedbackMessages(); ?>

