<script src="<?php echo Config::get('URL'); ?>javascript/jquery.fittext.js"></script>
<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/timeline.css" />

<script language="JavaScript">

$( document ).ready(function() {
	var a = $(".timeline-block");
    $(window).on("scroll", function() {
    	a.each(function() {
        	var a = $(this);
            a.offset().top <= $(window).scrollTop() + .75 * $(window).height() && a.removeClass("is-hidden").addClass("animate")
        })
    })


    $("#fitText1").fitText(1.2);
});



</script>

<style>
.main_page {
	background: url('<?php echo Config::get('URL')?>/images/timelines/<?php echo $this->timeline->timeline_image; ?>') repeat;
}

.main_page #timeline_header {
	background: #548c1d url('<?php echo Config::get('URL') ?>/images/timelines/<?php echo $this->timeline->title_image; ?>') top center no-repeat;
	background-size: cover;
	position: relative;
	height: 100vh;
	margin: 0;
	padding: 0;
}

</style>

<div class='breadcrumbs'>
	<button onclick="history.go(-1);"><?php echo _('Back')?></button> <<
	<?php echo $this->timeline->title; ?>
</div>


<div class="main_page">
<div id="timeline_header" >
	<div class="header" id='dynamicDiv'>
		<h1 id='fitText1'><?php echo $this->timeline->title; ?></h1>
		<a href="#timeline" class="button fade-in-up"><?php echo $this->timeline->subtitle; ?></a>
	</div>
</div>

<?php if($this->timeline->type =='1' && isset($this->points)) { // Historical timeline
?>
<div id="timeline">
	<main>
		<div class="timeline-bar"></div>
		<?php foreach ($this->points as $point) { ?>
		<section class="timeline-block is-hidden">
			<div class="timeline-date">
				<p><?php echo $point->date; ?></p>
			</div>
			<div class="timeline-content">
				<h2><?php echo $point->title; ?></h2>
				<p class="description">
				<?php if(isset($point->external_source)) {?>
	<img class='timeline_image_preview' src=<?php echo $point->external_source?> alt='Image for timeline point' /><br/>
	<?php }?>
	<?php echo $point->description; ?></p>

			</div>
		</section>
		<?php } ?>
	</main>
</div>
<?php }?>

<?php if($this->timeline->type =='2' && isset($this->visit)) { // User timeline ?>
<div id="timeline">
	<main>
		<div class="timeline-bar"></div>
		<?php foreach ($this->visit as $point) { ?>

		<section class="timeline-block is-hidden">
			<div class="timeline-date">
				<p><?php echo str_replace("T"," ",$point->start_time); ?></p>
			</div>
			<?php $content = ContentModel::getContentWorkaround($this->timeline->exhibition_id, $point->audience, $point->language, $point->perspective, $point->id);?>
			<div class="timeline-content">
				<h2><?php echo str_replace("_", " ", $point->id); ?></h2>
				<p class="description"><span class='bold'><?php echo $content->title;?></span><br/>
				<?php echo sprintf(_('You viewed this item for %d minute(s) and %d second(s)'),gmdate("i",$point->duration),gmdate("s",$point->duration)); ?>
				<br/>
				<br/>
				<?php if (isset($content->url) && ($content->type=='image'||$content->type=='snapshot')) {?>
					<img src='<?php echo $content->url; ?>' class='timeline_image_preview'><br/>
				<?php } ?>

				<?php echo $content->text;?>
				</p>
			</div>
		</section>
		<?php } ?>
	</main>
</div>
<?php }?>
</div>
