<div class="container">
<h1><?php echo _('My timelines');?></h1>
    <div>

        <h2><?php echo _('From here you can create a new timeline or edit an existing one.');?></h2>
            <form method="post" action="<?php echo Config::get('URL');?>timeline/create">
                <label for='timeline_title'><?php echo _('Title of new timeline');?>: </label>
                <input type="text" name="timeline_title" id='timeline_title'/>
                <br/>
                <label for='timeline_subtitle'><?php echo _('Subtitle');?>: </label>
                <input type="text" name="timeline_subtitle" id='timeline_subtitle'/>
                <br/>
                <label for='timeline_type'><?php echo _('Type');?>: </label>
                <select id='timeline_type' name='timeline_type'>
                    <option value="">&nbsp;</option>
                	<option value="1"><?php echo _('Historical');?></option>
                	<option value="2"><?php echo _('User Visit');?></option>
                </select>
				<br/>
                <label for='exhibition_id'><?php echo _('Exhibition');?>: </label>
                <?php  if (isset($this->exhibitions)){ ?>
                <select id='exhibition_id' name='exhibition_id'>
                	<option value=""></option>
                	<?php foreach ($this->exhibitions as $exhibition) {?>
						<option value="<?php echo $exhibition->exhibition_id; ?>"><?php echo $exhibition->title; ?></option>
					<?php }?>

				</select>
				<?php } ?>
				<br/>
                <input type="submit" value='<?php echo _('Create this timeline');?>' autocomplete="off" class='button'/>
            </form>
    </div>
   <?php
    if (isset($this->timelines) && count($this->timelines) >0){
		    foreach ($this->timelines as $key => $value) {
		    	?>
				<div class="visualisations_container">
					<div class="visualisations">
						<div class="box text timeline load post text image_on title_on" >
							<div class="box_inner">
								<a href="<?= Config::get('URL') . 'timeline/edit/' . $value->timeline_id; ?>" alt='<?php echo _('edit');?>'>
								<h2 class="text_title"><?php echo htmlspecialchars($value->title, ENT_COMPAT, 'UTF-8');?></h2>
								<img class="box_single_image centered-and-cropped" src="<?php echo isset($value->title_image)? Config::get('IMAGES_URL').'timelines/'.$value->title_image: Config::get('IMAGES_URL').'blank_cover.jpeg';?>" alt="<?php echo $value->title;?>">
								</a>
							</div>
							<div class="info_bar">
								<div class="buttons">
										<div class='box_passcode'></div>
										<div class='box_date'>
											<a href="<?= Config::get('URL') . 'timeline/view/' . $value->timeline_id; ?>"><?php echo _('View');?></a>
											&nbsp;&nbsp;&nbsp;
											<a href="<?= Config::get('URL') . 'timeline/delete/' . $value->timeline_id; ?>"><?php echo _('Delete');?></a>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>

<?php     }
		} else {

?>
       <div><?php echo _('No timelines found.');?></div>
<?php } ?>

</div>