
<div class="container">
	<h1><?php echo _('Edit timeline');?> "<?php echo htmlspecialchars($this->timeline->title, ENT_QUOTES, 'UTF-8'); ?>"</h1>

    <form method='post' action="<?php echo Config::get('URL'); ?>timeline/updateTimeline" >
    	<input type='hidden' name='timeline_id' value='<?php echo $this->timeline->timeline_id; ?>'>
		<label for='title'><?php echo _('Title of timeline');?>: </label>
		<input id='title' type="text" name="title" value='<?php echo $this->timeline->title; ?>'/>
        <br/>
        <label for='timeline_subtitle'><?php echo _('Subtitle');?>: </label>
        <input id='timeline_subtitle' type="text" name="timeline_subtitle" value='<?php echo $this->timeline->subtitle; ?>'/>
        <br/>
        <label for='type'><?php echo _('Type');?>: </label>
        <select id='type' name='timeline_type'>
        	<option value="">&nbsp;</option>
           	<option value="1" <?php echo $this->timeline->type==1?'selected':''; ?> ><?php echo _('Historical');?></option>
           	<option value="2" <?php echo $this->timeline->type==2?'selected':''; ?> ><?php echo _('User Visit');?></option>
        </select>
		<br/>
		<label for='status'><?php echo _('Status');?>:</label>
		 <select id='status' name='timeline_status'>
           	<option value=""></option>
			<option value="draft" <?php echo ($this->timeline->status=='draft')?'selected':''?> ><?php echo _("draft");?></option>
			<option value="<?php echo Config::get('PUBLIC_STATUS')?>" <?php echo ($this->timeline->status==Config::get('PUBLIC_STATUS'))?'selected':''?>><?php echo _(Config::get('PUBLIC_STATUS'));?></option>
		</select>
		<br/>
		<?php echo _('Exhibition:');?><?php echo $this->exhibition->title; ?>
		<br/>
		<input type="submit" value='<?php echo _('Update timeline');?>' autocomplete="off" class='button'/>


		<a href='<?= Config::get('URL') . 'timeline/view/' . $this->timeline->timeline_id; ?>'><input class='button right' type='button' value='<?php echo _('Preview');?>'></a>
	</form>
	<br/><br/>
	<br/><br/>
	 <form method='post' action="<?php echo Config::get('URL'); ?>timeline/updateTitleImageFile" enctype="multipart/form-data">
	 	<input type='hidden' name='timeline_id' value='<?php echo $this->timeline->timeline_id; ?>'>
		<label for='title_image'><?php echo _('Title background image');?>: </label> <?php echo $this->timeline->title_image ?>
		<br/>
		<?php if(isset($this->timeline->title_image) && $this->timeline->title_image!='') {?>
		<a href='<?php echo Config::get('URL')?>/images/timelines/<?php echo $this->timeline->title_image; ?>'><img class='timeline_image_preview' src='<?php echo Config::get('URL')?>/images/timelines/<?php echo $this->timeline->title_image; ?>' alt='timeline title image'></a>
		<?php }?>
		<input type="file" name="title_image" id="title_image">
		<input type="submit" value='<?php echo _('Update timeline');?>' autocomplete="off" class='button'/>
		<a href="<?= Config::get('URL') . 'timeline/deleteTitleImage/' . $this->timeline->timeline_id; ?>">
		<input type="button" value='<?php echo _('Delete');?>' autocomplete="off" class='button'/></a>
	</form>
	<br/><br/>

	 <form method='post' action="<?php echo Config::get('URL'); ?>timeline/updateTimelineImageFile" enctype="multipart/form-data">
		<input type='hidden' name='timeline_id' value='<?php echo $this->timeline->timeline_id; ?>'>
		<label for='timeline_image'><?php echo _('Timeline background image');?>: </label> <?php echo $this->timeline->timeline_image ?>
		<br/>
		<?php if(isset($this->timeline->timeline_image) && $this->timeline->timeline_image!='') {?>
		<a href='<?php echo Config::get('URL')?>/images/timelines/<?php echo $this->timeline->timeline_image; ?>'><img class='timeline_image_preview' src='<?php echo Config::get('URL')?>/images/timelines/<?php echo $this->timeline->timeline_image; ?>' alt='timeline background image'></a>
		<?php } ?>
		<input type="file" name="timeline_image" id="timeline_image">
		<input type="submit" value='<?php echo _('Update timeline');?>' autocomplete="off" class='button'/>
		<a href="<?= Config::get('URL') . 'timeline/deleteTimelineImage/' . $this->timeline->timeline_id; ?>">
		<input type="button" value='<?php echo _('Delete');?>' autocomplete="off" class='button'/></a>
	</form>
	<br/><br/>
	<br/><br/>


	<?php if($this->timeline->type =='1') {?>
		<h2><?php echo _("Historical Timeline Points");?>:</h2>

		<table class='create_points' style="border-collapse: collapse">
		<tr>
		<th><?php echo _("Date");?></th>
		<th><?php echo _("Title");?></th>
		<th><?php echo _("Description");?></th>
		<th><?php echo _("Position");?></th>
		<th><?php echo _("Add/Edit/Delete");?></th>
		</tr>
		<tr>
		<td><form method='post' action="<?php echo Config::get('URL'); ?>point/create" >
			<input type='hidden' name='timeline_id' value='<?php echo $this->timeline->timeline_id; ?>'>

			<input type="text" name="date" id='add_date'/>
		</td><td>
			<input type="text" name="title" id='add_title'/>
		</td><td>
		</td><td>
			<input type="number" name="position" id='add_position'/>
		</td><td>
			<input type="submit" value='<?php echo _('Add point');?>' autocomplete="off" class='button'/>
		</form>
		</tr>
	<?php }?>

	<?php if( isset($this->points) && count($this->points)>0) {
		?>
		<?php foreach ($this->points as $point) { ?>
			<tr>
			<td><?php echo $point->date; ?></td>
			<td><?php echo $point->title; ?></td>
			<td><?php echo substr ($point->description,0,50).'...'; ?></td>
			<td><?php echo $point->position; ?></td>

			<td><a href="#dialog" name="modal">
			<a href='<?php echo Config::get('URL').'point/view/'.$point->point_id; ?>'>
			<input type="button" value='<?php echo _('Edit point');?>' class='button'/>
			</a>

			<a href="<?= Config::get('URL') . 'point/delete/' . $point->point_id.'/'.$point->timeline_id; ?>">
			<input type="button" value='<?php echo _('Delete');?>' class='button'/></a>

			</td>
			</tr>
<?php } ?>
		</table>
		<?php }?>
</div>

