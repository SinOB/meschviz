
<script type="text/javascript">

$(document).ready(function(){
	// add text counter
	var text_max = 500;

	var length = $('#update_description').val().length;
	length = text_max-length;

    $('#textarea_feedback').html(length + ' characters remaining');

    $('#update_description').keyup(function() {
        var text_length = $('#update_description').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' characters remaining');
    });
});

</script>


<div class='breadcrumbs'>
	<a href='<?php echo Config::get('URL'); ?>timeline'><?php echo _('Back to timelines');?></a> <<
	<a href='<?php echo Config::get('URL'); ?>timeline/edit/<?php echo $this->point->timeline_id; ?>'><?php echo _('Back to timeline');?></a>
</div>
<div class="container">
	<h1><?php echo _('Update Timeline Point');?> "<?php echo htmlspecialchars($this->point->title, ENT_QUOTES, 'UTF-8'); ?>"</h1>


	<?php require Config::get('PATH_VIEW') .'timeline/media.php';?>


	<?php if(isset($this->point->external_source)) {?>
	Point image: <img class='timeline_image_preview' src=<?php echo $this->point->external_source?> alt='Image for timeline point' />

	<input type="button" value='<?php echo _('Delete image');?>' class='button' onclick="updateTimelinePointImage('','','','');"/>
	<br/><br/>
	<?php }?>

	<form method='post' action="<?php echo Config::get('URL'); ?>point/update" >
		<input type='hidden' id='update_point_id' name='point_id' value='<?php echo $this->point->point_id; ?>'>
		<input type='hidden'  name='timeline_id' value='<?php echo $this->point->timeline_id; ?>'>
		<label for='update_date'><?php echo _('Date');?>: </label>
		<input id='update_date' type="text" name="date" value="<?php echo $this->point->date; ?>"/><br/>
		<label for='update_title'><?php echo _('Title');?>: </label>
		<input type="text" id='update_title' name="title" value='<?php echo $this->point->title; ?>'/><br/>
		<label for='update_description'><?php echo _('Description');?>: </label>
		<textarea rows="4" cols="40" maxlength='500' name="description" id='update_description'><?php echo $this->point->description; ?></textarea>
		<div id="textarea_feedback"></div><br/><br/>

		<label for='update_position'><?php echo _('Position');?>: </label>
		<input type="number" name="position" id='update_position' value='<?php echo $this->point->position; ?>'/><br/>
		<input type="submit" value='<?php echo _('Update point');?>' autocomplete="off" class='button'/>

		<a href='<?= Config::get('URL') . 'timeline/view/' . $this->point->timeline_id; ?>'><input class='button right' type='button' value='<?php echo _('Preview');?>'></a>
	</form>


	<form method='post' id='updateImage' name='updateImage' action="<?php echo Config::get('URL'); ?>point/update_image">
		<input type='hidden' name='point_id' value='<?php echo $this->point->point_id; ?>'>
		<input type='hidden' name='timeline_id' id='timeline_id' value='<?php echo $this->point->timeline_id; ?>'>
		<input type='hidden' name='license_id' id ='license_id' value=''>
		<input type='hidden' id='source_type' name='source_type' value="">
		<input type='hidden' id='source_id' name='source_id' value="">
		<input type='hidden' id='external_source' name='external_source' value="">
	</form>
</div>

