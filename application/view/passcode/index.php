<?php ?>

<div class="container">
    <h1><?php echo _('Manage Passcodes');?></h1>
    <table class='create_articles'>
    <tr>
    	<th><?php echo _("Passcode");?></th>
    	<th><?php echo _("User");?></th>
    	<th><?php echo _("Date Created");?></th>
    	<th><?php echo _("Exhibition");?></th>
    	<th><?php echo _("Delete");?></th>
    </tr>
    <?php foreach($this->passcodes as $passcode) {?>
    <tr>
    	<td><?php echo $passcode->passcode; ?></td>
    	<td><?php echo $passcode->user_id; ?></td>
    	<td><?php echo $passcode->date_created; ?></td>
    	<td><?php echo $passcode->exhibition_id; ?></td>
    	<td><a href='<?php echo Config::get('URL'); ?>passcode/delete/<?php echo $passcode->passcode_id; ?>'><input type='button' class='button' value='<?php echo _("Delete");?>'></a></td>
    </tr>

    <?php }?>
    </table>
</div>