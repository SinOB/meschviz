<?php $permission = $this->exhibition-> display_visualisations_bitmask; ?>
<script src="<?php echo Config::get('URL'); ?>javascript/jquery.colorbox-min.js"></script>
<script src="<?php echo Config::get('URL'); ?>/javascript/masonry.pkgd.min.js"></script>

<link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/colorbox.css" />

<!-- pop-ip div content for when user clicks on unavailable visualisation when not logged in -->
<div style='display:none'>
	<div id='inline_content'>
	<div class="bubble_content" style="text-align:center;">
		<h1><?php echo _("Please login to view this content.")?></h1>
			<p>
			<a href="<?php echo Config::get('URL'); ?>login/register" class='button'><?php echo _('Register'); ?></a>
			<a href="<?php echo Config::get('URL'); ?>login/index"  class='button'><?php echo _('Login');?></a>
			</p>
	</div>
	</div>
</div>


<script type="text/javascript">

$( document ).ready(function() {

	// prepare hidden colorbox - make it fixed to get around issue where jumps on tab opening
	$(".inline").colorbox({inline:true, width:"400px", fixed: true});

	// If user has attempted to open a visualisation in a new tab but is not logged in - display colorbox modal
	if(window.location.hash=='#inline_content'){
		$('.inline').colorbox({open:true})
	}


	// once page loaded move divs so images don't overlap
	var $container = $('.visualisations');
	$container.masonry({
		itemSelector:'.box',
		columnWidth:160,
		isFitWidth: true
	});
});
</script>


<div class='breadcrumbs'>
<?php if (Session::userIsLoggedIn()) {?>
	<a href='<?php echo Config::get('URL'); ?>exhibition'><?php echo _('My Visits');?></a> <<
<?php }?>
	<?php echo $this->exhibition->title; ?>
</div>

<div class="visualisations_container">
	<div class="visualisations">

		<!-- Double sized exhibition box -->

		<?php if(ExhibitionModel::canDisplayVisualisation("summary",$permission)) {?>
		<div  class="box text load double image_on title_on">
			<div class="box_inner">
			<?php if(isset($this->exhibition->exhibition_url) && $this->exhibition->exhibition_url!=''){?>
				<a target="_blank"  href ='<?php echo $this->exhibition->exhibition_url; ?>'>
				<h2 class="text_title"><?php echo $this->exhibition->title; ?></h2>
				</a>
			<?php } else {?>
				<h2 class="text_title"><?php echo $this->exhibition->title; ?></h2>
			<?php } ?>
				<?php //if( $this->exhibition->code=='16'){?>
				<!-- <img class='box_double_image centered-and-cropped' src="<?php echo Config::get('IMAGES_URL'); ?>/exhibitions/doubleWw1.png" > -->
				<?php //} else
					if($this->exhibition->code=='17') {?>
				<img class='box_double_image centered-and-cropped' src="<?php echo Config::get('IMAGES_URL'); ?>/exhibitions/doubleAwall.png" >
				<?php } elseif($this->exhibition->code=='31') {?>
				<img class='box_double_image centered-and-cropped' src="<?php echo Config::get('IMAGES_URL'); ?>/exhibitions/doubleFap.png" >
				<?php } elseif($this->exhibition->code=='1013') {?>
				<img class='box_double_image centered-and-cropped' src="<?php echo Config::get('IMAGES_URL'); ?>/exhibitions/doubleKeys.png" >
				<?php } else {?>
				<img class='box_double_image centered-and-cropped' src="<?php echo $this->exhibition->image;?>">
				<?php }?>
				<p><?php echo $this->exhibition->description;?></p>
			</div>
			<div class="info_bar">
				<div class="buttons">
						<div class='box_date'>
						<?php if (isset( $this->passcodes )) {
							foreach ( $this->passcodes as $passcode ) {
								echo isset($passcode->date_created)?date("d/m/Y", strtotime($passcode->date_created)).', ':"";
							}
						} ?></div>
				</div>
			</div>
		</div>
		<?php } ?>

		<!-- Single sized news box -->
		<?php if (isset($this->news) && count($this->news)>0) {?>
		<div class="box text load image_on title_on" >
			<div class="box_inner">
			<h2 class="text_title"><?php echo _("Exhibition Updates");?></h2>

			<?php foreach($this->news as $news) {?>
				<h3 class='news_header'><?php echo $news->title;?>(<?php echo date("jS M Y H:i", strtotime($news->date_created));?>)</h3>
			<p>
				<?php echo $news->text;?>
			</p>
			<?php } ?>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_date'></div>
				</div>
			</div>
		</div>
		<?php }?>


		<?php if (isset( $this->passcodes )) {
			foreach ( $this->passcodes as $passcode ) { ?>

			<?php if(ExhibitionModel::canDisplayVisualisation("svgcharacter",$permission)) {?>
			<!-- Svg test character -->
			<div class="box text load image_on title_on" >
			<div class="box_inner">
				<h2 class="text_title"><?php echo _('Exhibition Personality');?></h2>
			 	<iframe height='300px' src="<?php echo Config::get('URL');?>visualisation/svgCharacter/<?php echo $passcode->passcode; ?>"></iframe>
			 	<p>This is your personal avatar, automatically generated for you on the basis of your visit.</p>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_passcode'><?php echo $passcode->passcode;?></div>
					<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
				</div>
			</div>
			</div>
			<?php } ?>

			<!-- Unseen-->
			<?php if (ExhibitionModel::canDisplayVisualisation("unseen",$permission)) { ?>
			<div class="box quote load" >
			<div class="box_inner">
				<a href="<?php echo Config::get('URL');?>visualisation/unseen/<?php echo $passcode->passcode; ?>" alt='view'>
				<p>
				<?php echo _('Everything you have missed');?>
				</p>
				</a>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_passcode'><?php echo $passcode->passcode;?></div>
					<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
				</div>
			</div>
			</div>
			<?php } ?>

			<?php if(ExhibitionModel::canDisplayVisualisation("passport", $permission) && $this->exhibition->code =='17') { // only display for atlantikwall?>
			<div class="box text  load text image_on title_on" >
				<div class="box_inner">
				<?php if (!Session::userIsLoggedIn()) {?>
					<a class='inline' href="#inline_content" >
				<?php } else {?>
					<a href="<?php echo Config::get('URL');?>visualisation/stamps/<?php echo $passcode->passcode;?>" alt='view'>
				<?php } ?>
				<h2 class="text_title"><?php echo _('Visit Passport');?></h2>
				<img width='310px' height='350px' class="box_single_image centered-and-cropped" src="<?php echo Config::get('IMAGES_URL'); ?>/visuals/passbook.jpeg" alt="<?php echo $passcode->passcode;?>">
				</a>
				</div>
				<div class="info_bar">
					<div class="buttons">
						<div class='box_passcode'><?php echo $passcode->passcode;?></div>
						<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
					</div>
				</div>
			</div>
			<?php } ?>


			<!-- Favourites-->
			<?php
			if(ExhibitionModel::canDisplayVisualisation("favourite",$permission)) {
				//Get text from favorite item
				$favorite = VisitLogModel::getFavoritePoint($passcode->passcode);
				if(isset($favorite)){
					$favoriteContent = ContentModel::getContentWorkaround($this->exhibition->exhibition_id, $favorite->audience, $favorite->language, $favorite->perspective, $favorite->id);
			?>
			<div class="box text  load text image_on title_on" >
			<div class="box_inner">
				<a href="<?php echo Config::get('URL');?>visualisation/favorite/<?php echo $passcode->passcode; ?>" alt='view'>
				<h2 class="text_title"><?php echo _('Your Favourites');?></h2>
				<h3 class='news_header'><?php echo isset($favoriteContent)?$favoriteContent->title:'';?></h3>
				<p><?php echo substr($favoriteContent->text, 0 ,300)."..." ; ?>
				<div class="read_more"><?php echo _("Read more");?></div></p>
				</a>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_passcode'><?php echo $passcode->passcode;?></div>
					<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
				</div>
			</div>
			</div>
			<?php } } ?>


			<!-- Inspiration -->
			<?php if(ExhibitionModel::canDisplayVisualisation("inspiration",$permission)) { ?>
			<div class="box text  load image_on title_on" >
			<div class="box_inner">
				<a href="<?php echo Config::get('URL');?>visualisation/inspiration/<?php echo $passcode->passcode; ?>" alt='view'>
				<h2 class="text_title"><?php echo _('Inspired by your visit');?></h2>

				<?php $inspiration = PersonalisationModel::getVisitPersonalisation($passcode->passcode, 5, null, null);?>
				<div class="photoset-grid" >
						<div class="photoset-row cols-1">
							<div class="photoset-cell photoset-cell_fullwidth">
								<img height='300px' width='310px' src="<?php echo isset($inspiration->suggestions[0]['external_source'])?($inspiration->suggestions[0]['external_source']): Config::get('IMAGES_URL').'blank_cover.jpeg' ;?>" class='centered-and-cropped'>
							</div>
						</div>
						<?php if(isset($inspiration) && count($inspiration->suggestions)>=3) { ?>
						<div class="photoset-row cols-2">
							<div class="photoset-cell photoset-cell_halfwidth1">
								<img height='200px' src="<?php echo isset($inspiration->suggestions[1]['external_source'])? $inspiration->suggestions[1]['external_source']: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
									class='centered-and-cropped'>
							</div>
							<div class="photoset-cell photoset-cell_halfwidth2">
								<img height='200px' src="<?php echo isset($inspiration->suggestions[2]['external_source'])? $inspiration->suggestions[2]['external_source']: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
									class='centered-and-cropped'>
							</div>
						</div>
						<?php }?>
				</div>
				</a>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_passcode'><?php echo $passcode->passcode;?></div>
					<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
				</div>
			</div>
			</div>
			<?php } ?>


			<!-- UoA -->
			<?php if(ExhibitionModel::canDisplayVisualisation("personalisation",$permission)) { ?>
			<div class="box quote load" >
			<div class="box_inner">
				<a href="<?php echo Config::get('URL');?>visualisation/personalisation/<?php echo $passcode->passcode; ?>" alt='view'>
				<p class='smaller'>
				<?php echo _('UoA <br/>recommender');?>
				</p>
				</a>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_passcode'><?php echo $passcode->passcode;?></div>
					<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
				</div>
			</div>
			</div>
			<?php } ?>

			<?php
			if (isset( $this->user_visit_timelines )) {
				foreach ( $this->user_visit_timelines as $timeline ) {
					if($timeline->status == Config::get('PUBLIC_STATUS')) {?>

			<!-- User visit timeline -->
			<div class="box text load image_on title_on" >
				<div class="box_inner">
				<?php if (!Session::userIsLoggedIn()) {?>
							<a class='inline' href="#inline_content" >
						<?php } else {?>
							<a href="<?= Config::get('URL') . 'timeline/view/' . $timeline->timeline_id.'/'.$passcode->passcode; ?>" alt='view'>
						<?php } ?>

						<h2 class="text_title"><?php echo _('Timeline');?> - <?php echo $timeline->title;?></h2>
						<img  width='310px' height='350px' class="box_single_image centered-and-cropped" src="<?php echo isset($timeline->title_image)? Config::get('IMAGES_URL').'timelines/'.$timeline->title_image: Config::get('IMAGES_URL').'blank_cover.jpeg';?>" alt="<?php echo $timeline->title;?>">
						</a>
				</div>
				<div class="info_bar">
					<div class="buttons">
						<div class='box_passcode'><?php echo $passcode->passcode;?></div>
						<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
					</div>
				</div>
			</div>
			<?php } } } ?>

			<?php /*if(ExhibitionModel::canDisplayVisualisation("path",$permission)) {?>
			<!-- Single sized animal path visualisation box -->
			<!-- Daniela requested this visualisation be removed :-(-->
			<div class="box text load image_on title_on" >
				<div class="box_inner">
				<?php if (!Session::userIsLoggedIn()) {?>
					<a class='inline' href="#inline_content" >
				<?php } else {?>
					<a href="<?php echo Config::get('URL');?>visualisation/path/<?php echo $passcode->passcode; ?>" alt='view'>
				<?php }?>
					<h2 class="text_title"><?php echo _('Animal Path');?></h2>
					<img width='310px' height='350px' class='box_single_image centered-and-cropped' src="<?php echo Config::get('IMAGES_URL'); ?>/visuals/path.jpg" alt="<?php echo $passcode->passcode;?>">
					</a>
				</div>
				<div class="info_bar">
					<div class="buttons">
							<div class='box_passcode'><?php echo $passcode->passcode;?></div>
							<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?>
							</div>
					</div>
				</div>
			</div>

 			<?php  }*/ ?>

			<!-- Single sized bubbles visualisation -->
			<?php if(ExhibitionModel::canDisplayVisualisation("bubbles",$permission)) { ?>
			<div class="box text load image_on title_on" >
				<div class="box_inner">
				<?php if (!Session::userIsLoggedIn()) {?>
					<a class='inline' href="#inline_content" >
				<?php } else {?>
					<a href="<?php echo Config::get('URL');?>visualisation/bubbles/<?php echo $passcode->passcode;?>" alt='view'>
				<?php } ?>
					<h2 class="text_title"><?php echo _('MeSch Bubbles');?></h2>
					<img width='310px' height='350px' class='box_single_image centered-and-cropped' src="<?php echo Config::get('IMAGES_URL'); ?>/visuals/bubbles.jpg" alt="<?php echo $passcode->passcode;?>">
					<p><?php echo _('The character in the centre represents your visit and the dots around it the content of the exhibition. Click on the dots to discover what you have seen and what you missed.' );?></p>
				</a>
				</div>
				<div class="info_bar">
					<div class="buttons">
						<div class='box_passcode'><?php echo $passcode->passcode;?></div>
						<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
					</div>
				</div>
			</div>
			<?php } ?>

			<?php if(ExhibitionModel::canDisplayVisualisation("cloud",$permission)) {?>
			<!-- double sized embedded word cloud visualisation -->
			<div class="box text load double image_on title_on" >
				<div class="box_inner">
					<h2  class="text_title"><?php echo _('Word Cloud');?></h2>
						<iframe height='300px' width='620' src="<?php echo Config::get('URL');?>visualisation/cloud/<?php echo $passcode->passcode; ?>"></iframe>
				</div>
				<div class="info_bar">
					<div class="buttons">
						<div class='box_passcode'><?php echo $passcode->passcode;?></div>
						<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
					</div>
				</div>
			</div>
			<?php } ?>


			<?php if(ExhibitionModel::canDisplayVisualisation("album",$permission)) { ?>
		<!-- Photo album -->
		<div class="box text load image_on title_on" >
			<div class="box_inner">
			<?php if (!Session::userIsLoggedIn()) {?>
					<a class='inline' href="#inline_content" >
				<?php } else {?>
					<a href="<?php echo Config::get('URL');?>visualisation/album/<?php echo $passcode->passcode;?>" alt='view'>
				<?php } ?>
					<h2 class="text_title"><?php echo _('Photo Album');?></h2>
					<?php $photoAlbumImage = ContentModel::getSingleContentImageByExhibition($this->exhibition->exhibition_id);?>
					<img width='310px' height='350px' class="box_single_image centered-and-cropped" src="<?php echo isset($photoAlbumImage->url)?$photoAlbumImage->url:Config::get('IMAGES_URL').'/visuals/album_cover.jpg'; ?>" alt="<?php echo $passcode->passcode;?>">
					</a>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_passcode'><?php echo $passcode->passcode;?></div>
					<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
				</div>
			</div>
		</div>
		<?php } ?>


		<?php if (isset( $this->magazines )) {
				foreach ( $this->magazines as $key => $value ) {
					if($value->status == Config::get('PUBLIC_STATUS')) {?>


		<div class="box text load image_on title_on" >
			<div class="box_inner">
				<?php if (!Session::userIsLoggedIn()) {?>
					<a class='inline' href="#inline_content" >
				<?php } else {?>
					<a href="<?= Config::get('URL') . 'magazine/view/' . $value->magazine_id.'&passcode='.$passcode->passcode; ?>" alt='view'>
				<?php }?>
				<h2 class="text_title"><?php echo _('Magazine');?> - <?php echo $value->magazine_title;?></h2>

				<div class="photoset-grid" >
						<div class="photoset-row cols-1">
							<div class="photoset-cell photoset-cell_fullwidth">
								<img width='310px' height='300px' src="<?php echo isset($value->front_images)? $value->front_images[0]->external_source: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
								 	class='centered-and-cropped'>
							</div>
						</div>
						<?php if(isset($value->front_images) && count($value->front_images)>=3) { ?>
						<div class="photoset-row cols-2">
							<div class="photoset-cell photoset-cell_halfwidth1">
								<img height='200px' src="<?php echo isset($value->front_images)? $value->front_images[1]->external_source: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
									class='centered-and-cropped'>
							</div>
							<div class="photoset-cell photoset-cell_halfwidth2">
								<img height='200px' src="<?php echo isset($value->front_images)? $value->front_images[2]->external_source: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
									class='centered-and-cropped'>
							</div>
						</div>

						<?php if(count($value->front_images)>=6) {?>
						<div class="photoset-row cols-3">
							<div class="photoset-cell photoset-cell_thirdwidth_first">
								<img height='100px' src="<?php echo isset($value->front_images)? $value->front_images[3]->external_source: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
									class='centered-and-cropped'>
							</div>
							<div class="photoset-cell photoset-cell_thirdwidth_second">
								<img height='100px' src="<?php echo isset($value->front_images)? $value->front_images[4]->external_source: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
									class='centered-and-cropped'>
							</div>
							<div class="photoset-cell photoset-cell_thirdwidth_third">
								<img height='100px' src="<?php echo isset($value->front_images)? $value->front_images[5]->external_source: Config::get('IMAGES_URL').'blank_cover.jpeg';?>"
									class='centered-and-cropped'>
							</div>
						</div>
						<?php } }?>
				</div>
			</a>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_passcode'><?php echo $passcode->passcode;?></div>
					<div class='box_date'><?php echo isset($passcode->date_created)?date("d M Y", strtotime($passcode->date_created)):""; ?></div>
				</div>
			</div>
		</div>

		<?php } } } ?>

		<?php }}


		// historical timeline visualisation unrelated to passcodes
if (isset( $this->historical_timelines ))
{
	foreach ( $this->historical_timelines as $timeline ) {
		if($timeline->status == Config::get('PUBLIC_STATUS')) {?>

		<div class="box text load image_on title_on" >
			<div class="box_inner">
			<?php if (!Session::userIsLoggedIn()) {?>
					<a class='inline' href="#inline_content" >
				<?php } else {?>
					<a href="<?= Config::get('URL') . 'timeline/view/' . $timeline->timeline_id; ?>" alt='view'>
				<?php }?>
				<h2 class="text_title"><?php echo _('Timeline');?> - <?php echo $timeline->title;?></h2>
				<img width='310px' height='350px' class="box_single_image centered-and-cropped" src="<?php echo isset($timeline->title_image)? Config::get('IMAGES_URL').'timelines/'.$timeline->title_image: Config::get('IMAGES_URL').'blank_cover.jpeg';?>" alt="<?php echo $timeline->title;?>">
				</a>
			</div>
			<div class="info_bar">
				<div class="buttons">
					<div class='box_date'></div>
				</div>
			</div>
		</div>
		<?php } } } ?>

		<?php if (!Session::userIsLoggedIn()) {?>
		<div class="box text load image_on title_on" >
			<div class="box_inner">
				<h2 class="text_title"><?php echo _('Click here to register');?></h2>
				<p><a href="<?php echo Config::get('URL'); ?>login/register" class='button'>
 				<?php echo _('Register'); ?>
            	</a>
            	</p>
            </div>
            <div class="info_bar">
				<div class="buttons">
					<div class='box_date'></div>
				</div>
			</div>
		</div>
		<?php }?>



	<div>
</div>


	<!-- <h1><?php echo sprintf(_('Thank you for visiting the %s exhibition.'),_($this->exhibition->title));?>&nbsp;
	<?php echo _('This exhibition has the following visualisations');?>:</h1> -->


</div></div>
