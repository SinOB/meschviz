<?php
$permission = $this->exhibition->display_visualisations_bitmask;
?>

<div class="container">
<h1><?php echo $this->exhibition->title; ?></h1>
<h2><?php echo _("Edit display permissions for visualisations for this exhibition"); ?></h2>
<p><?php echo _("Check the box next to the visualisation name and click the 'update' button to enable display of this visualisation for this exhibition. The user will only see one instance of each visualisation per passcode.");?></p>
 	<form method="post" action="<?php echo Config::get('URL');?>exhibition/updateVisualisationDisplaySettings">
 		<input type='hidden' name='exhibition_id' value='<?php echo $this->exhibition->exhibition_id; ?>'  >
		<input type="checkbox" name="visualisation_permission[]" value="summary"
			<?php echo ExhibitionModel::canDisplayVisualisation("summary",$permission)?"checked":''; ?> >
			<?php echo _('Exhibition Summary');?><br/>
		<input type="checkbox" name="visualisation_permission[]" value="svgcharacter"
			<?php echo ExhibitionModel::canDisplayVisualisation("svgcharacter",$permission)?"checked":''; ?> >
			<?php echo _('Exhibition Personality');?><br/>
			<?php if($this->exhibition->code =='17') { // only display for atlantikwall ?>
		<input type="checkbox" name="visualisation_permission[]" value="passport"
			<?php echo ExhibitionModel::canDisplayVisualisation("passport",$permission)?"checked":''; ?> >
			<?php echo _('Visit Passport');?><br/>
			<?php } ?>
		<input type="checkbox" name="visualisation_permission[]" value="unseen"
			<?php echo ExhibitionModel::canDisplayVisualisation("unseen",$permission)?"checked":''; ?> >
			<?php echo _('Everything you have missed');?><br/>
		<input type="checkbox" name="visualisation_permission[]" value="favourite"
			<?php echo ExhibitionModel::canDisplayVisualisation("favourite",$permission)?"checked":''; ?> >
			<?php echo _('Your Favourites');?><br/>
		<input type="checkbox" name="visualisation_permission[]" value="inspiration"
			<?php echo ExhibitionModel::canDisplayVisualisation("inspiration",$permission)?"checked":''; ?> >
			<?php echo _('Inspired by your visit');?><br/>
		<input type="checkbox" name="visualisation_permission[]" value="personalisation"
			<?php echo ExhibitionModel::canDisplayVisualisation("personalisation",$permission)?"checked":''; ?> >
			<?php echo _('UoA recommender');?><br/>
		<!-- <input type="checkbox" name="visualisation_permission[]" value="path"
			<?php echo ExhibitionModel::canDisplayVisualisation("path",$permission)?"checked":''; ?> >
			<?php echo _('Animal Path');?><br/> -->
		<input type="checkbox" name="visualisation_permission[]" value="bubbles"
			<?php echo ExhibitionModel::canDisplayVisualisation("bubbles",$permission)?"checked":''; ?> >
			<?php echo _('MeSch Bubbles');?><br/>
		<input type="checkbox" name="visualisation_permission[]" value="cloud"
			<?php echo ExhibitionModel::canDisplayVisualisation("cloud",$permission)?"checked":''; ?> >
			<?php echo _('Word Cloud');?><br/>
		<input type="checkbox" name="visualisation_permission[]" value="album"
			<?php echo ExhibitionModel::canDisplayVisualisation("album",$permission)?"checked":''; ?> >
			<?php echo _('Photo Album');?><br/>
		<!-- <input type="checkbox" name="visualisation_permission[]" value="photoperson"
			<?php echo ExhibitionModel::canDisplayVisualisation("photoperson",$permission)?"checked":''; ?> >
			<?php echo _('Photo Person');?><br/> -->
		<br/>
		<input type="submit" value='<?php echo _('Update');?>' autocomplete="off" class='button'/>
	</form>
</div>


