<div class="container">
	<h1><?php echo _('Get your visit');?></h1>
	<div class="add_passcode">
        <span class='bold'><?php echo _('From here you can add a visit passcode and view your visit information');?>.</span>
        <p>
            <form method="post" action="<?php echo Config::get('URL');?>passcode/create">
                <label for='passcode'><?php echo _("Your passcode is the 11 characer alphanumberic string on your ticket in the format 'ABC-123-1234'");?> </label>
                <input id='passcode' type="text" name="passcode" placeholder="Enter your ticket passcode"/>

                <input type="submit" value='<?php echo _('Add this passcode');?>' autocomplete="off" class='button'/>
            </form>
        </p>
    </div>

    <h1><?php echo _('Exhibitions you have visited');?></h1>

<?php
if (isset($this->exhibitions)){
	foreach ($this->exhibitions as $exhibition) {
	?>

	<div class="visualisations_container">
		<div class="visualisations">
			<div class="box exhibition text  load post text image_on title_on" >
				<div class="box_inner">
					<a href="<?= Config::get('URL') . 'exhibition/view/' . $exhibition->exhibition_id; ?>">
					<h2 class="text_title"><?php echo $exhibition->title;?></h2>
					<img class="box_single_image centered-and-cropped" src="<?php echo (isset($exhibition->image)&& $exhibition->image!='' ? $exhibition->image : Config::get('IMAGES_URL').'blank_cover.jpeg'); ?>" alt="<?php echo $exhibition->title;?>">
					</a>
				</div>
				<div class="info_bar">
					<div class="buttons">
							<div class='box_passcode'><?php echo sprintf(_('%s passcode(s)'), $exhibition->passcode_count); ?></div>
							<div class='box_date'>
							<?php $news = NewsModel::getNewsByExhibition($exhibition->exhibition_id);
							if(count($news)>0){
								if (strtotime($news[0]->date_created) > Session::get ( 'user_previous_login_timestamp' )){
									echo _("Updated!!");
			    				}
							}
			    			?>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	}
}
?>

</div>
