  <link rel="stylesheet" href="<?php echo Config::get('URL'); ?>css/jquery-ui.css">
  <script src="<?php echo Config::get('URL'); ?>javascript/jquery-ui.js"></script>
  <script>
  $(function() {
    $( "#start" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#end" ).datepicker({ dateFormat: 'dd-mm-yy' });
  });
  </script>

<div class="container">
    <h1><?php echo _('News items');?></h1>
    <div>

        <h2><?php echo _('From here you can create a new news items and delete existing ones.');?></h2>

		<form method="post" action="<?php echo Config::get('URL');?>news/create">
                <label for='title'><?php echo _('Title');?>: </label>
                <input id='title' type="text" name="title" required/>
                <br/>
                <label for='text'><?php echo _('Text');?>: </label>
                <textarea id='text' name="text" rows="6" cols="80" required ></textarea>
                <br/> <br/>
                <label for='exhibition'><?php echo _('Exhibition');?>: </label>
                <?php  if (isset($this->exhibitions)){ ?>
                <select id='exhibition' name='exhibition_id' required >
                	<option value=""></option>
                	<?php foreach ($this->exhibitions as $exhibition) {?>
					<option value="<?php echo $exhibition->exhibition_id; ?>"><?php echo $exhibition->title; ?></option>
				<?php }?>

			</select>
			<br/>
			<label for='start'><?php echo _('Display from');?>:</label>
			<input type="text" id="start" name="start" required />
			<label for='end'><?php echo _('Display to');?>:</label>
			<input type="text" id="end" name="end" required />

			<?php } ?>
				<br/>

                <input type="submit" value='<?php echo _('Create this news item');?>' autocomplete="off" class='button'/>
		</form>
	</div>

	<?php if(isset($this->news)){ ?>
	<table class='news'>
		<tr>
			<th><?php echo _('Title');?></th>
			<th><?php echo _('Text');?></th>
			<th><?php echo _('Exhibition');?></th>
			<th><?php echo _('Date Created');?></th>
			<th><?php echo _('Display From -> To');?></th>
			<th><?php echo _('Delete');?></th>
		</tr>
		<?php foreach ($this->news as $news) { ?>
			<tr>
			<td><?php echo $news->title;?></td>
			<td><?php echo $news->text;?></td>
			<td><?php foreach ($this->exhibitions as $exhibition) {if($exhibition->exhibition_id== $news->exhibition_id){echo $exhibition->title;}}?></td>
			<td><?php echo $news->date_created;?></td>
			<td><?php echo $news->from_date; ?> -> <?php echo $news->to_date; ?></td>
			<td> <a href='<?= Config::get('URL') . 'news/delete/' . $news->news_id; ?>'><input type="button" value='<?php echo _('Delete');?>' autocomplete="off" class='button'/></a></td>
			</tr>
	<?php }?>
	</table>
	<?php }?>
</div>