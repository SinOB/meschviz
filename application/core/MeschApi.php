<?php

class MeschApi extends API
{
	public function __construct($request, $origin)
	{
		parent::__construct( $request );

		$APIKey = new APIKey();

		if (!array_key_exists( 'apiKey', $this->request ))
		{
			throw new Exception( 'No API Key provided' );
		} 		// Test if apikey supplied is valid
		else if (! $APIKey->verifyKey( $this->request ['apiKey'], $origin ))
		{
			throw new Exception( 'Invalid API Key' );
		}
	}


	/**
	 * Create a curator accout via the api
	 * example url: http://localhost/mags/api/createUser?apiKey=12345&user_name=frank&user_email=there@here.com&user_pwd=12345&user_museum_id=23876
	 * @return array containing HTTP status code and a message if success or error.
	 */
	protected function createUser()
	{

		try
		{

			// Check that we have all the required parameters
			if ( !isset( $this->request ['user_name'] ) || $this->request ['user_name'] == ''
					|| ! isset( $this->request ['user_email'] ) || $this->request ['user_email'] == ''
					|| !isset( $this->request ['user_pwd'] ) || $this->request ['user_pwd'] == ''
					|| ! isset( $this->request ['user_museum_id'] ) || $this->request ['user_museum_id'] == ''
					|| ! is_numeric( $this->request ['user_museum_id'] )
			)
			{
				return array (
						"message" => "Error: Missing or invalid parameters received."
				);
			}

			$user_name = $this->request ['user_name'];
			$user_email = $this->request ['user_email'];
			$user_password = $this->request ['user_pwd'];
			$user_password_hash = password_hash( $user_password, PASSWORD_DEFAULT );
			$user_museum_id = $this->request ['user_museum_id'];
			$user_account_type = 2; // Hardcoded to curator user

			// Check if username already exists
			if (UserModel::doesUsernameAlreadyExist( $user_name ))
			{
				return array (
						"message" => "Error: Username already taken."
				);
			}

			// Check if email already exists
			if (UserModel::doesEmailAlreadyExist( $user_email ))
			{
				return array (
						"message" => "Error: User email already taken."
				);
			}

			// Test that we have the museum in our database and get its local id
			$museum_id = MuseumModel::getMuseumId( $user_museum_id );
			if (! isset( $museum_id ) || $museum_id == '')
			{
				return array (
						"message" => "Error: Invalid museum id."
				);
			}

			// generate random hash for email verification (40 char string)
			$user_activation_hash = hash( 'sha256', uniqid( mt_rand(), true ), false );

			// set language to english by default
			$user_locale = 'en_GB';

			// Create the user account
			if (! RegistrationModel::writeNewUserToDatabase( $user_name, $user_password_hash, $user_email, time(), $user_activation_hash, $user_locale ))
			{
				return array (
						"message" => "Error: Unable to create user."
				);
			}

			// get user_id of the user that has been created, to keep things clean we DON'T use lastInsertId() here
			$user_id = UserModel::getUserIdByUsername( $user_name );

			if (! isset( $user_id ) || $user_id == '')
			{
				return array (
						"message" => "Error: Unable to create user."
				);
			}

			// Update the user type and museumid
			if (! AdminModel::updateUserAccount( $user_id, $user_account_type, $museum_id ))
			{
				// If any issue updating account: instantly delete the user
				RegistrationModel::rollbackRegistrationByUserId( $user_id );
				return array (
						"message" => "Error: Unable update user type and museum id."
				);
			}

			// Now need to activate the account
			if (! RegistrationModel::activateNewUser( $user_id, $user_activation_hash ))
			{
				// If any issue activating user: instantly delete the user
				RegistrationModel::rollbackRegistrationByUserId( $user_id );
				return array (
						"message" => "Error: Unable to activate the user account."
				);
			}

			// Return user id on success
			return array (
					"message" => "Success: new user created",
					"user_id" => $user_id
			);
		} catch ( Exception $e )
		{
			return array (
					"message" => "Error: Exception occured. " . $e
			);
		}
	}

	/**
	 * Update an existing curator account based on the supplied user id
	 * example url: http://localhost/mags/api/updateUser?apiKey=12345&user_id=17&user_name=new&user_email=here@here.com&user_pwd=1111&user_museum_id=23112
	 * @return array containing HTTP status code and a message if success or error.
	 */
	protected function updateUser()
	{

		try
		{
			// Check that we have all the required parameters
			if (! isset( $this->request ['user_id'] ) || $this->request ['user_id'] == ''
					|| ! isset( $this->request ['user_name'] ) || $this->request ['user_name'] == ''
					|| ! isset( $this->request ['user_email'] ) || $this->request ['user_email'] == ''
					|| ! isset( $this->request ['user_pwd'] ) || $this->request ['user_pwd'] == ''
					|| ! isset( $this->request ['user_museum_id'] ) || $this->request ['user_museum_id'] == ''
					|| ! is_numeric( $this->request ['user_museum_id'] ))
			{
				return array (
						"message" => "Error: Missing or invalid parameters received."
				);
			}

			$user_id = $this->request ['user_id'];
			$new_user_name = $this->request ['user_name'];
			$new_user_email = $this->request ['user_email'];
			$new_user_password = $this->request ['user_pwd'];
			$new_user_password_hash = password_hash( $new_user_password, PASSWORD_DEFAULT );
			$new_user_museum_id = $this->request ['user_museum_id'];
			$user_account_type = 2; // Hardcoded to curator user

			// Need original user from the database to perform the reset
			$user = UserModel::getPublicProfileOfUser( $user_id );

			// START Update Password - this must be done before change username
			// generate integer-timestamp (to see when exactly the user (or an attacker) requested the password reset via api)
			$temporary_timestamp = time();
			// 	generate random hash for email password reset verification (40 char string)
			$user_password_reset_hash = hash('sha256' , uniqid(mt_rand(), true), false);

			// Set token (= a random hash string and a timestamp) into database ...
			$token_set = PasswordResetModel::setPasswordResetDatabaseToken($user->user_name, $user_password_reset_hash, $temporary_timestamp);
			if (!$token_set) {
				return array (
						"message" => "Error: failed when tried to update user password."
				);
			}
			// Validate the password
			if(PasswordResetModel::validateNewPassword($user->user_name, $user_password_reset_hash, $new_user_password, $new_user_password))
			{
				if(!PasswordResetModel::saveNewUserPassword($user->user_name, $new_user_password_hash, $user_password_reset_hash))
				{
					return array (
							"message" => "Error: failed when tried to update user password."
					);
				}
			} else {
				return array (
						"message" => "Error: invalid password. ".Session::get('feedback_negative')[0]
				);
			}
			// END of updating password

			// Update user name
			if ($user->user_name != $new_user_name)
			{
				// Check if new username already exists
				if (UserModel::doesUsernameAlreadyExist( $new_user_name ))
				{
					return array (
							"message" => "Error: Username already taken."
					);
				}
				if(!UserModel::saveNewUserName($user->user_id, $new_user_name))
				{
					return array (
							"message" => "Error: Failed to update username."
					);
				}
			}

			// Update user email address
			if ($user->user_email != $new_user_email)
			{
				echo "Update the email address";
				// Check if new email already exists
				if (UserModel::doesEmailAlreadyExist( $new_user_email ))
				{
					return array (
							"message" => "Error: User email already taken."
					);
				}
				if(!UserModel::saveNewEmailAddress($user->user_id, $new_user_email))
				{
					return array (
							"message" => "Error: Failed to update email address."
					);
				}
			}

			// Update museum id

			// Test that we have the museum in our database and get its local id
			$museum_id = MuseumModel::getMuseumId( $new_user_museum_id );
			if (! isset( $museum_id ) || $museum_id == '')
			{
				return array (
						"message" => "Error: Invalid museum id."
				);
			}
			if ($user->user_museum != $museum_id)
			{
				echo "Update museum id";
				// Update the user type and museumid
				if (! AdminModel::updateUserAccount( $user->user_id, $user_account_type, $museum_id ))
				{
					return array (
							"message" => "Error: Unable update user type and museum id."
					);
				}
			}
			// Return user id on success
			return array (
					"message" => "Success: updated user",
					"user_id" => $user->user_id
			);
		} catch ( Exception $e )
		{
			return array (
					"message" => "Error: Exception occured. " . $e
			);
		}
	}

	/**
	 * Changes account from curator account to basic user level access.
	 * effectively revokes the curator permissions but leaves the account intact.
	 * @return array containing HTTP status code and a message if success or error.
	 */
	protected function revokeCuratorPermissions()
	{
		try
		{
			// Check that we have all the required parameters
			if (! isset( $this->request['user_id'] ) || $this->request['user_id'] == '')
			{
				return array (
						"message" => "Error: Missing or invalid parameters received."
				);
			}
			$user_id = $this->request['user_id'];
			$user_account_type = 3;
			$museum_id = 0;

			// Update the user type and museumid
			AdminModel::updateUserAccount( $user_id, $user_account_type, $museum_id );

			$user = UserModel::getPublicProfileOfUser( $user_id );
			if($user->user_account_type == $user_account_type)
			{
				// Return user id on success
				return array (
						"message" => "Success: Account no longer has curator level access. Has been changed to basic user account.",
						"user_id" => $user_id
				);
			}

			return array (
					"message" => "Error: Unable to remove curator level access from user account.",
					"user_id" => $user_id
			);


		} catch ( Exception $e )
		{
			return array (
					"message" => "Error: Exception occured. " . $e
			);
		}
	}
}