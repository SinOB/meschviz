<?php
class APIKey{

	public static function verifyKey($key, $origin)
	{
		// Just incase server admin has forgotten to set the api key in the config file
		if(Config::get('MESCH_API_KEY') == null || Config::get('MESCH_API_KEY')=='')
		{
			return false;
		}
		if( Config::get('MESCH_API_KEY') === $key){
			return true;
		}
		return false;
	}
}