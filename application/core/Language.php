<?php

/**
 * Class Language
 * Handles internationalisation (I18N) support
 * String translation only - don't want numerical localisations (yet)
 */
class Language
{

	public function __construct()
	{

	}

	// TODO what happens if browser has a language setting....

	/**
	 * Set the langugae on the session and page for the user
	 *
	 * @param string $locale
	 */
	public static function set_locale($locale = NULL)
	{
		$language = $locale;

		// if no locale variable is passed to the function
		if (! isset( $locale ))
		{
			// if user already has their session set to a session language - use this
			if (isset( $_SESSION ) && isset( $_SESSION ["language"] ))
			{
				$language = $_SESSION ["language"];
				// if no locale and no session language set - do nothing
			} else
			{
				return;
			}
		}

		// save the new locale to the database for logged in users
		if(Session::userIsLoggedIn()){
			UserModel::editUserLocale(Session::get ( 'user_id' ), $language);
		}

		$domain = Config::get( 'LOCALE_DOMAIN' );
		putenv( "LANG=" . $language );
		setlocale( LC_MESSAGES, $language );
		bindtextdomain( $domain, Config::get( 'LOCALE_DIR' ) );
		textdomain( $domain );
		bind_textdomain_codeset( $domain, 'UTF-8' );

		// Set the session language
		Session::set( 'language', $language );
	}

	/**
	 * Test if the supplied locale is considered a valid one
	 *
	 * @param unknown $user_locale
	 * @return boolean
	 */
	// TODO this should check if locale is in one of those we have translations for - otherwise fall back to english??
	public static function is_valid($user_locale){
		if(isset($user_locale) && strlen($user_locale)>2){
			return true;
		}
		return false;
	}
}
