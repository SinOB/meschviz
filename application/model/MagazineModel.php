<?php

/**
 * MagazineModel
 */
class MagazineModel {
	/**
	 * Get all magazines in the system
	 *
	 * @return array an array with several objects
	 */
	public static function getAllMagazines() {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, magazine_id, magazine_title, status FROM magazines ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ();

		return $query->fetchAll ();
	}

	/**
	 * Get all magazines in the system owned by the current user
	 *
	 * @return array an array with several objects
	 */
	public static function getUserMagazines() {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, magazine_id, magazine_title, status FROM magazines WHERE user_id = :user_id ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':user_id' => Session::get ( 'user_id' )
		) );

		return $query->fetchAll ();
	}

	public static function getMagazinesByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, magazine_id, magazine_title, status FROM magazines WHERE exhibition_id = :exhibition_id ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll ();

	}

	/**
	 * Get a single magazine
	 *
	 * @param int $magazine_id id of the specific magazine
	 * @return object a single object (the result)
	 */
	public static function getMagazine($magazine_id) {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, magazine_id, magazine_title, exhibition_id, status
				FROM magazines
				WHERE magazine_id = :magazine_id
				LIMIT 1";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':magazine_id' => $magazine_id
		) );


		return $query->fetch ();
	}

	public static function getMagazineFrontImage($magazine_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT external_source
			FROM articles
			LEFT JOIN components
			ON components.article_id=articles.article_id
			WHERE magazine_id = :magazine_id
			AND external_source!=''
			LIMIT 1";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':magazine_id' => $magazine_id
		) );

		return $query->fetch();
	}

	public static function getMagazineFrontImages($magazine_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT external_source
			FROM articles
			LEFT JOIN components
			ON components.article_id=articles.article_id
			WHERE magazine_id = :magazine_id
			AND external_source!=''
			LIMIT 6";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':magazine_id' => $magazine_id
		) );

		return $query->fetchAll();
	}

	/**
	 * Create a new magazine
	 *
	 * @param string $magazine_title magazine title that will be created
	 * @return bool feedback (was the magazine created properly ?)
	 */
	public static function createMagazine($magazine_title, $exhibition_id) {

		if (! $magazine_title || strlen ( $magazine_title ) == 0 || !$exhibition_id || $exhibition_id=='') {
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_MAGAZINE_CREATION_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "INSERT INTO magazines (magazine_title, exhibition_id, user_id, date_created)
				VALUES (:magazine_title, :exhibition_id, :user_id, now())";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':magazine_title' => $magazine_title,
				':exhibition_id' => $exhibition_id,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_MAGAZINE_CREATION_FAILED' ) );
		return false;
	}

	/**
	 * Delete a specific magazine
	 *
	 * @param int $magazine_id id of the magazine
	 * @return bool feedback (was the magazine deleted properly ?)
	 */
	public static function deleteMagazine($magazine_id) {
		if (! $magazine_id) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "DELETE FROM magazines WHERE magazine_id = :magazine_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':magazine_id' => $magazine_id,
				':user_id' => Session::get ( 'user_id' )
		) );

		// Delete all articles and components
		// First get all the articles
		$articles = ArticleModel::getArticlesByMagazine($magazine_id);
		for($i = 0; $i < count( $articles ); $i ++)
		{
			// Next get all the components related to the article
			$components = ComponentModel::getComponentsByArticle($articles[$i]->article_id);
			// Delete all components in the articles
			for($j=0; $j< count($components); $j++)
			{
				// Delete the component
				ComponentModel::deleteComponent($components[$j]->component_id);
			}
			// Delete the article
			ArticleModel::deleteArticle($articles[$i]->article_id);
		}

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_MAGAZINE_DELETION_FAILED' ) );
		return false;
	}

	/**
	 * Update an existing magazine
	 *
	 * @param int $magazine_id id of the specific magazine
	 * @param string $magazine_title new text of the specific magazine
	 * @return bool feedback (was the update successful ?)
	 */
	public static function updateMagazine($magazine_id, $magazine_title, $status) {
		if (! $magazine_id || ! $magazine_title) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "UPDATE magazines
				SET magazine_title = :magazine_title,
				status =:status
				WHERE magazine_id = :magazine_id
				AND user_id = :user_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':magazine_id' => $magazine_id,
				':status' => $status,
				':magazine_title' => $magazine_title,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_MAGAZINE_EDITING_FAILED' ) );
		return false;
	}
}
