<?php
require_once ("../sources/phpRecipeApi.php");
class RecipeModel {

	/**
	 * Test that the recipe API is up and running (aka authroing tool model)
	 */
	public static function test()
	{
		$f = new phpRecipeApi (  );
		return $f->test();
	}
}