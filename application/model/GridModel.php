<?php

/**
 * GridModel
 */
class GridModel {

	/**
	 * Get all grid visualisations in the system owned by the current user
	 *
	 * @return array an array with several objects
	 */
	public static function getUserGrids()
	{

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT user_id, grid_id, title, status FROM grids WHERE user_id = :user_id ORDER BY date_created DESC";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':user_id' => Session::get( 'user_id' )
		) );

		return $query->fetchAll();
	}

	public static function getGridImages($grid_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT image_id, grid_id, url, license_id FROM grid_images WHERE grid_id = :grid_id ORDER BY date_created ASC";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':grid_id' => $grid_id
		) );

		return $query->fetchAll();
	}

	public static function getGrid($grid_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT user_id, grid_id, title, exhibition_id, status FROM grids WHERE grid_id = :grid_id ORDER BY date_created DESC";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':grid_id' => $grid_id
		) );

		return $query->fetch();
	}


	public static function getGridsByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT user_id, grid_id, title, status FROM grids WHERE exhibition_id = :exhibition_id ORDER BY date_created DESC";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll();
	}

	/**
	 * Create a grid
	 *
	 * @param string $title title of grid that will be created
	 * @param int $exhibition_id unique id of exhibition
	 * @return boolean feedback
	 */
	public static function create($title, $exhibition_id)
	{

		if (! $title || strlen( $title ) == 0 || ! $exhibition_id )
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_GRID_CREATION_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "INSERT INTO grids (title, exhibition_id, user_id, date_created)
				VALUES (:title, :exhibition_id, :user_id, now())";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':title' => $title,
				':exhibition_id' => $exhibition_id,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_GRID_CREATION_FAILED' ) );
		return false;
	}


	public static function updateGrid($grid_id, $title, $status) {
		if (! $grid_id || ! $title) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "UPDATE grids
				SET title = :title,
				status =:status
				WHERE grid_id = :grid_id
				AND user_id = :user_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':grid_id' => $grid_id,
				':status' => $status,
				':title' => $title,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_GRID_EDITING_FAILED' ) );
		return false;
	}

	public static function addImage($grid_id, $url, $license_id) {
		if (! $grid_id || !$url ||strlen( $url ) == 0 || ! $license_id )
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_GRID_IMAGE_CREATION_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "INSERT INTO grid_images (grid_id, url, license_id, user_id, date_created)
				VALUES (:grid_id, :url ,:license_id, :user_id, now())";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':grid_id' => $grid_id,
				':url' => $url,
				':license_id' => $license_id,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_GRID_IMAGE_CREATION_FAILED' ) );
		return false;
	}

	/**
	 * Delete a specific grid - also delete the associated image urls
	 *
	 * @param int $grid_id id of the grid
	 * @return bool feedback (was the grid deleted properly ?)
	 */
	public static function delete($grid_id)
	{

		if (! $grid_id)
		{
			return false;
		}

		// now delete from the database
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "DELETE FROM grids WHERE grid_id = :grid_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':grid_id' => $grid_id,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			$sql = "DELETE FROM grid_images WHERE grid_id = :grid_id AND user_id = :user_id LIMIT 1";
			$query = $database->prepare( $sql );
			$query->execute( array (
					':grid_id' => $grid_id,
					':user_id' => Session::get( 'user_id' )
			) );
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_GRID_DELETION_FAILED' ) );
		return false;
	}


	public static function deleteImage($image_id, $grid_id)
	{
		if (! $grid_id || !$image_id)
		{
			return false;
		}

		// now delete from the database
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "DELETE FROM grid_images WHERE grid_id = :grid_id AND image_id = :image_id LIMIT 1";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':grid_id' => $grid_id,
				':image_id' => $image_id
		) );
		if ($query->rowCount() == 1){
			return true;
		}
		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_GRID_IMAGE_DELETION_FAILED' ) );
		return false;
	}
}