<?php

/**
 * POIModel
 */
class POIModel {

	/**
	 * Create/Update exhibition points of interest based on data subblied by recipe API
	 * @param unknown $point_of_interest
	 * @param unknown $exhibition_id
	 * @return boolean
	 */
	public static function createUpdatePOI($point_of_interest, $exhibition_id) {

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT point_of_interest_id, point_of_interest, exhibition_id
		FROM points_of_interest
		WHERE point_of_interest=:point_of_interest
		AND exhibition_id = :exhibition_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':point_of_interest' => $point_of_interest,
				':exhibition_id' => $exhibition_id
		) );

		$poi = $query->fetch();
		// if record exists - do an update
		if(isset($poi) && $poi!='')
		{
			echo "\t\tUPDATE data for point_of_interest $point_of_interest.\n";
			$sql = "UPDATE points_of_interest SET
					point_of_interest =:point_of_interest,
					exhibition_id=:exhibition_id
					WHERE point_of_interest_id=:point_of_interest_id";

			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':point_of_interest' => $point_of_interest,
					':exhibition_id' => $exhibition_id,
					':point_of_interest_id' => $poi->point_of_interest_id
			) );
			if (!mysql_error()){
				return $poi->point_of_interest_id;
			}
			echo "\t\tERROR!!! - unable to update exhibition $point_of_interest into the database.\n";
			return false;
		}
		else
		{
			echo "\t\tINSERT data for point_of_interest $point_of_interest.\n";

			$sql = "INSERT INTO points_of_interest
					(point_of_interest, exhibition_id, date_created)
					VALUES (:point_of_interest, :exhibition_id, now())";
			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':point_of_interest' => $point_of_interest,
					':exhibition_id' => $exhibition_id,
			) );
			if (!mysql_error()){
				return $database->lastInsertId();
			}
			echo "\t\tERROR!!! - unable to insert point of interest $point_of_interest into the database.\n";
			return false;
		}
	}

	/**
	 * Get all available points of interest by exhibition id
	 * @param int $exhibition_id
	 */
	public static function getPOIsByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT point_of_interest_id, exhibition_id, point_of_interest, date_created
				FROM points_of_interest WHERE exhibition_id = :exhibition_id
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll ();

	}

	/**
	 * Based on the supplied passcode - get the available points of interes
	 * @param unknown $passcode
	 */
	public static function getPOIsByPasscode($passcode)
	{
		// get the exhibition id from the passcode
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$exhibition_id = $exhibition->exhibition_id;
		//get the points of interest
		return POIModel::getPOIsByExhibition($exhibition_id);
	}

	/**
	 * Delete all old points of interest affiliated with an exhibition. Used by the authoring tool recipe update script.
	 * @string $exhibition_id
	 * @array $keep_points
	 * @return boolean
	 */
	public static function deletePOIsForExhibition($exhibition_id, $keep_points) {
		if (! $exhibition_id ) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();


		// Need to cleanly handle array of WHERE IN
		if(!isset($keep_points) || count($keep_points)<=0){
			// if dont want to keep any contents at all
			$parameters = [0];
			$questionmarks = "?";
		} else {
			$parameters = $keep_points;
			$questionmarks = str_repeat("?,", count($keep_points)-1) . "?";
		}


		// PDO does not appear to have a very clean way to handle 'where in' array - have to build query manually
		// and bind the variables as below
		$sql = "DELETE FROM points_of_interest
				WHERE exhibition_id = ?
				AND point_of_interest not in ($questionmarks)";

		// append exhibition id to parameters array
		array_unshift($parameters, $exhibition_id);

		$query = $database->prepare ( $sql );
		$query->execute ($parameters);

		if ($query->rowCount () >= 1) {
			return true;
		}

		echo "\t\tNo points of interest deleted from the database.\n";
		return false;
	}
}
