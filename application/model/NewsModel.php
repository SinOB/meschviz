<?php

/**
 * NewsModel
 */
class NewsModel {

	/**
	 * Get all news items as created by the current user
	 */
	public static function getNews() {

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT news_id, exhibition_id, title, text, user_id, date_created, from_date, to_date
				FROM news
				WHERE user_id = :user_id";

		$query = $database->prepare ( $sql );
		$query->execute (array(
				':user_id' => Session::get ( 'user_id' ),
		));

		return $query->fetchAll ();
	}

	/**
	 * Get all news items created for the specified exhibition id
	 * @param int $exhibition_id
	 */
	public static function getNewsByExhibition($exhibition_id) {

		$todaysDate = date("Y-m-d");
		$database = DatabaseFactory::getFactory ()->getConnection ();
		$sql = "SELECT news_id, exhibition_id, title, text, user_id, date_created, from_date, to_date
                FROM news WHERE exhibition_id = :exhibition_id
				AND from_date <= :date AND to_date>= :date
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id,
				':date' =>$todaysDate
		) );

		return $query->fetchAll ();
	}

	/**
	 * Create a news item
	 * @param int $exhibition_id
	 * @param string $title
	 * @param string $text
	 * @return boolean
	 */
	public static function create($exhibition_id, $title, $text, $start, $end) {

		if (! $title || strlen ( $title ) == 0 || !$exhibition_id || $exhibition_id=='' || ! $text || strlen ( $text ) == 0 ) {
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_NEWS_CREATION_FAILED' ) );
			return false;
		}

		if(!$start || !$end){
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_NEWS_CREATION_FAILED' ) );
			return false;
		}

		$from_date=date('Y-m-d 00:00:00', strtotime($start));
		$to_date= date('Y-m-d 23:59:59', strtotime($end));

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "INSERT INTO news (exhibition_id, title, text, user_id, date_created, from_date, to_date)
				VALUES (:exhibition_id, :title, :text,  :user_id, now(), :from_date, :to_date)";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id,
				':title' => $title,
				':text' => $text,
				':from_date' => $from_date,
				':to_date' => $to_date,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_NEWS_CREATION_FAILED' ) );
		return false;
	}

	/**
	 * Delete a specific news item
	 *
	 * @param int $news_id id of the news item
	 * @return bool feedback (was the news item deleted properly ?)
	 */
	public static function delete($news_id) {
		if (! $news_id) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "DELETE FROM news WHERE news_id = :news_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':news_id' => $news_id,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_NEWS_DELETION_FAILED' ) );
		return false;
	}

}