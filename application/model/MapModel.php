<?php

/**
 * MapModel
 */
class MapModel {

	/**
	 * Get all maps for the user
	 *
	 * @return array an array with several objects
	 */
	public static function getMaps() {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT map_id, exhibition_id, title, background_image, point_data, user_id, date_created, status FROM maps
				WHERE user_id = :user_id
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':user_id' => Session::get ( 'user_id' )
		) );

		return $query->fetchAll ();
	}

	/**
	 * Get maps by exhibition id
	 *
	 * @param unknown $exhibition_id
	 */
	public static function getMapsByExhibition($exhibition_id) {

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT map_id, exhibition_id, title, background_image, point_data, user_id, date_created, status FROM maps
				WHERE exhibition_id = :exhibition_id
				ORDER BY date_created DESC";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll();
	}

	/**
	 * Get map by map id
	 *
	 * @param unknown $map_id
	 */
	public static function getMap($map_id) {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT map_id, exhibition_id, title, background_image, point_data, user_id, date_created, status FROM maps
				WHERE map_id = :map_id
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':map_id' => $map_id
		) );

		return $query->fetch();
	}

	/**
	 * Create a map object
	 *
	 * @param unknown $exhibition_id
	 * @param unknown $title
	 * @param unknown $background_image
	 * @return boolean
	 */
	public static function create($exhibition_id, $title, $background_image)
	{
		if (! $exhibition_id || ! $title || strlen( $title ) == 0  || ! $background_image)
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_MAP_CREATION_FAILED' ) );
			return false;
		}


		$folder = Config::get( 'PATH_IMAGES' ) . 'maps/';
		$extension = pathinfo( $background_image['file_upload']['name'], PATHINFO_EXTENSION );

		$new_filename = uniqid(). '.' . $extension;

		if(!FileModel::validateImageFile( $background_image['file_upload'] )){
			return false;
		}

		// check folder writing rights, check if upload fits all rules
		if (FileModel::isFolderWritable( $folder ))
		{
			// remove any existing file that could correspond with new name
			FileModel::delete($folder, $new_filename);

			// save the file
			if (! move_uploaded_file( $background_image['file_upload']['tmp_name'], $folder . $new_filename ))
			{
				Session::add( 'feedback_negative', Text::get( 'FEEDBACK_MAP_CREATION_FAILED' ) );
				return false;
			}

			$database = DatabaseFactory::getFactory()->getConnection();

			$sql = "INSERT INTO maps (exhibition_id, title, background_image, user_id, date_created)
					VALUES (:exhibition_id, :title, :background_image, :user_id, now())";
			$query = $database->prepare( $sql );
			$query->execute( array (
					':exhibition_id' => $exhibition_id,
					':title' => $title,
					':background_image' => $new_filename,
					':user_id' => Session::get( 'user_id' )
			) );

			if ($query->rowCount() == 1)
			{
				return true;
			}

			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_MAP_CREATION_FAILED' ) );
			return false;
		}
		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_MAP_CREATION_FAILED' ) );
		return false;
	}

	/**
	 * Update map title, status and point data
	 *
	 * @param unknown $map_id
	 * @param unknown $title
	 * @param unknown $status
	 * @param unknown $point_data
	 * @return boolean
	 */
	public static function update($map_id, $title, $status, $point_data){
		if (! $map_id || ! $title) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "UPDATE maps
				SET title = :title,
				status =:status,
				point_data =:point_data
				WHERE map_id = :map_id
				AND user_id = :user_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':map_id' => $map_id,
				':status' => $status,
				':title' => $title,
				':point_data' => $point_data,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_MAP_EDITING_FAILED' ) );
		return false;
	}

	/**
	 * Delete a specific map object from the database and the background image from the file system
	 *
	 * @param int $map_id id of the map
	 * @return bool feedback
	 */
	public static function delete($map_id)
	{
		if (! $map_id)
		{
			return false;
		}

		// Delete any existing file from the system if we had an upload in there already
		$map = MapModel::getMap($map_id);
		if(isset($map->background_image)){
			$folder = Config::get( 'PATH_IMAGES' ) . 'maps/';
			FileModel::delete($folder, $map->background_image);
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "DELETE FROM maps WHERE map_id = :map_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':map_id' => $map_id,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_MAP_DELETION_FAILED' ) );
		return false;
	}
}
