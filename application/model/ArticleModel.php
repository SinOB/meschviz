<?php

/**
 * ArticleModel
 */
class ArticleModel {

	public static function getArticle($article_id) {
		$database = DatabaseFactory::getFactory ()->getConnection ();
		$sql = "SELECT article_id, magazine_id, article_title
                FROM articles WHERE article_id = :article_id
				 AND user_id = :user_id ";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':article_id' => $article_id,
				':user_id' => Session::get ( 'user_id' )
		) );

		return $query->fetch ();
	}

	public static function getArticlesByMagazine($magazine_id) {
		$database = DatabaseFactory::getFactory ()->getConnection ();
		$sql = "SELECT article_id, magazine_id, article_title
                FROM articles WHERE magazine_id = :magazine_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':magazine_id' => $magazine_id
		) );

		return $query->fetchAll ();
	}

	public static function createArticle($magazine_id, $article_title) {

		if (! $article_title || strlen ( $article_title ) == 0) {
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_ARTICLE_CREATION_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$insert_sql = "INSERT INTO articles (magazine_id, article_title, user_id, date_created)
        		VALUES (:magazine_id, :article_title, :user_id, now())";
		$query = $database->prepare ( $insert_sql );
		$query->execute ( array (
				':magazine_id' => $magazine_id,
				':article_title' => $article_title,
				':user_id' => Session::get ( 'user_id' )
		) );

		if (! $query->rowCount () == 1) {
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_ARTICLE_CREATION_FAILED' ) );
			return false;
		}
	}

	public static function updateArticle($article_id, $article_title) {
		if (! $article_id || ! $article_title) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "UPDATE articles SET article_title = :article_title WHERE article_id = :article_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':article_id' => $article_id,
				':article_title' => $article_title,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_ARTICLE_EDITING_FAILED' ) );
		return false;
	}

	/**
	 * Delete a specific article
	 *
	 * @param int $article_id id of the article
	 * @return bool feedback (was the article deleted properly ?)
	 */
	public static function deleteArticle($article_id) {
		if (! $article_id) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "DELETE FROM articles WHERE article_id = :article_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':article_id' => $article_id,
				':user_id' => Session::get ( 'user_id' )
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		// default return
		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_ARTICLE_DELETION_FAILED' ) );
		return false;
	}
}
