<?php

/**
 * PointModel - Handling points on a timeline
 */
class PointModel
{

	public static function getPoint($point_id){

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT point_id, timeline_id, date, title, description, position, user_id, date_created,
				source_type, source_id, external_source
				FROM timeline_points
				WHERE point_id = :point_id";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':point_id' => $point_id
		) );

		return $query->fetch();

	}
	/**
	 * Get all points for a specific timeline
	 *
	 * @return array an array with several objects
	 */
	public static function getTimelinePoints($timeline_id)
	{

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT point_id, timeline_id, date, title, description, position, user_id, date_created,
				source_type, source_id, external_source
				FROM timeline_points
				WHERE timeline_id = :timeline_id
				ORDER BY position ASC";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':timeline_id' => $timeline_id
		) );

		return $query->fetchAll();
	}

	/**
	 * Create a timeline point
	 *
	 * @param unknown $timeline_id
	 * @param unknown $date
	 * @param unknown $title
	 * @param unknown $description
	 * @param unknown $order
	 * @return boolean
	 */
	public static function create($timeline_id, $date, $title, $position)
	{
		if (! $timeline_id || ! $title || strlen( $title ) == 0  || ! $position)
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_POINT_CREATION_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "INSERT INTO timeline_points (timeline_id, date, title, position, user_id, date_created)
				VALUES (:timeline_id, :date, :title, :position, :user_id, now())";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':timeline_id' => $timeline_id,
				':date' => $date,
				':title' => $title,
				':position' => $position,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_POINT_CREATION_FAILED' ) );
		return false;
	}

	  /**
	  * Update a timeline point
	  *
	  * @param unknown $point_id
	  * @param unknown $timeline_id
	  * @param unknown $date
	  * @param unknown $title
	  * @param unknown $description
	  * @param unknown $position
	  * @return boolean
	  */
	public static function update($point_id, $timeline_id, $date, $title, $description, $position)
	{
		if (!$point_id || !$timeline_id || ! $title || strlen( $title ) == 0  || ! $position)
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_POINT_UPDATE_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "UPDATE timeline_points
				SET date =:date,
				title=:title,
				description =:description,
				position =:position
				WHERE user_id =:user_id
				AND point_id= :point_id
				AND timeline_id=:timeline_id";

		$query = $database->prepare( $sql );
		$query->execute( array (
				':point_id' => $point_id,
				':timeline_id' => $timeline_id,
				':date' => $date,
				':title' => $title,
				':description' => $description,
				':position' => $position,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_POINT_UPDATE_FAILED' ) );
		return false;
	}


	/**
	 * Upload an image to the server and save it to the
	 * @param unknown $point_id
	 * @param unknown $timeline_id
	 * @param unknown $file
	 * @param unknown $license_id
	 * @return boolean
	 */
	public static function upload($point_id, $timeline_id, $file, $license_id)
	{

		// upload to local directory
		$folder = Config::get( 'PATH_IMAGES' ) . 'uploads/';
		$extension = pathinfo( $file['file_upload']['name'], PATHINFO_EXTENSION );

		$new_filename = uniqid(). '.' . $extension;

		// check folder writing rights, check if upload fits all rules
		if (FileModel::isFolderWritable( $folder ) and FileModel::validateImageFile( $file['file_upload'] ))
		{
			// remove any existing file that could correspond with new name
			FileModel::delete($folder, $new_filename);

			// save the file
			if (! move_uploaded_file( $file['file_upload']['tmp_name'], $folder . $new_filename ))
			{
				Session::add( 'feedback_negative', Text::get( 'FEEDBACK_FILE_IMAGE_UPLOAD_FAILED' ) );
				return false;
			}

			$external_source=Config::get( 'IMAGES_URL' ) . 'uploads/'.$new_filename;

			// update image on point
			PointModel::updateImage($point_id, $timeline_id, 'upload', $new_filename,  $external_source, $license_id );


			return true;
		}

		return false;
	}



	public static function updateImage($point_id, $timeline_id, $source_type, $source_id, $external_source, $license_id )
	{
		if (!$point_id || !$timeline_id)
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_POINT_UPDATE_FAILED' ) );
			return false;
		}

		// Delete any existing file from the system if we had an upload in there already
		$point = PointModel::GetPoint($point_id);
		if(isset($point->source_type) && $point->source_type=='upload'){
			$folder = Config::get( 'PATH_IMAGES' ) . 'uploads/';
			FileModel::delete($folder, $point->source_id);
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "UPDATE timeline_points
				SET source_type =:source_type,
				source_id=:source_id,
				external_source =:external_source,
				license_id =:license_id
				WHERE user_id =:user_id
				AND point_id= :point_id
				AND timeline_id=:timeline_id";

		$query = $database->prepare( $sql );
		$query->execute( array (
				':point_id' => $point_id,
				':timeline_id' => $timeline_id,
				':source_type' => (isset($source_type)&&$source_type!='')?$source_type:null,
				':source_id' => (isset($source_id)&&$source_id!='')?$source_id:null,
				':external_source' => (isset($external_source)&&$external_source!='')?$external_source:null,
				':license_id' => (isset($license_id)&&$license_id!='')?$license_id:null,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_POINT_UPDATE_FAILED' ) );
		return false;

	}

	/**
	 * Delete a specific timeline point
	 *
	 * @param int $point_id id of the point
	 * @return bool feedback
	 */
	public static function delete($point_id)
	{

		if (! $point_id)
		{
			return false;
		}

		// Delete any existing file from the system if we had an upload in there already
		$point = PointModel::GetPoint($point_id);
		if(isset($point->source_type) && $point->source_type=='upload'){
			$folder = Config::get( 'PATH_IMAGES' ) . 'uploads/';
			FileModel::delete($folder, $point->source_id);
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "DELETE FROM timeline_points WHERE point_id = :point_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':point_id' => $point_id,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_POINT_DELETION_FAILED' ) );
		return false;
	}

}