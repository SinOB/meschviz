<?php

class LanguageModel
{

	public static function change($language)
	{
		Language::set_locale( $language );
	}

	/**
	 * Create/Update exhibition languages based on data supplied by recipe api
	 *
	 * @param string $language_string
	 * @param int $exhibition_id
	 * @return boolean
	 */
	public static function createUpdateLanguage($language_string, $exhibition_id)
	{

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT id, language, exhibition_id
		FROM exhibition_languages
		WHERE language=:language
		AND exhibition_id=:exhibition_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':language' => $language_string,
				':exhibition_id' => $exhibition_id
		) );

		$language = $query->fetch();
		// if record exists - do an update
		if(isset($language) && $language!='')
		{
			echo "\t\tUPDATE data for language $language_string.\n";
			$sql = "UPDATE exhibition_languages SET
					language =:language,
					exhibition_id=:exhibition_id
					WHERE id=:id";

			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':language' => $language_string,
					':exhibition_id' => $exhibition_id,
					':id' => $language->id
			) );
			if (!mysql_error()){
				return $language->id;
			}
			echo "\t\tERROR!!! - unable to update language $language_string into the database.\n";
			return false;
		}
		else
		{
			echo "\t\tINSERT data for language $language_string.\n";

			$sql = "INSERT INTO exhibition_languages
					(language, exhibition_id, date_created)
					VALUES (:language, :exhibition_id, now())";
			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':language' => $language_string,
					':exhibition_id' => $exhibition_id,
			) );
			if (!mysql_error()){
				return $database->lastInsertId();
			}
			echo "\t\tERROR!!! - unable to insert language $language_string into the database.\n";
			return false;
		}
	}

	/**
	 * Delete all old languages affiliated with an exhibition. Used by the authoring tool recipe update script.
	 * @string $exhibition_id
	 * @array $keep_languages
	 * @return boolean
	 */
	public static function deleteLanguagesForExhibition($exhibition_id, $keep_languages) {
		if (! $exhibition_id ) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();


		// Need to cleanly handle array of WHERE IN
		if(!isset($keep_languages) || count($keep_languages)<=0){
			// if dont want to keep any contents at all
			$parameters = [0];
			$questionmarks = "?";
		} else {
			$parameters = $keep_languages;
			$questionmarks = str_repeat("?,", count($keep_languages)-1) . "?";
		}


		// PDO does not appear to have a very clean way to handle 'where in' array - have to build query manually
		// and bind the variables as below
		$sql = "DELETE FROM exhibition_languages
		WHERE exhibition_id = ?
		AND language not in ($questionmarks)";

		// append exhibition id to parameters array
		array_unshift($parameters, $exhibition_id);

		$query = $database->prepare ( $sql );
		$query->execute ($parameters);

		if ($query->rowCount () >= 1) {
			return true;
		}

		echo "\t\tNo languages deleted from the database.\n";
		return false;
	}
}
