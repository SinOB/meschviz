<?php
require_once ("../sources/phpFlickr.php");
class FlickrModel {

	/**
	 * Get most recent photos where tag or text matches supplied string
	 *
	 * @return array an array with several objects (the results)
	 */
	// TODO / NB flickr search by all rights reserved (license id ==0) does not work at this time
	public static function getPhotosByString($string, $count, $reusability) {

		$params = array (
					"text" => $string,
					"tag_mode" => "any",
					"per_page" => $count,
					"media" => "photos",
					"safe_search" => 1,
					// make work like site search by sorting by relevance rather than default to most recent
					"sort" => "relevance",
					// Extras to return in results are Get in 'small' size and flickr license id
					"extras" => "url_s, license,"
		);

		// attach copyright limitations if have any - does not work for all rights reserved
		if(isset($reusability))
		{
			$flickr_licenses =[];
			$licenses = LicenseModel::getLicenses();
			for ($i = 0; $i < count($licenses); $i++) {
				if($licenses[$i]->reusability === $reusability)
				{
					$flickr_licenses[strval($licenses[$i]->flickr_id)] = 1;
					continue;
				}
			}
			// TODO NB Flickr search by license 0 only does not work at this time - returns all
			$params["license"] = implode (",", array_keys($flickr_licenses));
		}

		$f = new phpFlickr ( Config::get ( 'FLICKR_API_KEY' ) );
		$cache_dir = Config::get('FLICKR_CACHE_DIR');
		$f->enableCache ( "fs", $cache_dir );
		$recent = $f->photos_search ($params);

		return $recent;
	}

	public static function test()
	{
		$f = new phpFlickr ( Config::get ( 'FLICKR_API_KEY' ) );
		return $f->test_echo ();

	}


	public static function getSearchResults($string, $count, $language, $reusability) {
		$f = new FlickrModel ();

		$data = $f->getPhotosByString ( $string, $count, $reusability);
		$results = array();
		$licenses = LicenseModel::getLicensesAsFlickrArray();

		foreach ( $data ['photo'] as $photo ) {
			// get the license
			$license_id = $licenses[$photo ['license']]->license_id;
			$html = '<img id="' . $photo ['id'] . '" src="' . $photo ['url_s'] . '" alt="' . $photo ['title'] . '" >';
			$component['id']=$photo ['id'];
			$component['source']='flickr';
			$component['license_id'] = $license_id;
			$component['external_source']=$photo ['url_s'] ;
			$component['html']=$html;
			$results [] = $component;
		}
		return $results;
	}

}
