<?php

/**
 * PasscodeModel
*/
class PasscodeModel {

	/**
	 * Get all passcodes in the system owned by the current user
	 *
	 * @return array an array with several objects
	 */
	public static function getUserPasscodes() {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, passcode_id, passcode, exhibition_id
				FROM passcodes
				WHERE user_id = :user_id ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':user_id' => Session::get ( 'user_id' )
		) );

		return $query->fetchAll ();
	}

	/**
	 * Get all passcodes in the system
	 */
	public static function getPasscodes() {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, passcode_id, passcode, exhibition_id, date_created
				FROM passcodes
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array() );

		return $query->fetchAll ();
	}

	public static function getUserPasscodesByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, passcode_id, passcode, exhibition_id, date_created
				FROM passcodes WHERE user_id = :user_id
				AND exhibition_id= :exhibition_id
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':user_id' => Session::get ( 'user_id' ),
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll ();

	}

	public static function getRandomPasscode($exhibition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT passcode
				FROM passcodes WHERE
				exhibition_id= :exhibition_id
				ORDER BY RAND() LIMIT 1";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetch();

	}

	/**
	 * Add a passcode to the database for a user
	 *
	 * @param string $passcode passcode expected in the format ABC-123-A1B
	 * @return bool feedback (was the passcode added properly ?)
	 */
	public static function createPasscode($passcode, $user_id=null) {


		if (! $passcode || strlen ( $passcode ) == 0) {
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_CREATION_FAILED' ) );
			return false;
		}

		// Make sure the supplied passcode is in the correct format
		if(!preg_match('/^[a-zA-Z]{3}-[0-9]{3}-[0-9]{4}$/i', $passcode)) {
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_CREATION_INVALID_FAILED' ) );
			return false;
		}

		// check if the passcode already exists for the user
		$user_passcodes = PasscodeModel::getUserPasscodes();
		if(isset($user_passcodes) && count($user_passcodes)>0){
			for($i=0;$i<count($user_passcodes);$i++){
				if($user_passcodes[$i]->passcode==$passcode)
				{
					echo "user already added this one";
					return false;
				}
			}
		}


		//Test if the passcode exists via the log API
		$logdata = VisitLogModel::getVisitPointsByPasscode($passcode);

		if(!isset($logdata) || count($logdata)==0)
		{
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_CREATION_INVALID_FAILED' ) );
			return false;
		}
		$date_created =$logdata[0]->start_time;

		// Get the exhibition id
		list($first, $second, $exhibition_code) = preg_split('/-/', $passcode);
		$exhibition_code =ltrim($exhibition_code, '0');
		$exhibition = ExhibitionModel::getExhibitionByExhibitionCode($exhibition_code);
		$exhibition_id = $exhibition->exhibition_id;
		// we have an invalid exhibition id
		if(!isset($exhibition_id) || $exhibition_id=='')
		{
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_CREATION_INVALID_FAILED' ) );
			return false;
		}


		if(!isset($user_id)){
			$user_id = Session::get ( 'user_id' );
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "INSERT INTO passcodes (passcode, user_id, exhibition_id, date_created)
				VALUES (:passcode, :user_id, :exhibition_id, :date_created)";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':passcode' => $passcode,
				':user_id' => $user_id,
				':exhibition_id' => $exhibition_id,
				':date_created'=> $date_created
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_CREATION_FAILED' ) );
		return false;
	}


	public static function isValidPasscode($passcode){
		if (preg_match('/^[A-Z]{3}-[0-9]{3}-[0-9]{4}$/i', $passcode)) {
			return true;
		}
		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_INVALID' ) );
		return false;
	}


	/**
	 * Delete a specific passcode
	 * @param unknown $passcode_id
	 * @return boolean
	 */
	public static function delete($passcode_id){
		if (! $passcode_id) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "DELETE FROM passcodes WHERE passcode_id = :passcode_id LIMIT 1";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':passcode_id' => $passcode_id,
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_DELETION_FAILED' ) );

	}
}