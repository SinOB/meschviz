<?php

/**
 * ExhibitionModel
 */
class ExhibitionModel {
	/**
	 * Get all exhibitions in the system
	 *
	 * @return array an array with several objects
	 */
	public static function getExhibitionsByAdmin() {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT exhibition_id, museum_id, code, title, description, image, exhibition_url, display_visualisations_bitmask
				FROM exhibitions
				LEFT JOIN users
				ON users.user_museum=exhibitions.museum_id
				WHERE user_id = :user_id
				ORDER BY date_created DESC";

		$query = $database->prepare ( $sql );
		$query->execute (array(
				':user_id' => Session::get ( 'user_id' )
		));

		return $query->fetchAll ();
	}

	public static function getExhibitionsByUser()
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT DISTINCT exhibitions.exhibition_id, exhibitions.title, exhibitions.description, exhibitions.image, count(passcode) AS passcode_count
		FROM passcodes
		LEFT JOIN exhibitions
		ON exhibitions.exhibition_id = passcodes.exhibition_id
		WHERE passcodes.user_id=:user_id
		GROUP BY exhibition_id
		ORDER BY passcodes.date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':user_id' => Session::get ( 'user_id' )
		) );

		return $query->fetchAll ();
	}

	/**
	 * Get all exhibitions - should only be used by recipe updator
	 */
	public static function getExhibitions()
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();
		$sql = "SELECT exhibition_id, code, recipe_id
		FROM exhibitions";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':user_id' => Session::get ( 'user_id' )
		) );

		return $query->fetchAll ();
	}

	public static function getExhibition($exhabition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT exhibition_id, title, description, image, code, exhibition_url, display_visualisations_bitmask
		FROM exhibitions
		WHERE exhibition_id=:exhibition_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhabition_id
		) );

		return $query->fetch();
	}

	/**
	 * Get the local exhibition_id based on the exhebition code supplied in the passcode
	 * @param unknown $exhibition_code
	 */
	public static function getExhibitionByExhibitionCode($exhibition_code)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();
		$sql = "SELECT exhibition_id, title, description, image, code, exhibition_url, display_visualisations_bitmask
		FROM exhibitions
		WHERE code=:code";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':code' => $exhibition_code
		) );

		return $query->fetch();
	}

	public static function getExhibitionByPasscode($passcode)
	{
		if(!isset($passcode)){
			return;
		}
		// first extract the exhibition id from the passcode
		list($first, $second, $exhibition_code) = preg_split('/-/', $passcode);
		$exhibition_code =ltrim($exhibition_code, '0');

		// get the exhibition
		return ExhibitionModel::getExhibitionByExhibitionCode($exhibition_code);
	}

	/**
	 * Get the exhibition based on the authoring tool recipe id. This is used by the recipe update script.
	 * @param unknown $recipe_id
	 */
	public static function getExhibitionFromReipeId($recipe_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT exhibition_id, title, description, image, recipe_last_updated, exhibition_url, display_visualisations_bitmask
		FROM exhibitions
		WHERE recipe_id=:recipe_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':recipe_id' => $recipe_id
		) );

		return $query->fetch();
	}


	/**
	 * Create/Update exhibition information based on data received from recipe api
	 *
	 * @param unknown $recipe_id
	 * @param unknown $title
	 * @param unknown $desription
	 * @param unknown $museum_id
	 * @param unknown $exhibition_short_code
	 * @param unknown $exhibition_image
	 * @return boolean
	 */
	public static function createUpdateExhibition($recipe_id, $title, $desription, $museum_id, $exhibition_short_code, $exhibition_image, $exhibition_url)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT exhibition_id, title, description, image, recipe_id
		FROM exhibitions
		WHERE recipe_id=:recipe_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':recipe_id' => $recipe_id
		) );

		$exhibition = $query->fetch();
		// if record exists - do an update
		if(isset($exhibition) && $exhibition!='')
		{
			echo "\t\tUPDATE data for exhibition $recipe_id.\n";
			$sql = "UPDATE exhibitions SET
					museum_id =:museum_id,
					code=:code,
					title=:title,
					description=:description,
					image=:image,
					exhibition_url=:exhibition_url
					WHERE recipe_id=:recipe_id";

			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':museum_id' => $museum_id,
					':code' => $exhibition_short_code,
					':title' => $title,
					':description' => $desription,
					':image' => $exhibition_image,
					':exhibition_url'=>$exhibition_url,
					':recipe_id'=> $recipe_id
			) );
			if (!mysql_error()){
				return $exhibition->exhibition_id;
			}
			echo "\t\tERROR!!! - unable to update exhibition $recipe_id into the database.\n";
			return false;
		} else {

			echo "\t\tINSERT data for exhibition $recipe_id.\n";
			$sql = "INSERT INTO exhibitions
					(museum_id, code, title, description, date_created, image, recipe_id, exhibition_url)
					VALUES (:museum_id, :code, :title, :description, now(), :image, :recipe_id, :exhibition_url)";
			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':museum_id' => $museum_id,
					':code' => $exhibition_short_code,
					':title' => $title,
					':description' => $desription,
					':image' => $exhibition_image,
					':recipe_id'=> $recipe_id,
					':exhibition_url'=>$exhibition_url,
			) );
			if (!mysql_error()){
				return $database->lastInsertId();
			}
			echo "\t\tERROR!!! - unable to insert exhibition $recipe_id into the database.\n";
			return false;
		}
	}

	/**
	 * Update the recipe last updated date. Usd by the recipe api update script. When a recipe has been processed
	 * and updated this function is called as the last step to mark that the our system has been updated successfully.
	 *
	 * @param unknown $recipe_id
	 * @param unknown $recipe_last_updated
	 * @return boolean
	 */
	public static function updateExhibitionRecipeLastUpdated($recipe_id, $recipe_last_updated)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();
		$sql = "UPDATE exhibitions SET
					recipe_last_updated =:recipe_last_updated
					WHERE recipe_id=:recipe_id";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':recipe_last_updated' => $recipe_last_updated,
				':recipe_id'=> $recipe_id
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		echo "\t\tERROR!!! Unable to update recipe_last_updated date";
		return false;
	}

	// NB DO NOT CHANGE THE ORDER OF THIS ARRAY!!!
	// If php allowed const arrays then that is what this would be marked as.
	// If adding new visualisations, append to the end of the array
	public static $staticVisualisationsDisplayBitMaskOrder = array(
			"summary"=>0,
			"svgcharacter"=>0,
			"passport"=>0,
			"unseen"=>0,
			"favourite"=>0,
			"inspiration"=>0,
			"personalisation"=>0,
			"path"=>0,
			"bubbles"=>0,
			"cloud"=>0,
			"album"=>0,
			"photoperson"=>0
	);

	/**
	 * Update the exhibition visualisation display column based on the supplied array. Convert the array into
	 * a string where allowed visualisations have 1 at the associated position and not allowed visualisations have 0
	 * @param unknown $exhibtion_id
	 * @param unknown $display
	 * @return boolean
	 */
	public static function updateVisualisationDisplaySettings($exhibtion_id, $display){

		// first convert the new value to a string
		$newDisplayPermissionsArray = ExhibitionModel::$staticVisualisationsDisplayBitMaskOrder;
		foreach ($display as $key => $value)
		{
			$newDisplayPermissionsArray[$value] = true;
		}

		//
		$newPermissionString = implode('',array_values($newDisplayPermissionsArray));

		// update the value in the database
		$database = DatabaseFactory::getFactory ()->getConnection ();
		$sql = "UPDATE exhibitions SET
					display_visualisations_bitmask =:newPermissionString
					WHERE exhibition_id=:exhibition_id";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':newPermissionString' => $newPermissionString,
				':exhibition_id'=> $exhibtion_id
		) );

		if ($query->rowCount () == 1) {
			return true;
		}

		Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_EXHIBITION_DISPLAY_UPDATE_FAILED' ) );
		return false;

	}

	public static function canDisplayVisualisation($visualisation, $bitmask){
		$position = array_search($visualisation, array_keys(ExhibitionModel::$staticVisualisationsDisplayBitMaskOrder));

		if(isset($bitmask[$position]) && $bitmask[$position]=='1'){
			return true;
		}
		return false;
	}

	public static function deleteExhibitionByRecipeId($recipe_id)
	{
		// first get the exhibition
		$exhibition = ExhibitionModel::getExhibitionFromReipeId($recipe_id);

		if(isset($exhibition) && isset($exhibition->exhibition_id)){

			// Delete all content for invalid exhibition
			ContentModel::deleteContentsForExhibition($exhibition->exhibition_id, array());

			// Delete all points
			POIModel::deletePOIsForExhibition($exhibition->exhibition_id, array());

			// Delete all languages
			LanguageModel::deleteLanguagesForExhibition($exhibition->exhibition_id, array());

			// Delete all perspectives
			PerspectiveModel::deletePerspectivesForExhibition($exhibition->exhibition_id, array());

			// Now delete the exhibition
			// Only do this once the receipe gets a proper flag
			$database = DatabaseFactory::getFactory ()->getConnection ();

			$sql = "DELETE FROM exhibitions
						WHERE recipe_id=:recipe_id";

			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':recipe_id'=> $recipe_id
			) );

		}

		return;
	}
}