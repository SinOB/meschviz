<?php

/**
 * Admin model
 * @author sin
 *
 */
class AdminModel
{

	public static function updateUserAccount($user_id, $user_account_type, $user_museum_id)
	{

		if (! $user_id || ! $user_account_type)
		{
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "UPDATE users
				SET user_account_type = :user_account_type,
				user_museum = :user_museum
				WHERE user_id = :user_id";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':user_account_type' => $user_account_type,
				':user_museum' => $user_museum_id,
				':user_id' => $user_id
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_USER_EDITING_FAILED' ) );
		return false;
	}
}