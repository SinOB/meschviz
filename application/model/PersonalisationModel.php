<?php
require_once ("../sources/phpSuperAPI.php");

/**
 * PersonalisationModel
 */
class PersonalisationModel {


	// Get object suggestions from UoA based on users visit
	public static function getObjectSuggestion($passcode, $count, $language, $reusability)
	{
		// get the session id
		$visit_session = VisitLogModel::getVisitSessionByPasscode($passcode);
		if(isset($visit_session)){

		// get the exhibition code
		// first extract the exhibition id from the passcode
		list($first, $second, $exhibition_code) = preg_split('/-/', $passcode);
		$exhibition_code =ltrim($exhibition_code, '0');

		$handler = new phpSuperAPI();
		$results = $handler->getObjectSuggestion($visit_session->session, $count, $exhibition_code);
		return $results;
		} else {
			echo "No visit session found. Suspect API is down.";
		}

	}


	public static function getVisitPersonalisation($passcode, $count, $language, $reusability)
	{
		// Get exhibition
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);

		// Get points viewed
		$viewedPoints = VisitLogModel::getVisitPointsByPasscode($passcode);

		// initialise empty array
		$tags = [];

		// set words to ignore for each language
		$ignore_nl = ['door','veel','niet','werden','vanuit','de','en','in','een','de','na','ln','van','het','voor','te','on','werd','was','die','ook','zij','naar','nog', 'uit','hun','met','aan','als'];
		$ignore_en = ['to','a','from','with','for','too','if','where','when','there','of','in','on','was','in','and','by','the','this','be','is','were','be','that','but','or','within','also','at','all','whereas','its','how','had','as','used','it','they','like','have','because','it\'s','them','here','what','their','your','will','dont'];

		if(!isset($viewedPoints)){
			return;
		}

		// Collect an array of all words found and how often found
		foreach($viewedPoints as $point){
			$content = ContentModel::getContentWorkaround(
					$exhibition->exhibition_id,
					$point->audience,
					$point->language,
					$point->perspective,
					$point->id);

			// Make sure we have some text to work with
			if(!isset($content->text) || $content->text==''){
				continue;
			}

			// first remove any newlines
			$words =preg_replace('/\s+/', ' ', $content->text);
			// remove any forward slashes (used in phrases such as and/or)
			$words =preg_replace('|/|', ' ', $words);
			// remove all round brackets
			$words =preg_replace('/\(|\)|:/',' ', $words);

			// now split based on spaces into words
			$words = explode(" ", $words);

			foreach($words as $word){
				$word = trim($word, ',');
				$word = trim($word, '.');
				$word = trim($word, "‘"); // Gets special single quote at start
				$word = trim($word, "’"); // Gets special single quote at end
				$word = trim($word, "'"); // Gets regular single quote
				$word = preg_replace("/\"/", "", $word);
				$word = preg_replace("/\”/", "", $word);
				$word = preg_replace("/\“/", "", $word);
				$word = strtolower($word);

				if( strlen($word)<=3) {
					continue;
				}

				if(strtolower($point->language)=='english'){
					if(in_array($word, $ignore_en) ){
						continue;
					}
				}
				else if(strtolower($point->language)=='dutch'){
					if(in_array($word, $ignore_nl)){
						continue;
					}
				}

				if(isset($tags[$word]))
				{
					$tags[$word]++;
				}else {
					$tags[$word]=1;
				}
			}

		}
		// sort the words by popularity - most popular first
		arsort($tags);

		// Get 3 most popular words
		$subset = array_slice($tags,0,3,1);

		$string = implode('+OR+',array_keys($subset));

		// Make sure string is UTF-8
		$string = iconv(mb_detect_encoding($string),'UTF-8', $string);

		// Call the Europeana ES api to get some suggestions based on the words found
		$data = EuropeanaModel::getSearchResults ($string,
				$count,
				$language,
				$reusability);

		return (object) array('search_string'=>$string,
				'suggestions'=>$data
		);

	}
}
