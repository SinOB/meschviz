<?php

/**
 * ComponentModel
 */
class ComponentModel {

	public static function getComponentsByArticle($article_id) {
		$database = DatabaseFactory::getFactory()->getConnection();
		$sql = "SELECT component_id, source_type, source_id, external_source, article_id, text, license_id
                FROM components WHERE article_id = :article_id";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':article_id' => $article_id
		) );

		return $query->fetchAll();
	}

	public static function getComponent($component_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();
		$sql = "SELECT component_id, source_type, source_id, external_source, article_id, text, license_id
                FROM components WHERE component_id = :component_id";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':component_id' => $component_id
		) );

		return $query->fetch();
	}


	/** /
	 * Return an image src wrapped in <img > tag or text depending on source from component
	 * @param unknown $source_type
	 * @param unknown $source_id
	 * @param unknown $text
	 * @param unknown $external_source
	 * @return string|boolean
	 */
	public static function getComponentHTMLFromSource($source_type, $source_id, $text, $external_source) {

		switch ($source_type) {
			case 'flickr' :
			case 'europeana_direct' : // europeana direct
			case 'europeana': // europeana via mesch api
			case 'museia': // museia via mesch api
			case 'external': // linked directly from the web
				return '<img id="' . $source_id . '" src="' . $external_source . '" alt="">';
				break;
			case 'text':
				return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
				break;
			case 'upload':
				return '<img id="' . $source_id . '" src="' . $external_source . '" alt="">';
				break;
		}
		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_COMPONENT_SOURCE_INVALID' ) );
		return false;
	}


	/** /
	 * Search availble sources for components that match search term
	 * @param unknown $source_search
	 * @param unknown $count
	 * @return void|multitype:
	 */
	public static function searchComponents($source_search, $count, $language, $reusability) {
		if (! $source_search) {
			return;
		}

		$results = array ();

		// If no search source or search source is europeana direct
		if(null === Request::post( 'search_source' ) || Request::post( 'search_source' ) =='europeana_direct')
		{
			$europeana_direct_data = EuropeanaModel::getSearchResults($source_search, $count, $language, $reusability);
			$results = array_merge( $results, $europeana_direct_data);
		}
		// Get public Flicker images
		else if (Request::post( 'search_source' ) == 'flickr') {
			$flickr_data = FlickrModel::getSearchResults( $source_search, $count, $language, $reusability);
			$results = array_merge( $results, $flickr_data );
		}
		// Get Museia info via mesch api
		else if(Request::post( 'search_source' ) =='museia'){
			$mesch_museia_data = SuperApiModel::getSearchResults( $source_search, $count, 'museia', $language, $reusability);
			$results = array_merge( $results, $mesch_museia_data);
		}
		// Get Ectrl/UoA europeana subset with bad images
		else if(Request::post( 'search_source' ) =='europeana'){
			$mesch_europena_data = SuperApiModel::getSearchResults( $source_search, $count, 'europeana', $language, $reusability);
			$results = array_merge( $results, $mesch_europena_data);
		}

		return $results;
	}

	/** /
	 * Save a component to the database
	 * @param unknown $article_id
	 * @param unknown $source_id
	 * @param unknown $source_type
	 * @param unknown $text
	 * @param unknown $external_source
	 * @return boolean
	 */
	// upload component to copyright table
	public static function createComponent($article_id, $source_id, $source_type, $text, $external_source, $license_id) {
		if (! $article_id || ! $source_type  ) {
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_COMPONENT_CREATION_FAILED' ) );
			return false;
		}
		if(!($text ||$external_source))
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_COMPONENT_CREATION_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "INSERT INTO components (article_id, source_type, source_id, external_source, text, date_created, user_id, license_id)
				VALUES (:article_id, :source_type, :source_id, :external_source, :text, now(), :user_id, :license_id)";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':article_id' => $article_id,
				':source_type' => $source_type,
				':source_id' => $source_id,
				':external_source' => $external_source,
				':text' => $text,
				':user_id' => Session::get( 'user_id' ),
				':license_id' => $license_id
		) );

		if ($query->rowCount() == 1) {
			$sql = "SELECT component_id FROM components
					WHERE article_id = :article_id
					AND source_type=:source_type
					AND source_id = :source_id
					AND external_source=:external_source
					AND text=:text
					AND user_id=:user_id";

			$query = $database->prepare( $sql );
			$query->execute( array (
					':article_id' => $article_id,
					':source_type' => $source_type,
					':source_id' => $source_id,
					':external_source' => $external_source,
					':text' => $text,
					':user_id' => Session::get( 'user_id' )
			) );

			// echo the added component id for use with the ajax call
			$result = $query->fetch();

			if($source_type != 'upload' && $source_type != 'text' && $source_type != 'external'){
				echo $result->component_id;
			}
			return true;
		}

		// default return
		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_COMPONENT_CREATION_FAILED' ) );
		return false;
	}


	public static function uploadComponent($article_id, $file, $license_id)
	{
		// upload to the local directory
		$folder = Config::get( 'PATH_IMAGES' ) . 'uploads/';
		$extension = pathinfo( $file['file_upload']['name'], PATHINFO_EXTENSION );

		$new_filename = uniqid(). '.' . $extension;

		// check folder writing rights, check if upload fits all rules
		if (FileModel::isFolderWritable( $folder ) and FileModel::validateImageFile( $file['file_upload'] ))
		{
			// remove any existing file that could correspond with new name
			FileModel::delete($folder, $new_filename);

			// save the file
			if (! move_uploaded_file( $file['file_upload']['tmp_name'], $folder . $new_filename ))
			{
				Session::add( 'feedback_negative', Text::get( 'FEEDBACK_FILE_IMAGE_UPLOAD_FAILED' ) );
				return false;
			}

			$external_source=Config::get( 'IMAGES_URL' ) . 'uploads/'.$new_filename;

			// create as a component on the system
			ComponentModel::createComponent($article_id, $new_filename, 'upload', '', $external_source, $license_id);

			return true;
		}
		return false;


	}

	/**
	 * Delete a specific component
	 *
	 * @param int $component_id id of the component
	 * @return bool feedback (was the component deleted properly ?)
	 */
	public static function deleteComponent($component_id) {
		if (! $component_id) {
			return false;
		}

		$component = ComponentModel::getComponent($component_id);

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "DELETE FROM components WHERE component_id = :component_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':component_id' => $component_id,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1) {
			// if is of type ='upload' - delete the file from the upload directory
			if($component->source_type=='upload')
			{
				$folder = Config::get( 'PATH_IMAGES' ) . 'uploads/';
				FileModel::delete($folder, $component->source_id);
			}
			return true;
		}

		// default return
		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_COMPONENT_DELETION_FAILED' ) );
		return false;
	}
}
