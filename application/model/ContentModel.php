<?php

/**
 * ContentModel
 */
class ContentModel {

	public static function createUpdate($uid, $exhibition_id, $target_audience, $language, $perspective,$point_of_interest, $type, $title, $url, $text) {

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT content_id, uid, exhibition_id, title
		FROM content
		WHERE uid=:uid
		AND exhibition_id=:exhibition_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':uid' => $uid,
				':exhibition_id' => $exhibition_id
		) );

		$content = $query->fetch();
		// if record exists - do an update
		if(isset($content) && $content!='')
		{
			echo "\t\tUPDATE data for content $uid.\n";
			$sql = "UPDATE content SET
					target_audience =:audience,
					language =:language,
					perspective=:perspective,
					point_of_interest =:point_of_interest,
					type =:type,
					title=:title,
					url =:url,
					text=:text
					WHERE uid=:uid
					AND exhibition_id=:exhibition_id";

			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':uid' => $uid,
					':exhibition_id' => $exhibition_id,
					':audience'=>$target_audience,
					':language' => $language,
					':perspective' => $perspective,
					':point_of_interest' => $point_of_interest,
					':type' => $type,
					':title' => $title,
					':url' => $url,
					':text' => $text
			) );
			if (!mysql_error()){
				return $content->content_id;
			}
			echo "\t\tERROR!!! - unable to update content $uid into the database.\n";
			return false;
		}
		else
		{
			echo "\t\tINSERT data for content $uid.\n";

			$insert_sql = "INSERT INTO content (uid, exhibition_id, target_audience, language, perspective,point_of_interest, type, title, url, text, date_created)
		        		VALUES (:uid, :exhibition_id, :audience, :language, :perspective, :point_of_interest, :type, :title, :url, :text, now())";
			$query = $database->prepare ( $insert_sql );
			$query->execute ( array (
					':uid' => $uid,
					':exhibition_id' => $exhibition_id,
					':audience'=>$target_audience,
					':language' => $language,
					':perspective' => $perspective,
					':point_of_interest' => $point_of_interest,
					':type' => $type,
					':title' => $title,
					':url' => $url,
					':text' => $text
			) );
			if (!mysql_error()){
				return $database->lastInsertId();
			}
			echo "\t\tERROR!!! - unable to insert content $uid into the database.\n";
			return false;
		}

	}

	public static function getContent($uid, $exhibition_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT target_audience, language, perspective, point_of_interest, title, url, text, type
		FROM content
		WHERE uid=:uid
		AND exhibition_id=:exhibition_id
		ORDER BY language, perspective";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':uid' => $uid,
				':exhibition_id'=>$exhibition_id,
		) );

		return $query->fetch();
	}

	public static function getSingleContentImageByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT url
		FROM content
		WHERE exhibition_id=:exhibition_id
		AND type in ('image', 'snapshot')
		LIMIT 1";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetch();
	}

	/**
	 * Atlantikwall logs will not include the content uid for points visited. As a work around, use the
	 * tags (language, perspective and point_of_interest) to determine the content uid and then return the
	 * content from the content table
	 *
	 * @param unknown $exhibition_id
	 * @param unknown $language
	 * @param unknown $perspective
	 * @param unknown $point_of_interest
	 */
	public static function getContentWorkaround($exhibition_id, $audience, $language, $perspective, $point_of_interest)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT content_id, uid, exhibition_id, target_audience, language, perspective, point_of_interest, date_created, type
				FROM content
				WHERE exhibition_id = :exhibition_id
				AND target_audience = :audience
				AND language =:language
				AND perspective = :perspective
				AND point_of_interest = :point_of_interest
				ORDER BY language, perspective";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':audience'=>$audience,
				':language' => $language,
				':perspective'=> $perspective,
				':point_of_interest'=>$point_of_interest,
				':exhibition_id'=>$exhibition_id
		));

		$result = $query->fetch();

		return ContentModel::getContent($result->uid, $exhibition_id);
	}


	public static function GetContentByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT content_id, uid, exhibition_id, target_audience, language, perspective, point_of_interest, title, url, text, date_created, type
				FROM content
				WHERE exhibition_id = :exhibition_id
				ORDER BY language, perspective";

		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id'=>$exhibition_id
		));

		return $query->fetchAll();

	}

public static function deleteContentsForExhibition($exhibition_id, $keep) {

		if (! $exhibition_id) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();


		// Need to cleanly handle array of WHERE IN
		if(!isset($keep) || count($keep)<=0){
			// if dont want to keep any contents at all
			$parameters = [0];
			$questionmarks = "?";
		} else {
			$parameters = $keep;
			// Need to cleanly handle array of WHERE IN
			$questionmarks = str_repeat("?,", count($keep)-1) . "?";
		}

		// PDO does not appear to have a very clean way to handle 'where in' array - have to build query manually
		// and bind the variables as below
		$sql = "DELETE FROM content
		WHERE exhibition_id = ?
		AND uid not in ($questionmarks)";

		// append exhibition id to parameters array

		array_unshift($parameters, $exhibition_id);

		$query = $database->prepare ( $sql );
		$query->execute ($parameters);

		if ($query->rowCount () >= 1) {
			return true;
		}

		echo "\t\tNo contents deleted from the database.\n";
		return false;

	}
}
