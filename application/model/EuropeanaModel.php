<?php
require_once ("../sources/phpEuropeana.php");

class EuropeanaModel {

	public static function getSearchResults($string, $count, $language, $reusability) {

		$f = new phpEuropeana(Config::get('EUROPEANA_API_KEY'));
		$data = $f->search ($string, $count, $language, $reusability);

		$results = array();
		$licenses = LicenseModel::getLicensesAsUrlArray();

		if(isset($data) and isset($data->items) && count($data->items)>0){
			foreach($data->items as $item)
			{
				// Need to handle case where license is not currently set in the database
				if(!isset($licenses[$item->rights[0]]->license_id))
				{
					LicenseModel::missingLicense($item->rights[0]);
				}

				// Limit to case where is an image AND has an image source
				if ( $item->type === 'IMAGE'
					 &&	isset($item->edmPreview) && $item->edmPreview[0] !=''
					 &&	isset($licenses[$item->rights[0]]->license_id)){

					$component['license_id'] = $licenses[$item->rights[0]]->license_id;
					$component ['id'] = $item->id;
					$component ['source'] = 'europeana_direct';
					$component['external_source'] = $item->edmPreview[0];
					// if we have a link to a page that displays further info - get it
					if(isset($item->edmIsShownAt)){
						$component['shown_at']=$item->edmIsShownAt[0];
					}
					$component ['html'] = '<img id="' . $item->id . '" src="'.$item->edmPreview[0] . '" alt="" >';
					$results [] = $component;
				}
			}
		}
		return $results;
	}
}

?>