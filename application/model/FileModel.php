<?php

class FileModel
{

	/**
	 * Checks if the file folder exists and is writable
	 *
	 * @return bool success status
	 */
	public static function isFolderWritable($folder)
	{
		if (is_dir($folder) AND is_writable($folder)) {
			return true;
		}

		Session::add('feedback_negative', Text::get('FEEDBACK_FILE_FOLDER_DOES_NOT_EXIST_OR_NOT_WRITABLE'));
		return false;
	}

	/**
	 * Validates the image
	 * @param filehandler $file is $_FILES['file_name']
	 * @return boolean
	 */
	public static function validateImageFile($file)
	{
		// are there any errors
		if (!isset($file['error']) || is_array($file['error'])) {
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_FILE_IMAGE_UPLOAD_FAILED' ) );
			return false;
		}

		// if $_FILES['file_name'] is not set
		if (!isset($file)) {
			Session::add('feedback_negative', Text::get('FEEDBACK_FILE_IMAGE_UPLOAD_FAILED'));
			return false;
		}

		if(!file_exists( $file['tmp_name'] )){
			Session::add('feedback_negative', Text::get('FEEDBACK_FILE_IMAGE_UPLOAD_TOO_BIG'));
			return false;
		}

		// if input file too big (>5MB)
		if ($file['size'] > 5000000) {
			Session::add('feedback_negative', Text::get('FEEDBACK_FILE_IMAGE_UPLOAD_TOO_BIG'));
			return false;
		}

		// get the image width, height and mime type
		$image_proportions = getimagesize($file['tmp_name']);
		if (!($image_proportions['mime'] == 'image/jpeg' || $image_proportions['mime'] == 'image/png' ||$image_proportions['mime'] ==  'image/gif')) {
			Session::add('feedback_negative', Text::get('FEEDBACK_FILE_IMAGE_UPLOAD_WRONG_TYPE'));
			return false;
		}

		return true;
	}

	public static function delete($folder, $filename)
	{
		if (file_exists( $folder . $filename ))
		{
			unlink( $folder . $filename );
		}
	}

}