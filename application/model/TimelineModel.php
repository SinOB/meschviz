<?php

/**
 * TimelineModel
 */
class TimelineModel
{

	/**
	 * Get a single timeline
	 *
	 * @param int $timeline_id id of the specific timeline
	 * @return object a single object (the result)
	 */
	public static function getTimeline($timeline_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT user_id, timeline_id, title, subtitle, exhibition_id, type, title_image, timeline_image, status
				FROM timelines
				WHERE timeline_id = :timeline_id
				LIMIT 1";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':timeline_id' => $timeline_id
		) );

		return $query->fetch();
	}


	public static function getHistoricalTimelinesByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, timeline_id, title, subtitle, exhibition_id, type, title_image, timeline_image, status
				FROM timelines
				WHERE exhibition_id = :exhibition_id
				AND type=1
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll ();
	}

	public static function getUserVisitTimelinesByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT user_id, timeline_id, title, subtitle, exhibition_id, type, title_image, timeline_image, status
				FROM timelines
				WHERE exhibition_id = :exhibition_id
				AND type=2
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll ();
	}


	public static function updateImage($timeline_id, $field, $file)
	{
		$folder = Config::get( 'PATH_IMAGES' ) . 'timelines/';
		$extension = pathinfo( $file['name'], PATHINFO_EXTENSION );

		$new_filename = $timeline_id . '_' . $field . '.' . $extension;

		if(!FileModel::validateImageFile( $file )){
			return false;
		}

		// check folder writing rights, check if upload fits all rules
		if (FileModel::isFolderWritable( $folder ))
		{
			// remove any existing file that could correspond with new name
			FileModel::delete($folder, $new_filename);

			// remove any existing file with old name (we have two deletes because the extensions may vary)
			$timeline = TimelineModel::getTimeline($timeline_id);
			FileModel::delete($folder, $timeline->$field);

			// save the file
			if (! move_uploaded_file( $file ['tmp_name'], $folder . $new_filename ))
			{
				Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_IMAGE_UPLOAD_FAILED' ) );
				return false;
			}

			$database = DatabaseFactory::getFactory()->getConnection();

			$sql = "UPDATE timelines
				SET " . $field . " =:" . $field . " WHERE timeline_id = :timeline_id
				AND user_id = :user_id";
			$query = $database->prepare( $sql );

			$query->execute( array (
					':timeline_id' => $timeline_id,
					':' . $field => $new_filename,
					':user_id' => Session::get( 'user_id' )
			) );
			return true;
		}
		return false;
	}

	/**
	 * Get all timelines in the system owned by the current user
	 *
	 * @return array an array with several objects
	 */
	public static function getUserTimelines()
	{

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT user_id, timeline_id, title, title_image, status FROM timelines WHERE user_id = :user_id ORDER BY date_created DESC";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':user_id' => Session::get( 'user_id' )
		) );

		return $query->fetchAll();
	}

	/**
	 * Create a timeline
	 *
	 * @param string $title title of timeline that will be created
	 * @param int $exhibition_id unique id of exhibition
	 * @param unknown $type is it a historical or user visit timeline
	 * @return boolean feedback (was the timeline created properly ?)
	 */
	public static function create($title, $subtitle, $type, $exhibition_id)
	{

		if (! $title || strlen( $title ) == 0 || ! $exhibition_id || ! $type)
		{
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_CREATION_FAILED' ) );
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "INSERT INTO timelines (title, subtitle, exhibition_id, type, user_id, date_created)
				VALUES (:title, :subtitle, :exhibition_id, :type, :user_id, now())";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':title' => $title,
				':subtitle' => $subtitle,
				':exhibition_id' => $exhibition_id,
				':type' => $type,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_CREATION_FAILED' ) );
		return false;
	}

	public static function updateTitleImageFile($timeline_id, $files)
	{
		if (!$timeline_id || !$files)
		{
			return false;
		}
		if (isset( $files ) && isset( $files ['title_image'] ) )
		{
			$field = 'title_image';
			if (! TimelineModel::updateImage( $timeline_id, $field, $files['title_image'] ))
			{
				return false;
			}
		} else {
			Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_IMAGE_UPLOAD_FAILED' ) );
			return false;
		}
	}

	public static function updateTimelineImageFile($timeline_id, $files)
	{
		if (! $timeline_id || !$files)
		{
			return false;
		}

		if (isset( $files ) && isset( $files ['timeline_image'] ) )
		{
			$field = 'timeline_image';
			if (! TimelineModel::updateImage( $timeline_id, $field, $files['timeline_image'] ))
			{
				return false;
			}
		}
	}

	/**
	 * Update the details of the timeline including setting the background images
	 *
	 * @param unknown $timeline_id
	 * @param unknown $title
	 * @param unknown $subtitle
	 * @param unknown $type
	 * @param unknown $files
	 * @return boolean
	 */
	public static function updateTimeline($timeline_id, $title, $subtitle, $type, $status)
	{
		if (! $timeline_id || ! $title || ! $type)
		{
			return false;
		}

		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "UPDATE timelines
				SET title = :title,
				subtitle = :subtitle,
				type = :type,
				status =:status
				WHERE timeline_id = :timeline_id
				AND user_id = :user_id";
		$query = $database->prepare( $sql );

		$query->execute( array (
				':timeline_id' => $timeline_id,
				':title' => $title,
				':subtitle' => $subtitle,
				':type' => $type,
				':status'=> $status,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_EDITING_FAILED' ) );
		return false;
	}


	public static function deleteImage($timeline_id, $type)
	{
		if (! $timeline_id || !$type)
		{
			return false;
		}

		// get image data so can delete from server if db delete is successful
		$timeline = TimelineModel::getTimeline($timeline_id);

		$database = DatabaseFactory::getFactory()->getConnection();

		switch($type)
		{
			case 'title_image':
				$sql = "UPDATE timelines
				SET title_image = :image
				WHERE timeline_id = :timeline_id
				AND user_id = :user_id";
				break;
			case 'timeline_image':
				$sql = "UPDATE timelines
				SET timeline_image = :image
				WHERE timeline_id = :timeline_id
				AND user_id = :user_id";
				break;
			default:
				return;
		}

		$query = $database->prepare( $sql );
		$image = null;
		$query->execute( array (
				':timeline_id' => $timeline_id,
				':image' => $image,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			$folder = Config::get( 'PATH_IMAGES' ) . 'timelines/';
			FileModel::delete($folder, $timeline->$type);
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_EDITING_FAILED' ) );
		return false;

	}
	/**
	 * Delete a specific timeline
	 *
	 * @param int $timeline_id id of the timeline
	 * @return bool feedback (was the timeline deleted properly ?)
	 */
	public static function delete($timeline_id)
	{

		if (! $timeline_id)
		{
			return false;
		}

		// get timeline info so can delete images from server if db delete successful
		$timeline = TimelineModel::getTimeline($timeline_id);

		// now delete from the database
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "DELETE FROM timelines WHERE timeline_id = :timeline_id AND user_id = :user_id LIMIT 1";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':timeline_id' => $timeline_id,
				':user_id' => Session::get( 'user_id' )
		) );

		if ($query->rowCount() == 1)
		{
			$folder = Config::get( 'PATH_IMAGES' ) . 'timelines/';
			FileModel::delete($folder, $timeline->title_image);
			FileModel::delete($folder, $timeline->timeline_image);
			return true;
		}

		Session::add( 'feedback_negative', Text::get( 'FEEDBACK_TIMELINE_DELETION_FAILED' ) );
		return false;
	}
}