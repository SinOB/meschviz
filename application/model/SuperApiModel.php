<?php
require_once ("../sources/phpSuperAPI.php");

class SuperApiModel {

	public static function test_logs()
	{
		$f = new phpSuperAPI();
		return $f->test_logs ();
	}

	public static function test_europeana()
	{
		return SuperApiModel::getEuropeanaSearchResults('test', 1, null, null);
	}

	public static function test_museia()
	{
		return SuperApiModel::getMuseiaSearchResults('test', 1, null, null);
	}

	public static function test_UoA_Europeana(){
		$f = new phpSuperAPI();
		return  $f->test_UoA_Europeana();
	}

	public static function getSearchResults($string, $count, $type, $language, $reusability) {

		if($type=='europeana')
		{
			return SuperApiModel::getEuropeanaSearchResults($string, $count, $language, $reusability);
		}
		if($type=='museia')
		{
			return SuperApiModel::getMuseiaSearchResults($string, $count, $language, $reusability);
		}
		return false;
	}

	/**
	 * Process the results of a search of the Europeana database received from the Authoring tool
	 *
	 * @param search string $string
	 * @param number of results to return $count
	 * @return array of formatted search results
	 */
	public static function getEuropeanaSearchResults($string, $count, $language, $reusability)
	{
		$f = new phpSuperAPI();
		$data = $f->getEuropeanaSearch ($string, $count, $language, $reusability);

		$results = array();
		$licenses = LicenseModel::getLicensesAsUrlArray();

		if(isset($data)){
			foreach($data as $item)
			{
				// Need to handle case where license is not currently set in the database
				if(!isset($licenses[$item->_source->edm_rights_url]->license_id))
				{
					LicenseModel::missingLicense($item->_source->edm_rights_url);
				}

				// Limit to case where is an image AND has an image source
				if ( $item->_source->edm_type === 'IMAGE' &&
						isset($item->_source->edm_preview) && $item->_source->edm_preview !='' &&
						isset($licenses[$item->_source->edm_rights_url]->license_id)){
					$component['license_id'] = $licenses[$item->_source->edm_rights_url]->license_id;
					$component ['id'] = $item->_id;
					$component ['source'] = 'europeana';
					$component['external_source'] = $item->_source->edm_preview;
					$component ['html'] = '<img id="' . $item->_id . '" src="'.$item->_source->edm_preview . '" alt="" >';
					$results [] = $component;
				}
			}
		}

		return $results;
	}


	/**
	 * Peform a search of Museia via msch api.
	 * Not sure why one would bother - as do not have permission to use any images
	 * and results do not appear to return any text other than a title...
	 *
	 * @param unknown $string
	 * @param unknown $count
	 * @return multitype:|multitype:string
	 */
	// TODO hardcoding the license to all rights reserved
	// Using source->Codice as the id of the image
	public static function getMuseiaSearchResults($string, $count, $language, $reusability)
	{
		$f = new phpSuperAPI();
		$data = $f->getMuseiaSearch($string, $count, $language, $reusability);
		$licenses = LicenseModel::getLicensesAsFlickrArray();

		$results = array();
		if(isset($data)){
			foreach ( $data  as $item ) {
				// TODO change this when result returned included copyright
				// for the moment hardcode copyright to all rights reserved
				$component['license_id'] =$licenses[0]->license_id;
				$component ['id'] = $item->_source->Codice;
				$component ['source'] = 'museia';
				$component['external_source']='http://images.suggesto.eu/mdg/museia/'.$item->_source->Nome_File;
				$component ['html'] = '<img width="250px" id="' . $component ['id'] . '" src="'.$component['external_source'] . '" alt="" >';
				$results[] = $component;
			}
		}
		return $results;
	}

}
