<?php

/**
 * MuseumModel
 */
class MuseumModel {

	public static function getMuseums()
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT museum_id, description FROM museums ORDER BY museum_id";
		$query = $database->prepare ( $sql );
		$query->execute ();

		return $query->fetchAll ();

	}

	public static function getMuseumId($authoring_tool_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT museum_id
		FROM museums
		WHERE authoring_tool_id = :authoring_tool_id";

		$query = $database->prepare( $sql );
		$query->execute( array (
				':authoring_tool_id' => $authoring_tool_id
		) );

		$row =  $query->fetch();

		if(isset($row) && $row!=''){
			$result = $row->museum_id;
			return $result;
		}

	}
	/**
	 * Create/Update museums and return museum_id based on data supplied by recipe API
	 *
	 * @param unknown $museum_authoring_tool_id
	 * @param unknown $museum_name
	 * @return boolean
	 */
	public static function createUpdateMuseum($museum_authoring_tool_id, $museum_name) {
		// Check if the museum already exists
		$database = DatabaseFactory::getFactory()->getConnection();
		$sql = "SELECT museum_id, description, authoring_tool_id
                FROM museums WHERE authoring_tool_id = :authoring_tool_id";
		$query = $database->prepare( $sql );
		$query->execute( array (
				':authoring_tool_id' => $museum_authoring_tool_id
		) );

		$museum =  $query->fetch();

		// if record exists - do an update
		if(isset($museum) && $museum!='')
		{
			echo "\tUPDATE data for museum $museum_authoring_tool_id.\n";

			$sql = "UPDATE museums
				SET description = :description
				WHERE authoring_tool_id = :authoring_tool_id";
			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':authoring_tool_id' => $museum_authoring_tool_id,
					':description' => $museum_name,
			) );

			if (!mysql_error()){
				return $museum->museum_id;
			}

			echo "\tError - unable to update museum entry $museum_authoring_tool_id.\n";
			return false;

		}
		// otherwise do an insert
		else
		{
			echo "\tINSERT data for museum $museum_authoring_tool_id.\n";
			$sql = "INSERT INTO museums (description, authoring_tool_id)
					VALUES (:description, :authoring_tool_id)";
			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':description' => $museum_name,
					':authoring_tool_id' => $museum_authoring_tool_id
			) );
			if (!mysql_error()){
				return $database->lastInsertId();
			}
			echo "\tError - unable to insert museum $museum_authoring_tool_id into the database.\n";
			return false;
		}

	}
}
