<?php

/**
 * LicenseModel
 */
class LicenseModel {
	/**
	 * Get all copyright licenses
	 *
	 * @return array an array with several objects
	 */
	public static function getLicenses() {
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT license_id, title, url, reusability, flickr_id
				FROM licenses
				ORDER BY license_id";

		$query = $database->prepare ( $sql );
		$query->execute (array());

		return $query->fetchAll ();
	}

	public static function getLicensesAsFlickrArray(){
		$licenses = LicenseModel::getLicenses();
		$results = array();
		foreach($licenses as $license)
		{
			if($license->flickr_id!=''){
				$results[$license->flickr_id]=$license;
			}
		}
		return $results;
	}

	public static function getLicensesAsUrlArray(){
		$licenses = LicenseModel::getLicenses();
		$results = array();
		foreach($licenses as $license)
		{
			$results[$license->url]=$license;
		}
		return $results;
	}

	public static function getLicensesAsArray()
	{
		$licenses = LicenseModel::getLicenses();
		$results = array();
		foreach($licenses as $license)
		{
			$results[$license->license_id]=$license;
		}
		return $results;
	}

	/**
	 * How to handle case where the license (usually from europeana) has not been found in our database.
	 * EMail the admin and let them know to update the database accordingly.
	 *
	 * @param unknown $license_url
	 */
	public static function missingLicense($license_url)
	{
		error_log("Sending email to request following license be added to license database ".$license_url);
		$subject = 'Missing copyright from meschviz database';
		$body='Please add the following license to the licenses table: ['.$license_url.']';
		$mail = new Mail;
		$mail_sent = $mail->sendMail(Config::get('EMAIL_SYSTEM_NOTIFY_ADMIN'),
				Config::get('EMAIL_SYSTEM_FROM_EMAIL'),
				Config::get('EMAIL_SYSTEM_FROM_NAME'),
				$subject, $body);
	}
}
