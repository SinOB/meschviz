<?php
class PerspectiveModel
{
	/**
	 * Create/Update exhibition perspectives based on data supplied by recipe API
	 *
	 * @param unknown $perspective_string
	 * @param unknown $exhibition_id
	 * @return boolean
	 */
	public static function createUpdatePerspective($perspective_string, $exhibition_id)
	{
		$database = DatabaseFactory::getFactory()->getConnection();

		$sql = "SELECT id, perspective, exhibition_id
		FROM perspectives
		WHERE perspective=:perspective
		AND exhibition_id=:exhibition_id";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':perspective' => $perspective_string,
				':exhibition_id' => $exhibition_id
		) );

		$perspective = $query->fetch();
		// if record exists - do an update
		if(isset($perspective) && $perspective!='')
		{
			echo "\t\tUPDATE data for perspective $perspective_string.\n";
			$sql = "UPDATE perspectives SET
					perspective =:perspective,
					exhibition_id=:exhibition_id
					WHERE id=:id";

			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':perspective' => $perspective_string,
					':exhibition_id' => $exhibition_id,
					':id' => $perspective->id
			) );
			if (!mysql_error()){
				return $perspective->id;
			}
			echo "\t\tERROR!!! - unable to update perspective $perspective_string into the database.\n";
			return false;
		}
		else
		{
			echo "\t\tINSERT data for perspective $perspective_string.\n";
			$sql = "INSERT INTO perspectives
					(perspective, exhibition_id, date_created)
					VALUES (:perspective, :exhibition_id, now())";
			$query = $database->prepare ( $sql );
			$query->execute ( array (
					':perspective' => $perspective_string,
					':exhibition_id' => $exhibition_id,
			) );
			if (!mysql_error()){
				return $database->lastInsertId();
			}
			echo "\t\tERROR!!! - unable to insert perspective $perspective_string into the database.\n";
			return false;

		}
	}

	/**
	 * Get available perspectives for a specific exhibition
	 * @param unknown $exhibition_id
	 */
	public static function getPerspectivesByExhibition($exhibition_id)
	{
		$database = DatabaseFactory::getFactory ()->getConnection ();

		$sql = "SELECT id, exhibition_id, perspective, date_created
				FROM perspectives WHERE exhibition_id = :exhibition_id
				ORDER BY date_created DESC";
		$query = $database->prepare ( $sql );
		$query->execute ( array (
				':exhibition_id' => $exhibition_id
		) );

		return $query->fetchAll ();

	}

	/**
	 * Delete all old perspectives affiliated with an exhibition. Used by the authoring tool recipe update script.
	 * @string $exhibition_id
	 * @array $keep_perspectives
	 * @return boolean
	 */
	public static function deletePerspectivesForExhibition($exhibition_id, $keep_perspectives) {
		if (! $exhibition_id ) {
			return false;
		}

		$database = DatabaseFactory::getFactory ()->getConnection ();

		// Need to cleanly handle array of WHERE IN
		if(!isset($keep_perspectives) || count($keep_perspectives)<=0){
			// if dont want to keep any contents at all
			$parameters = [0];
			$questionmarks = "?";
		} else {
			$parameters = $keep_perspectives;
			$questionmarks = str_repeat("?,", count($keep_perspectives)-1) . "?";
		}

		// PDO does not appear to have a very clean way to handle 'where in' array - have to build query manually
		// and bind the variables as below
		$sql = "DELETE FROM perspectives
		WHERE exhibition_id = ?
		AND perspective not in ($questionmarks)";

		// append exhibition id to parameters array
		array_unshift($parameters, $exhibition_id);

		$query = $database->prepare ( $sql );
		$query->execute ($parameters);

		if ($query->rowCount () >= 1) {
			return true;
		}

		echo "\t\tNo perspectives deleted from the database.\n";
		return false;

	}
}
