<?php
require_once ("../sources/phpSuperAPI.php");

class VisitLogModel
{

	/**
	 * Get unprocessed log of visit for a specific passcode
	 * Should never be called directly by visualisation. Use getVisitPointsByPasscode instead
	 *
	 * @param unknown $passcode
	 * @return unknown
	 */
	private static function getLogByPasscode($passcode)
	{

		$handler = new phpSuperAPI();
		$logInfoViewed = $handler->getVisitLogByPasscode( $passcode );

		return $logInfoViewed;
	}

	/**
	 * Used to check if there is a record available from the API for the supplied passcode
	 * @param unknown $passcode
	 * @return unknown
	 */
	public static function getVisitSessionByPasscode($passcode)
	{
		$handler = new phpSuperAPI();
		$result = $handler->getVisitSessionByPasscode( $passcode );
		return $result;
	}

	/**
	 * Get array of point objects from visit based on passcode - cleaned up version log result
	 *
	 * @param unknown $passcode
	 * @return array of point objects ordered by visit order
	 */
	// TODO ignores case where user has visited same point multiple times with same perspective, language and audence
	public static function getVisitPointsByPasscode($passcode)
	{
		$log = VisitLogModel::getLogByPasscode( $passcode );
		$points = array ();

		$i = 0;
		$max_timestamp=0;

		if(isset($log)){
			// start at beginning of visit
			foreach ( array_reverse($log)  as $row )
			{
				// Skip this field as does not always have perspective/language
				if (strtolower($row->eventtype) == 'start visit')
				{
					continue;
				}

				if (strtolower($row->eventtype) == 'stop visit')
				{
					$max_timestamp = $row->timestamp;
					continue;
				}

				// want to catch each point separately for each language/perspective/audience
				$point_id = $row->poi.'_'.$row->perspective.'_'.$row->language.'_'.$row->audience;

				// if point already exists
				if(isset($points[$point_id]))
				{
					// set the end timestamp
					$points[$point_id]->end_time= $row->timestamp;
				} else {
					// create the point in the first place
					$point = new stdClass();
					$point->id = $row->poi;
					$point->language=$row->language;
					$point->audience=$row->audience;
					$point->perspective=$row->perspective;
					$point->start_time = $row->timestamp;
					$point->end_time = $row->timestamp;
					$point->order = $i;
					$i++;
					$points[$point_id] = $point;
				}
			}

			// sort the points into order;
			usort( $points, function ($a, $b) {
				if ($a->order == $b->order) {
					return 0;
				}
				return ($a->order < $b->order) ? -1 : 1;

			} );

			// convert to indexed array
			$points = array_values($points);

			// If no endpoint - set the endpoint to next points startpoint timestamp
			for($j=0; $j<count($points);$j++){
				if($points[$j]->end_time==$points[$j]->start_time){
					if(isset($points[$j+1])){
						$points[$j]->end_time = $points[$j+1]->start_time;
					} else {
						// if no next point available - use the checkout timestamp
						$points[$j]->end_time = $max_timestamp;
					}
				}

				// calculate the duration visitor stayed on point
				$diff = strtotime( $points[$j]->end_time ) - strtotime( $points[$j]->start_time );
				$points[$j]->duration = $diff;
			}

			return $points;
		}
	}

	public static function getFavoritePoint($passcode){
		$pointsViewed = VisitLogModel::getVisitPointsByPasscode( $passcode );
		$max_point = $pointsViewed[0];
		if(isset($pointsViewed)){
			foreach( $pointsViewed as $point )
			{
				if($point->duration>$max_point->duration)
				{
					$max_point = $point;
				}
			}
			return $max_point;
		}
	}


	public static function getFavoritePoints($passcode, $count){
		$pointsViewed = VisitLogModel::getVisitPointsByPasscode( $passcode );

		function durationDecSort($point1,$point2)
		{
			if ($point1->duration == $point2->duration) return 0;
			return ($point1->duration < $point2->duration) ? 1 : -1;
		}
		usort($pointsViewed,'durationDecSort');

		// only return the specified number of results
		return array_slice ($pointsViewed, 0, $count, true);
	}

	/**
	 * Return average time spent per point during a visit
	 *
	 * @param array of point objects $pointsViewed
	 */
	public static function getAverageTimeViewedPoint($pointsViewed)
	{

		$viewed = count( $pointsViewed );
		$total = 0;
		foreach ( $pointsViewed as $point )
		{
			$total += $point->duration;
		}

		$average_seconds_viewed = $total / $viewed;

		return round( $average_seconds_viewed );
	}


	/**
	 * Get percentage of items viewed in a single visit
	 * For a single passcode visit
	 *
	 * @param array of point objects $pointsViewed
	 * @param array of all possible points at exhibition $allPoints
	 */
	public static function getPercentageOfPointsVisited($pointsViewed, $allPoints)
	{

		$viewed_count = count( $pointsViewed );
		$available_count = count( $allPoints );
		$percentage = ($viewed_count / $available_count) * 100;

		return round( $percentage );
	}


	/**
	 * Get percentage of content viewed in a single visit
	 * For a single passcode visit
	 * @param array of point objects $pointsViewed
	 * @param array of all possible points at exhibition $allPoints
	 */
	// TODO need to get duration from authoring tool
	public static function getPercentageOfContentConsumed($pointsViewed, $allPoints)
	{
		// get duration of all content
		$available_time = 0;
		$viewed_time = 0;
		foreach ( $allPoints as $point )
		{
			if(isset($point->content_length))
			{
				$available_time += $point->content_length;
			}
		}

		foreach ( $pointsViewed as $point )
		{
			$viewed_time += $point->duration;
		}

		// if we don't have any content duration returned from allpoints
		// TODO double check this still makes sense once we get from authoring tool
		if($available_time <=0)
		{
			$available_time =$viewed_time;
		}


		$percentage = ($viewed_time / $available_time) * 100;

		return round( $percentage );
	}

	static $animals = array (
			'fish' => array (
					'AvT' => '240',
					'PpV' => '80',
					'PcC' => '60'
			),
			'sheep' => array (
					'AvT' => '350',
					'PpV' => '50',
					'PcC' => '90'
			),
			'bird' => array (
					'AvT' => '120',
					'PpV' => '20',
					'PcC' => '5'
			),
			'iguana' => array (
					'AvT' => '20',
					'PpV' => '100',
					'PcC' => '100'
			),
			'panda' => array (
					'AvT' => '800',
					'PpV' => '90',
					'PcC' => '50'
			)
	);

	/**
	 * Get a users visit based on their passcode, calculate a series of variables an pass them to the function
	 * to return the users animal.
	 *
	 * @param unknown $passcode
	 * @return string identifying their animal
	 */
	public static function getAnimalByPasscode($passcode)
	{
		// Get all available points at the exhibition
		$allPoints = POIModel::getPOIsByPasscode($passcode);

		// Get all pointed visited at the exhibition
		$pointsViewed = VisitLogModel::getVisitPointsByPasscode( $passcode );

		if(isset($pointsViewed)){
			$average = VisitLogModel::getAverageTimeViewedPoint( $pointsViewed );
			$percentage_visited = VisitLogModel::getPercentageOfPointsVisited( $pointsViewed, $allPoints );
			$percentage_content_consumed = VisitLogModel::getPercentageOfContentConsumed( $pointsViewed, $allPoints );

			$animal = VisitLogModel::getAnimal( array (
					$average,
					$percentage_visited,
					$percentage_content_consumed
			) );

			return $animal;
		}
	}

	/**
	 * Accepts an array of user values and extracts their closest match to an animal based on the static array $animals
	 *
	 * @param array(average time per point, $percentage points visisted, percentage content consumed) $user_values
	 * @return string $user_path
	 */
	public static function getAnimal($user_values)
	{
		// Get an array of animal names
		$animal_names = array_keys( self::$animals );

		// Initialise an array to store the differences b/w the user and each animal to an array
		$animal_user_diffs = array ();

		// Get an array of how different the user is to each of the animals
		// foreach animal
		foreach ( self::$animals as $key => $animal )
		{
			$animal_values = array_values( $animal );

			// foreach column
			for($i = 0; $i < count( $user_values ); $i ++)
			{
				// get the absolute difference between the user and the animal
				$animal_user_diffs [$key] [] = abs( $animal_values [$i] - $user_values [$i] );
			}
		}
		// print_r( $animal_user_diffs );

		// foreach diff
		$closest_animals_per_coulmn = array ();

		// for each column value
		for($i = 0; $i < count( $user_values ); $i ++)
		{
			// foreach animal
			unset( $min_amount );
			foreach ( $animal_names as $animal )
			{
				if (isset( $min_amount ))
				{
					if ($min_amount > $animal_user_diffs [$animal] [$i])
					{
						$min_amount = $animal_user_diffs [$animal] [$i];
						$min_animal = $animal;
					}
				} else
				{

					$min_amount = $animal_user_diffs [$animal] [$i];
					$min_animal = $animal;
				}
			}
			$closest_animals_per_coulmn [] = $min_animal;
		}

		$final_animal = array ();
		// left with one array listing your nearest animal per column
		foreach ( $closest_animals_per_coulmn as $key => $value )
		{
			if (! isset( $final_animal [$value] ))
			{
				$final_animal [$value] = 1;
			}
			$final_animal [$value] ++;
		}

		// get key with highest index value from array
		$closest_animal = array_keys( $final_animal, max( $final_animal ) )[0];

		return $closest_animal;
	}
}
