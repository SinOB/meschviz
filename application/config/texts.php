<?php

/**
 * Texts used in the application.
 * These texts are used via Text::get('FEEDBACK_USERNAME_ALREADY_TAKEN').
 * Could be extended to i18n etc.
 */
return array(
	"FEEDBACK_UPDATING" => _("Site update in progress. Some (or all) site functionality may be unavailable while this message is displayed."),
	"FEEDBACK_UNKNOWN_ERROR" => _("Unknown error occurred!"),
	"FEEDBACK_PASSWORD_WRONG_3_TIMES" => _("You have typed in a wrong password 3 or more times already. Please wait 30 seconds to try again."),
	"FEEDBACK_ACCOUNT_NOT_ACTIVATED_YET" => _("Your account is not activated yet. Please click on the confirm link in the mail."),
	"FEEDBACK_PASSWORD_WRONG" => _("Password was wrong."),
	"FEEDBACK_USER_DOES_NOT_EXIST" => _("This user does not exist."),
	"FEEDBACK_LOGIN_FAILED" => _("Login failed."),
	"FEEDBACK_USERNAME_FIELD_EMPTY" => _("Username field was empty."),
	"FEEDBACK_PASSWORD_FIELD_EMPTY" => _("Password field was empty."),
	"FEEDBACK_USERNAME_OR_PASSWORD_FIELD_EMPTY" => _("Username or password field was empty."),
	"FEEDBACK_USERNAME_EMAIL_FIELD_EMPTY" => _("Username / email field was empty."),
	"FEEDBACK_EMAIL_FIELD_EMPTY" => _("Email field was empty."),
	"FEEDBACK_EMAIL_AND_PASSWORD_FIELDS_EMPTY" => _("Email and password fields were empty."),
	"FEEDBACK_USERNAME_SAME_AS_OLD_ONE" => _("Sorry, that username is the same as your current one. Please choose another one."),
	"FEEDBACK_USERNAME_ALREADY_TAKEN" => _("Sorry, that username is already taken. Please choose another one."),
	"FEEDBACK_USER_EMAIL_ALREADY_TAKEN" => _("Sorry, that email is already in use. Please choose another one."),
	"FEEDBACK_USERNAME_CHANGE_SUCCESSFUL" => _("Your username has been changed successfully."),
	"FEEDBACK_USERNAME_AND_PASSWORD_FIELD_EMPTY" => _("Username and password fields were empty."),
	"FEEDBACK_USERNAME_DOES_NOT_FIT_PATTERN" => _("Username does not fit the name pattern: only a-Z and numbers are allowed, 2 to 64 characters."),
	"FEEDBACK_EMAIL_DOES_NOT_FIT_PATTERN" => _("Sorry, your chosen email does not fit into the email naming pattern."),
	"FEEDBACK_EMAIL_SAME_AS_OLD_ONE" => _("Sorry, that email address is the same as your current one. Please choose another one."),
	"FEEDBACK_EMAIL_CHANGE_SUCCESSFUL" => _("Your email address has been changed successfully."),
	"FEEDBACK_CAPTCHA_WRONG" => _("The entered captcha security characters were wrong."),
	"FEEDBACK_PASSWORD_REPEAT_WRONG" => _("Password and password repeat are not the same."),
	"FEEDBACK_PASSWORD_TOO_SHORT" => _("Password has a minimum length of 6 characters."),
	"FEEDBACK_USERNAME_TOO_SHORT_OR_TOO_LONG" => _("Username cannot be shorter than 2 or longer than 64 characters."),
	"FEEDBACK_ACCOUNT_SUCCESSFULLY_CREATED" => _("Your account has been created successfully and we have sent you an email. Please click the VERIFICATION LINK within that mail."),
	"FEEDBACK_VERIFICATION_MAIL_SENDING_FAILED" => _("Sorry, we could not send you an verification mail. Your account has NOT been created."),
	"FEEDBACK_ACCOUNT_CREATION_FAILED" => _("Sorry, your registration failed. Please go back and try again."),
	"FEEDBACK_VERIFICATION_MAIL_SENDING_ERROR" => _("Verification mail could not be sent due to: "),
	"FEEDBACK_VERIFICATION_MAIL_SENDING_SUCCESSFUL" => _("A verification mail has been sent successfully."),
	"FEEDBACK_ACCOUNT_ACTIVATION_SUCCESSFUL" => _("Activation was successful! You can now log in."),
	"FEEDBACK_ACCOUNT_ACTIVATION_FAILED" => _("Sorry, no such id/verification code combination here..."),
	"FEEDBACK_AVATAR_UPLOAD_SUCCESSFUL" => _("Avatar upload was successful."),
	"FEEDBACK_AVATAR_UPLOAD_WRONG_TYPE" => _("Only JPEG and PNG files are supported."),
	"FEEDBACK_AVATAR_UPLOAD_TOO_SMALL" => _("Avatar source file's width/height is too small. Needs to be 100x100 pixel minimum."),
	"FEEDBACK_AVATAR_UPLOAD_TOO_BIG" => _("Avatar source file is too big. 5 Megabyte is the maximum."),
	"FEEDBACK_AVATAR_FOLDER_DOES_NOT_EXIST_OR_NOT_WRITABLE" => _("Avatar folder does not exist or is not writable. Please change this via chmod 775 or 777."),
	"FEEDBACK_AVATAR_IMAGE_UPLOAD_FAILED" => _("Something went wrong with the image upload."),
	"FEEDBACK_AVATAR_IMAGE_DELETE_SUCCESSFUL" => _("You successfully deleted your avatar."),
    "FEEDBACK_AVATAR_IMAGE_DELETE_NO_FILE" => _("You don't have a custom avatar."),
    "FEEDBACK_AVATAR_IMAGE_DELETE_FAILED" => _("Something went wrong while deleting your avatar."),
	"FEEDBACK_FILE_FOLDER_DOES_NOT_EXIST_OR_NOT_WRITABLE" => _("File folder does not exist or is not writable."),
	"FEEDBACK_FILE_IMAGE_UPLOAD_WRONG_TYPE" => _("Invalid file format. Only the following file types allowed; .gif, .jpg, .jpeg, .png."),
	"FEEDBACK_FILE_IMAGE_UPLOAD_TOO_BIG" => _("Invalid file. Must be smaller than 5MB."),
	"FEEDBACK_FILE_IMAGE_UPLOAD_FAILED" => _("Image file upload failed."),
	"FEEDBACK_PASSCODE_DELETION_FAILED" => _("Passcode deletion failed."),
	"FEEDBACK_PASSWORD_RESET_TOKEN_FAIL" => _("Could not write token to database."),
	"FEEDBACK_PASSWORD_RESET_TOKEN_MISSING" => _("No password reset token."),
	"FEEDBACK_PASSWORD_RESET_MAIL_SENDING_ERROR" => _("Password reset mail could not be sent due to: "),
	"FEEDBACK_PASSWORD_RESET_MAIL_SENDING_SUCCESSFUL" => _("A password reset mail has been sent successfully."),
	"FEEDBACK_PASSWORD_RESET_LINK_EXPIRED" => _("Your reset link has expired. Please use the reset link within one hour."),
	"FEEDBACK_PASSWORD_RESET_COMBINATION_DOES_NOT_EXIST" => _("Username/Verification code combination does not exist."),
	"FEEDBACK_PASSWORD_RESET_LINK_VALID" => _("Password reset validation link is valid. Please change the password now."),
	"FEEDBACK_PASSWORD_CHANGE_SUCCESSFUL" => _("Password successfully changed."),
	"FEEDBACK_PASSWORD_CHANGE_FAILED" => _("Sorry, your password changing failed."),
	"FEEDBACK_ACCOUNT_TYPE_CHANGE_SUCCESSFUL" => _("Account type change successful"),
	"FEEDBACK_ACCOUNT_TYPE_CHANGE_FAILED" => _("Account type change failed"),
	"FEEDBACK_MAGAZINE_CREATION_FAILED" => _("Magazine creation failed."),
	"FEEDBACK_MAGAZINE_EDITING_FAILED" => _("Magazine editing failed."),
	"FEEDBACK_MAGAZINE_DELETION_FAILED" => _("Magazine deletion failed."),
	"FEEDBACK_MAP_CREATION_FAILED" => _("Map creation failed"),
	"FEEDBACK_MAP_EDITING_FAILED" => _("Map editing failed"),
	"FEEDBACK_MAP_DELETION_FAILED" => _("Map deletion failed"),
	"FEEDBACK_NEWS_CREATION_FAILED" => _("News item creation failed"),
	"FEEDBACK_NEWS_DELETION_FAILED" => _("News item deletion failed"),
	"FEEDBACK_TIMELINE_CREATION_FAILED" => _("Timeline creation failed."),
	"FEEDBACK_TIMELINE_EDITING_FAILED" => _("Timeline editing failed."),
	"FEEDBACK_TIMELINE_DELETION_FAILED" => _("Timeline deletion failed."),
	"FEEDBACK_TIMELINE_IMAGE_UPLOAD_FAILED" => _("Timeline image upload failed."),
	"FEEDBACK_TIMELINE_POINT_UPDATE_FAILED" => _("Timeline point update failed."),
	"FEEDBACK_TIMELINE_POINT_CREATION_FAILED"=>  _("Timeline point creation failed."),
	"FEEDBACK_TIMELINE_POINT_DELETION_FAILED" =>  _("Timeline point deletion failed."),
	"FEEDBACK_ARTICLE_CREATION_FAILED" => _("Article creation failed."),
	"FEEDBACK_ARTICLE_EDITING_FAILED" => _("Article editing failed."),
	"FEEDBACK_ARTICLE_DELETION_FAILED" => _("Article deletion failed."),
	"FEEDBACK_COMPONENT_CREATION_FAILED" => _("Component creation failed."),
	"FEEDBACK_COMPONENT_EDITING_FAILED" => _("Component editing failed."),
	"FEEDBACK_COMPONENT_DELETION_FAILED" => _("Component deletion failed."),
	"FEEDBACK_COMPONENT_SOURCE_INVALID" => _("Component source type is invalid"),
	"FEEDBACK_PASSCODE_CREATION_FAILED" => _("Passcode addition failed."),
	"FEEDBACK_PASSCODE_CREATION_INVALID_FAILED" => _("Passcode addition failed. Passcode is not in the correct format (ABC-123-1234) or passcode does not exist."),
	"FEEDBACK_PASSCODE_EDITING_FAILED" => _("Passcode editing failed."),
	"FEEDBACK_PASSCODE_DELETION_FAILED" => _("Passcode deletion failed."),
	"FEEDBACK_PASSCODE_INVALID" => _("Failed to locate passcode. Passcode is not in the correct format (ABC-123-1234) or passcode does not exist."),
	"FEEDBACK_COOKIE_INVALID" => _("Your remember-me-cookie is invalid."),
	"FEEDBACK_COOKIE_LOGIN_SUCCESSFUL" => _("You were successfully logged in via the remember-me-cookie."),
	"FEEDBACK_USER_EDITING_FAILED" => _("Unable to update user access level and/or museum access."),
	"FEEDBACK_GRID_CREATION_FAILED" => _("Grid visualisation creation failed"),
	"FEEDBACK_GRID_EDITING_FAILED" => _("Grid visualisation editing failed"),
	"FEEDBACK_GRID_DELETION_FAILED" => _("Grid visualisation deleting failed"),
	"FEEDBACK_GRID_IMAGE_CREATION_FAILED" => _("Adding an image to the grid visualisation failed"),
	"FEEDBACK_GRID_IMAGE_DELETION_FAILED" => _("Deleting an image from the grid visualisation failed"),
	"FEEDBACK_INSUFFICIENT_GRID_IMAGES" => _("You must add at least 12 images for this visualisation to work properly. This warning will disappear once you have added enough images."),
	"FEEDBACK_PASSOCDE_RANDOM_ALLOCATED" => _("Visualisation generated with random passcode as follows: "),
	"FEEDBACK_PASSOCDE_UNAVAILABLE" => _("No passcode for this exhibition has been found."),
	"FEEDBACK_EUROPEANA_API_DOWN" => _("Europeana currently unavailable. Please try again later."),
	"FEEDBACK_VISIT_LOG_FETCH_FAILED" => _("No visit log received. Check if log API is down"),
	"FEEDBACK_VISIT_SESSION_FETCH_FAILED" => _("No visit session received. Check if log API is down"),
	"FEEDBACK_EXHIBITION_DISPLAY_UPDATE_FAILED" => _("Failed to update displayed exhibition visualisations"),

	//"FEEDBACK_FACEBOOK_LOGIN_NOT_REGISTERED" => _("Sorry, you don't have an account here. Please register first."),
	//"FEEDBACK_FACEBOOK_EMAIL_NEEDED" => _("Sorry, but you need to allow us to see your email address to register."),
	//"FEEDBACK_FACEBOOK_UID_ALREADY_EXISTS" => _("Sorry, but you have already registered here (your Facebook ID exists in our database)."),
	//"FEEDBACK_FACEBOOK_EMAIL_ALREADY_EXISTS" => _("Sorry, but you have already registered here (your Facebook email exists in our database)."),
	//"FEEDBACK_FACEBOOK_USERNAME_ALREADY_EXISTS" => _("Sorry, but you have already registered here (your Facebook username exists in our database)."),
	//"FEEDBACK_FACEBOOK_REGISTER_SUCCESSFUL" => _("You have been successfully registered with Facebook."),
	//"FEEDBACK_FACEBOOK_OFFLINE" => _("We could not reach the Facebook servers. Maybe Facebook is offline (that really happens sometimes)."),
);