<?php
/**
 * The news controller: Pulls news from news table
*/
class NewsController extends Controller {

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct()
	{
		parent::__construct();

		// VERY IMPORTANT: All controllers/areas that should only be usable by logged-in users
		// need this line! Otherwise not-logged in users could do actions. If all of your pages should only
		// be usable by logged-in users: Put this line into libs/Controller->__construct
		Auth::checkAuthentication();
	}

	/**
	 * Get all news items for this users from database and pass them to page
	 */
	public function index() {

		$this->View->render ( 'news/index', array (
				'exhibitions' => ExhibitionModel::getExhibitionsByAdmin(),
				'news' => NewsModel::getNews(),
				)
		);
	}

	/**
	 * Create a news item
	 * @return unknown
	 */
	public function create()
	{
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$component_id = NewsModel::create(
				Request::post('exhibition_id'),
				Request::post('title'),
				Request::post('text'),
				Request::post('start'),
				Request::post('end')
		);

		Redirect::to('news/index/');
	}

	/**
	 * Delete a news item
	 * @param unknown $news_id
	 */
	public function delete($news_id) {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		NewsModel::delete ( $news_id );
		Redirect::to ( 'news/index' );
	}

}