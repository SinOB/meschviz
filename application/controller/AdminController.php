<?php

class AdminController extends Controller
{
	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct()
	{
		parent::__construct();
		// special authentication check for the entire controller: Note the check-ADMIN-authentication!
		// All methods inside this controller are only accessible for admins
		Auth::checkAdminAuthentication();
	}
	/**
	 * This method controls what happens when you move to /admin or /admin/index in your app.
	 */
	public function index()
	{
		$this->View->render('admin/index', array(
				'users' => UserModel::getPublicProfilesOfAllUsers(),
				'museums' => MuseumModel::getMuseums(),
				'user_account_types'=> UserModel::getUserAccountTypes(),
			)
		);
	}

	/**
	 * Update the account for one specific user - change account type and user museum_id
	 */
	public function updateAccountSettings()
	{
		if(!UserModel::isAdmin()) {
			return;
		}
		AdminModel::updateUserAccount(
				Request::post ( 'user_id' ),
				Request::post ( 'user_account_type_id' ),
				Request::post ( 'museum_id' ));
		Redirect::to("admin");
	}
}
