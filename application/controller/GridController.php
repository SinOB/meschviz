<?php

/**
 * The grid visualisation controller:
 */
class GridController extends Controller {
	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct() {
		parent::__construct ();
		Auth::checkAuthentication ();
	}

	/**
	 * This method controls what happens when you move to /grid/index in your app.
	 * Gets all grids (of the user).
	 */
	public function index() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$grids = GridModel::getUserGrids();

		$this->View->render ( 'grid/index', array (
				'grids' => $grids,
				'exhibitions'=> ExhibitionModel::getExhibitionsByAdmin()
		) );
	}

	public function pop($page, $passcode)
	{
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->renderWithoutHeaderAndFooter('grid/grid/'.$page, array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition,
		) );
	}

	/**
	 * This method controls what happens when you move to /grid/create in your app.
	 * Creates a new grid visualisation. This is usually the target of form submit actions.
	 * POST request.
	 */
	public function create() {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		GridModel::create( Request::post ( 'title' ), Request::post ( 'exhibition_id' ));
		Redirect::to ( 'grid' );
	}

	/**
	 * This method controls what happens when you move to /grid/edit(/XX) in your app.
	 * Shows the current content of the grid and an editing form.
	 *
	 * @param $grid_id int id of the grid
	 */
	public function edit($grid_id) {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		if(!isset($grid_id)){
			Redirect::home();
		}
		$images = GridModel::getGridImages ( $grid_id );
		if(count($images) < 12){
			Session::add( 'feedback_warning', Text::get( 'FEEDBACK_INSUFFICIENT_GRID_IMAGES' ) );
		}

		$grid = GridModel::getGrid ( $grid_id );
		$context = array (
				'grid' => $grid,
				'images' => $images,
				'licenses'=> LicenseModel::getLicenses(),
				'exhibition'=>ExhibitionModel::getExhibition($grid->exhibition_id)
		);

		$this->View->render ( 'grid/edit', $context );
	}

	/**
	 * Displays the grid visualisation for the user.
	 * If regular user and has passcode will use this passcode
	 * If curator user previewing visualisation with no passcode will try to get random passcode from database.
	 * If no passcode supplied - page will render with warning and passcode dependent links will not function.
	 *
	 * @param $grid_id int id of the grid visualisation
	 * @param string $passcode alphanumeric string prepresenting visit log
	 */
	public function view($grid_id, $passcode=null) {

		$grid = GridModel::getGrid ( $grid_id );
		$context = array (
				'grid' => $grid,
				'images'=>GridModel::getGridImages($grid_id)
		);

		// If we are a curator and have no passcode - probably attempting a visualisation preview
		if(!isset($passcode) && UserModel::isCurator())
		{
			// try to get random passcode from database
			$result = PasscodeModel::getRandomPasscode($grid->exhibition_id);

			if(isset($result->passcode)){
				$passcode=$result->passcode;
				// Warn user that using randomly generated passcode
				Session::add( 'feedback_positive', Text::get( 'FEEDBACK_PASSOCDE_RANDOM_ALLOCATED' ).$passcode );
			}else{
				// If still have no passcode in database warn viewer that visualisation will not yet work as expected
				Session::add( 'feedback_negative', Text::get( 'FEEDBACK_PASSOCDE_UNAVAILABLE' ) );
			}
		}

		// Get the visit info for the passcode
		if(isset($passcode))
		{
			$context['passcode'] = $passcode;
			$context['exhibition'] = ExhibitionModel::getExhibitionByPasscode($passcode);
		}

		$this->View->render ( 'grid/view', $context );
	}

	/**
	 * This method controls what happens when you move to editSave in your app.
	 * Edits a grid visualisation
	 */
	public function editSave() {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		GridModel::updateGrid ( Request::post ( 'grid_id' ), Request::post ( 'title' ), Request::post ( 'status' ) );
		Redirect::to ( 'grid/edit/' . Request::post ( 'grid_id' ) );
	}

	public function addImage()
	{
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		GridModel::addImage(Request::post ( 'grid_id' ), Request::post ( 'url' ),Request::post ( 'license_id' ));
		Redirect::to ( 'grid/edit/' . Request::post ( 'grid_id' ) );
	}

	/**
	 * This method controls what happens when you move to /grid/delete(/XX) in your app.
	 * Deletes a grid and its grid_images
	 *
	 * @param int $grid_id id of the grid
	 */
	public function delete($grid_id) {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		GridModel::delete ( $grid_id );
		Redirect::to ( 'grid' );
	}

	public function imageDelete( $image_id, $grid_id){
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		GridModel::deleteImage ($image_id, $grid_id );
		Redirect::to ( 'grid/edit/' . $grid_id );

	}
}
