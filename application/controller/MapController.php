<?php

/**
 * The map controller:
 */
class MapController extends Controller {

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct() {
		parent::__construct ();
		Auth::checkAuthentication ();
	}

	/**
	 * This method controls what happens when you move to /map/index in your app.
	 * For curator should display list of the maps of all status
	 */
	public function index() {

		if(!UserModel::isCurator()){
			return;
		}

		$this->View->render ( 'map/index', array (
				'maps' => MapModel::getMaps(),
				'exhibitions'=> ExhibitionModel::getExhibitionsByAdmin()
		) );
	}

	/**
	 * This method controls what happens when you move to /map/create in your app.
	 * Creates a new map object
	 */
	public function create() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		MapModel::create (
				Request::post( 'exhibition_id' ),
				Request::post( 'title' ),
				$_FILES);

		Redirect::to ( 'map/index/');
	}

	/**
	 * Controls what happens when go to /map/edit/<map_id>
	 *
	 * @param unknown $map_id
	 */
	public function edit($map_id) {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$map = MapModel::getMap ( $map_id );

		$context = array (
				'map' => $map,
				'exhibition'=>ExhibitionModel::getExhibition($map->exhibition_id),
				'content'=> ContentModel::GetContentByExhibition($map->exhibition_id)
		);

		$this->View->render ( 'map/edit', $context );
	}

	/**
	 * Controls what happens when go to /map/view/<map_id>
	 *
	 * @param unknown $map_id
	 */
	public function view($map_id) {
		$map = MapModel::getMap ( $map_id );
		$passcode = Request::get ( 'passcode' );

		// If we are a curator and have no passcode - probably attempting a visualisation preview
		if(!isset($passcode) && UserModel::isCurator())
		{
			// try to get random passcode from database
			$result = PasscodeModel::getRandomPasscode($map->exhibition_id);

			if(isset($result->passcode)){
				$passcode=$result->passcode;
				// Warn user that using randomly generated passcode
				Session::add( 'feedback_positive', Text::get( 'FEEDBACK_PASSOCDE_RANDOM_ALLOCATED' ).$passcode );
			}else{
				// If still have no passcode in database warn viewer that visualisation will not yet work as expected
				Session::add( 'feedback_negative', Text::get( 'FEEDBACK_PASSOCDE_UNAVAILABLE' ) );
			}
		}

		$context = array (
				'map' => $map,
				'exhibition'=>ExhibitionModel::getExhibition($map->exhibition_id)
		);

		if(isset($passcode)) {

			$context['passcode'] = $passcode;
			$context['points'] = VisitLogModel::getVisitPointsByPasscode($passcode);
		}

		$this->View->render ( 'map/view', $context );
	}

	/**
	 * Controls what happens when update a map
	 */
	public function update() {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		MapModel::update( Request::post ( 'map_id' ),
				 Request::post ( 'title' ),
				 Request::post ( 'status' ),
				 Request::post ( 'point_data' ));

		Redirect::to ( 'map/edit/'.Request::post ( 'map_id' ));
	}

	/**
	 * This method controls what happens when you move to /map/delete(/XX) in your app.
	 * Deletes a map object from database and its image from the file system
	 *
	 * @param int $timeline_id id of the timeline
	 */
	public function delete($map_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		MapModel::delete ( $map_id );
		Redirect::to ('map/index/' );
	}


}
