<?php
class ExhibitionController extends Controller
{

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * This method controls what happens when you move to /exhibition/index in your app.
	 * Gets all passcodes (of the user).
	 */
	public function index() {

		// only allw to view all exhibitions if the user is logged in
		if (!Session::userIsLoggedIn()) {
			Redirect::home();
		}

		$this->View->render ( 'exhibition/index', array (
				'passcodes' => PasscodeModel::getUserPasscodes (),
				'exhibitions' => ExhibitionModel::getExhibitionsByUser()
		) );
	}

	/**
	 * This method controls what happens when you move to /exhibition/view(/XX) in your app.
	 * Shows the current content of the magazine and an editing form.
	 *
	 * @param $exhibition_id int id of the magazine
	 */
	public function view($exhibition_id) {

		// Only allw to view all exhibitions if the user is logged in
		if (!Session::userIsLoggedIn()) {
			Redirect::home();
		}

		// If missing exhibition id return to home page
		if(!isset($exhibition_id) || count($exhibition_id) <=0) {
			Redirect::home();
		}

		// If user has no passcodes for this exhibition they shoud not be here - send them home
		$passcodes = PasscodeModel::getUserPasscodesByExhibition($exhibition_id);
		if(count($passcodes)<=0){
			Redirect::home();
		}

		// Get front images for magazines
		$magazines = MagazineModel::getMagazinesByExhibition($exhibition_id);
		foreach($magazines as $magazine)
		{
			$val = MagazineModel::getMagazineFrontImages($magazine->magazine_id);
			if(isset($val) && count($val)>=1){
				$magazine->front_images = $val;
			}
		}

		$this->View->render ( 'exhibition/view', array (
				'exhibition'=>ExhibitionModel::getExhibition($exhibition_id),
				'passcodes' => $passcodes,
				'magazines' => $magazines,
				'historical_timelines' => TimelineModel::getHistoricalTimelinesByExhibition($exhibition_id),
				'user_visit_timelines' => TimelineModel::getUserVisitTimelinesByExhibition($exhibition_id),
				'news' => NewsModel::getNewsByExhibition($exhibition_id),
		) );
	}

	/**
	 * View public - non logged in version of exhibition page
	 */
	public function view_public()
	{
		$passcode_id = Request::get ( 'passcode' );

		if(!isset($passcode_id) || count($passcode_id) ==0){
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_INVALID' ) );
			Redirect::to('login/index');
			return ;
		}

		// Check if passcode is in a valid format
		if(!PasscodeModel::isValidPasscode($passcode_id)){
			error_log ("2");
			Redirect::to('login/index');
			return;
		}

		// Set passcode on session so can pull out if user decides to register
		Session::set ('passcode', $passcode_id);

		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode_id);

		// If the exhibition does not exist return to login page
		if(!isset($exhibition)){
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_INVALID' ) );
			Redirect::to('login/index');
			return;
		}

		// Call the API to test that a session exists for this passcode
		$visit_session = VisitLogModel::getVisitSessionByPasscode($passcode_id);
		if(!isset($visit_session) || count($visit_session)==0)
		{
			Session::add ( 'feedback_negative', Text::get ( 'FEEDBACK_PASSCODE_INVALID' ) );
			Redirect::to('login/index');
			return;
		}

		$magazines = MagazineModel::getMagazinesByExhibition($exhibition->exhibition_id);

		foreach($magazines as $magazine)
		{
			$val = MagazineModel::getMagazineFrontImages($magazine->magazine_id);
			if(isset($val) && count($val)>=1){
				$magazine->front_images = $val;
			}
		}

		// Make sure we record that a passcode was used with no user id
		PasscodeModel::createPasscode($passcode_id, 0);

		$this->View->render ( 'exhibition/view', array (
				'exhibition'=>ExhibitionModel::getExhibition($exhibition->exhibition_id),
				'passcodes' => array((object)['passcode'=>$passcode_id]),
				'magazines' => $magazines,
				'grids' => GridModel::getGridsByExhibition($exhibition->exhibition_id),
				'historical_timelines' => TimelineModel::getHistoricalTimelinesByExhibition($exhibition->exhibition_id),
				'user_visit_timelines' => TimelineModel::getUserVisitTimelinesByExhibition($exhibition->exhibition_id),
				'news' => NewsModel::getNewsByExhibition($exhibition->exhibition_id),
				'maps' => MapModel::getMapsByExhibition($exhibition->exhibition_id),
		) );
	}

	/**
	 * Display page to allow curator to modify display permissions for the static visualisations
	 * @param unknown $exhibition_id
	 */
	public function edit($exhibition_id){
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$this->View->render ( 'exhibition/edit', array (
				'exhibition'=>ExhibitionModel::getExhibition($exhibition_id),
		) );
	}


	public function updateVisualisationDisplaySettings()
	{
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$exhibition_id = Request::post ( 'exhibition_id' );
		$display = Request::post ( 'visualisation_permission' );

		ExhibitionModel::updateVisualisationDisplaySettings ($exhibition_id, $display);
		Redirect::to ( 'exhibition/edit/' . $exhibition_id);
	}
}
