<?php


/**
 * The Visualisation controller: Get visualisation info based on supplied passcode
 */
class ContentController extends Controller
{
	public function display_by_tags($exhibition_id, $audience, $language, $perspective, $point_of_interest)
	{
		$content = ContentModel::getContentWorkaround($exhibition_id, $audience, $language, $perspective, $point_of_interest);

		$this->View->renderWithoutHeaderAndFooter('visual/content', array (
				'content'=>$content
		) );
	}

	public function display($uid, $exhibition_id)
	{
		$content = ContentModel::getContent($uid, $exhibition_id);

		$this->View->renderWithoutHeaderAndFooter('visual/content', array (
				'content'=>$content
		) );
	}

}