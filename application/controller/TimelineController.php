<?php

class TimelineController extends Controller {

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct() {
		parent::__construct ();
		Auth::checkAuthentication ();
	}

	/**
	 * This method controls what happens when you move to /timeline/index in your app.
	 * Gets all timelines (of the user).
	 */
	public function index() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$timelines = TimelineModel::getUserTimelines();

		$this->View->render ( 'timeline/index', array (
				'timelines' => $timelines,
				'exhibitions'=> ExhibitionModel::getExhibitionsByAdmin()
		) );
	}

	/**
	 * This method controls what happens when you move to /timeline/view(/XX) in your app.
	 * Shows the current content of the timeline and an editing form.
	 *
	 * @param $timeline_id int id of the timeline
	 */
	public function view($timeline_id, $passcode=null) {

		$context = array (
				'timeline' => TimelineModel::getTimeline ( $timeline_id ),
				'points'=>PointModel::getTimelinePoints($timeline_id)
		);

		// Get the visit info for the passcode
		if(isset($passcode))
		{
			$logInfoViewed = VisitLogModel::getVisitPointsByPasscode( $passcode );
			$context['visit']=$logInfoViewed;
		}

		$this->View->render ( 'timeline/view', $context);
	}

	/**
	 * This method controls what happens when you move to /timeline/edit(/XX) in your app.
	 * Shows the current content of the timeline and an editing form.
	 *
	 * @param $timeline_id int id of the timeline
	 */
	public function edit($timeline_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$timeline=TimelineModel::getTimeline ( $timeline_id );
		$context = array (
				'timeline' => $timeline,
				'exhibitions'=> ExhibitionModel::getExhibitionsByAdmin(),
				'points'=>PointModel::getTimelinePoints($timeline_id),
				'exhibition'=>ExhibitionModel::getExhibition($timeline->exhibition_id)
		);

		$this->View->render ( 'timeline/edit', $context );
	}

	/**
	 * This method controls what happens when you move to /timeline/editSave in your app.
	 * Edits a timeline (performs the editing after form submit).
	 * POST request.
	 */
	public function updateTimeline() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		TimelineModel::updateTimeline (
				Request::post ( 'timeline_id' ),
				Request::post ( 'title' ),
				Request::post ( 'timeline_subtitle' ),
				Request::post ( 'timeline_type' ),
				Request::post ( 'timeline_status' )
				);
		Redirect::to ( 'timeline/edit/' . Request::post ( 'timeline_id' ) );
	}

	public function updateTitleImageFile()
	{
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		TimelineModel::updateTitleImageFile(Request::post ( 'timeline_id' ), $_FILES);
		Redirect::to ( 'timeline/edit/' . Request::post ( 'timeline_id' ) );
	}

	public function updateTimelineImageFile()
	{
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		TimelineModel::updateTimelineImageFile(Request::post ( 'timeline_id' ), $_FILES);
		Redirect::to ( 'timeline/edit/' . Request::post ( 'timeline_id' ) );
	}


	/**
	 * This method controls what happens when you move to /timeline/create in your app.
	 * Creates a new timeline. This is usually the target of form submit actions.
	 * POST request.
	 */
	public function create() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		TimelineModel::create (
				Request::post ( 'timeline_title' ),
				Request::post ( 'timeline_subtitle' ),
				Request::post ( 'timeline_type' ),
				Request::post ( 'exhibition_id' ));
		Redirect::to ( 'timeline' );
	}

	/**
	 * This method controls what happens when you move to /timeline/delete(/XX) in your app.
	 * Deletes a timeline.
	 *
	 * @param int $timeline_id id of the timeline
	 */
	public function delete($timeline_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		TimelineModel::delete ( $timeline_id );
		Redirect::to ( 'timeline' );
	}

	public function deleteTitleImage($timeline_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		TimelineModel::deleteImage ( $timeline_id, 'title_image' );
		Redirect::to ( 'timeline/edit/' . $timeline_id );
	}
	public function deleteTimelineImage($timeline_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		TimelineModel::deleteImage ( $timeline_id, 'timeline_image' );
		Redirect::to ( 'timeline/edit/' . $timeline_id );
	}

}