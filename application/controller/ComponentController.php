<?php
class ComponentController extends Controller
{

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct()
	{
		parent::__construct();
		Auth::checkAuthentication();
	}

	/**
	 * This method controls what happens when you move to /article/create in your app.
	 * Creates a new component. This is usually the target of form submit actions.
	 * POST request.
	 */
	public function create()
	{
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$text = Request::post('component_text');
		$component_id = ComponentModel::createComponent(Request::post('article_id'),
				Request::post('source_id'),
				Request::post('source_type'),
				Request::post('component_text'),
				Request::post('external_source'),
				Request::post('license_id')
				);

		// if we are saving a text type or an external source - return to article
		if(Request::post('source_type')=='text' || Request::post('source_type')=='external'){
			Redirect::to('article/edit/'.Request::post('article_id'));
		} else {
			return $component_id;
		}
	}

	public function upload()
	{
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		ComponentModel::uploadComponent(Request::post('article_id'),
				$_FILES,
				Request::post('license_id')
				);
		Redirect::to('article/edit/'.Request::post('article_id'));
	}

	/**
	 * Delete a component from an article
	 *
	 * @param unknown $component_id
	 * @param unknown $article_id
	 */
	public function delete()
	{
		$component_id = Request::post('delete_component_component_id');
		$article_id = Request::post('delete_component_article_id');

		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		ComponentModel::deleteComponent($component_id);
		Redirect::to('article/edit/'.$article_id);
	}
}