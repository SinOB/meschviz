<?php

/**
 * The Visualisation controller: Get visualisation info based on supplied passcode
 */
class VisualisationController extends Controller
{

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct()
	{

		parent::__construct();
	}

	/**
	 * This method controls what happens when you move to /visual/index in your app.
	 * Provides options to create visualisations.
	 */
	public function index() {
		Auth::checkAuthentication ();
		// Only allow access to Curator
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		$this->View->render ( 'visual/index', array (
				'exhibitions'=> ExhibitionModel::getExhibitionsByAdmin()
		) );
	}

	/**
	 * This method controls what happens when you move to /visualisation/path(/XX) in your app.
	 * Determine the animal path of the user based on the passcode.
	 */
	public function path($passcode)
	{
		Auth::checkAuthentication ();
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render( 'visual/path', array (
				'animal' => VisitLogModel::getAnimalByPasscode( $passcode ),
				'points' => VisitLogModel::getVisitPointsByPasscode($passcode),
				'exhibition'=>$exhibition,
				'content'=> ContentModel::GetContentByExhibition($exhibition->exhibition_id)
		) );
	}


	/**
	 * Allows the display of a face generated based on the passcode supplied.
	 * This function is accessable to non logged in users.
	 * @param unknown $passcode
	 */
	public function svgCharacter($passcode)
	{
		$this->View->renderWithoutHeaderAndFooter( 'visual/svgcharacter', array (
				'passcode'=>$passcode
		) );
	}


	/*
	 * Placeholder for visualisation - combination of exhibition points, perspectives and a character
	 */
	public function bubbles($passcode)
	{
		Auth::checkAuthentication ();
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render( 'visual/bubbles', array (
				'passcode'=>$passcode,
				'viewedPoints' => VisitLogModel::getVisitPointsByPasscode($passcode),
				'perspectives'=> PerspectiveModel::getPerspectivesByExhibition($exhibition->exhibition_id),
				'exhibition'=>$exhibition,
				'content'=> ContentModel::GetContentByExhibition($exhibition->exhibition_id)
		) );
	}

	public function photoperson($passcode)
	{
		Auth::checkAuthentication ();
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render( 'visual/photoperson', array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition
		) );
	}

	public function stamps($passcode)
	{
		Auth::checkAuthentication ();
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render( 'visual/stamps', array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition,
				'viewedPoints' => VisitLogModel::getVisitPointsByPasscode($passcode),
		) );

	}

	/**
	 * Renders a page displaying a text cloud from content viewed during the visit.
	 * @param unknown $passcode
	 */
	public function cloud($passcode)
	{
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->renderWithoutHeaderAndFooter( 'visual/cloud', array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition,
		) );
	}

	/*
	 * Renders a page containing a photo album of the visit.
	 */
	public function album($passcode)
	{
		Auth::checkAuthentication ();
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render( 'visual/album', array (
				'exhibition'=>$exhibition,
				'passcode'=>$passcode,
				'content'=> ContentModel::GetContentByExhibition($exhibition->exhibition_id),
				'viewedPoints' => VisitLogModel::getVisitPointsByPasscode($passcode),
		) );
	}

	public function unseen($passcode){
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render('visual/unseen', array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition,
		) );
	}

	public function favorite($passcode){
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render('visual/favorite', array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition,
		) );
	}
	public function inspiration($passcode){
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render('visual/inspiration', array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition,
		) );
	}

	public function personalisation($passcode){
		$exhibition = ExhibitionModel::getExhibitionByPasscode($passcode);
		$this->View->render('visual/personalisation', array (
				'passcode'=>$passcode,
				'exhibition'=>$exhibition,
		) );
	}
}
