<?php

class LanguageController extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function change()
	{

		$language = Request::post( 'lang' );
		LanguageModel::change( $language );

		Redirect::referrer();
	}
}