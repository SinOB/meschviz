<?php
/**
 * The article controller: Pulls articles from article table
 */
class ArticleController extends Controller {

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct() {
		parent::__construct ();
		Auth::checkAuthentication ();
	}

	/**
	 * This method controls what happens when you move to /article/create in your app.
	 * Creates a new article.
	 * POST request.
	 */
	public function create() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		ArticleModel::createArticle ( Request::post ( 'magazine_id' ), Request::post ( 'article_title' ) );
		Redirect::to ( 'magazine/edit/' . Request::post ( 'magazine_id' ) );
	}

	/**
	 * This method controls what happens when you move to /article/edit(/XX) in your app.
	 * Shows the current content of the article and an editing form. Also searches for
	 * source content.
	 *
	 * @param $article_id int id of the article
	 */
	public function edit($article_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$article = ArticleModel::getArticle ( $article_id );
		$components = ComponentModel::getComponentsByArticle ( $article_id );


		$context = array (
				'magazine' => MagazineModel::getMagazine ( $article->magazine_id ),
				'article' => $article,
				'components' => $components,
				'licenses'=> LicenseModel::getLicenses(),
		);

		// if we have a search term, use that instead
		if(null !==Request::post ( 'sources_search' ))
		{
			$context ['search_results'] = ComponentModel::searchComponents(
					Request::post ( 'sources_search' ),
					15,
					Request::post('language'),
					Request::post('reusability'));
		}
		else if(count($components)==0)
		{
			$context ['search_results'] = ComponentModel::searchComponents(
					$article->article_title,
					15,
					Request::post('language'),
					Request::post('reusability'));
		}

		$this->View->render ( 'article/edit', $context );
	}

	/**
	 * This method controls what happens when you move to /article/editSave in your app.
	 * Edits an article (performs the editing after form submit).
	 * POST request.
	 */
	public function editSave() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		ArticleModel::updateArticle ( Request::post ( 'article_id' ), Request::post ( 'article_title' ) );
		Redirect::to ( 'article/edit/' . Request::post ( 'article_id' ) );
	}

	/**
	 * This method controls what happens when you move to /article/delete(/XX)(/YYY) in your app.
	 * Deletes an article.
	 *
	 * @param int $article_id id of the magazine
	 * @param int $magazine_id id of the magazine
	 */
	public function delete($article_id, $magazine_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		ArticleModel::deleteArticle ( $article_id );
		Redirect::to ( 'magazine/edit/' . $magazine_id );
	}
}
