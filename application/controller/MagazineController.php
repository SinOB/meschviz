<?php

/* NB on delete need to delete magazine related articles and related components*/

/**
 * The magazine controller:
 */
class MagazineController extends Controller {
	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct() {
		parent::__construct ();
	}

	/**
	 * This method controls what happens when you move to /magazine/index in your app.
	 * For logged out user should redirect to login page.
	 * For curator should display list of their magazines of all statuses.
	 */
	public function index() {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$magazines = MagazineModel::getUserMagazines();
		foreach($magazines as $magazine)
		{
			$val = MagazineModel::getMagazineFrontImage($magazine->magazine_id);
			if(isset($val->external_source)){
				$magazine->front_image = $val->external_source;
			}
		}

		$this->View->render ( 'magazine/index', array (
				'magazines' => $magazines,
				'exhibitions'=> ExhibitionModel::getExhibitionsByAdmin()
		) );
	}

	/**
	 * This method controls what happens when you move to /magazine/create in your app.
	 * Creates a new magazine. This is usually the target of form submit actions.
	 * POST request.
	 */
	public function create() {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		MagazineModel::createMagazine ( Request::post ( 'magazine_text' ), Request::post ( 'exhibition_id' ));
		Redirect::to ( 'magazine' );
	}

	/**
	 * This method controls what happens when you move to /magazine/edit(/XX) in your app.
	 * Shows the current content of the magazine and an editing form.
	 *
	 * @param $magazine_id int id of the magazine
	 */
	public function edit($magazine_id) {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		$magazine = MagazineModel::getMagazine ( $magazine_id );

		$context = array (
				'magazine' => $magazine,
				'articles' => ArticleModel::getArticlesByMagazine ( $magazine_id ),
				'exhibition'=>ExhibitionModel::getExhibition($magazine->exhibition_id)
		);

		$this->View->render ( 'magazine/edit', $context );
	}

	/**
	 * This method controls what happens when you move to /magazine/view(/XX) in your app.
	 * Shows the current content of the magazine and an editing form.
	 *
	 * @param $magazine_id int id of the magazine
	 */
	public function view($magazine_id) {
		$magazine = MagazineModel::getMagazine ( $magazine_id );

		// If user not logged in can only view magazines with the status set to public
		if(!Session::userIsLoggedIn() && $magazine->status !=Config::get('PUBLIC_STATUS')){
			Redirect::home();
		}

		$articles = ArticleModel::getArticlesByMagazine ( $magazine_id );

		foreach ( $articles as $article ) {
			$components = ComponentModel::getComponentsByArticle ( $article->article_id );
			$article->components = $components;
		}

		$passcode = Request::get ( 'passcode' );

		$this->View->render ( 'magazine/view', array (
				'magazine' => $magazine,
				'articles' => $articles,
				'passcode' => isset($passcode)?$passcode:null
		) );
	}

	/**
	 * This method controls what happens when you move to /magazine/editSave in your app.
	 * Edits a magazine (performs the editing after form submit).
	 * POST request.
	 */
	public function editSave() {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		MagazineModel::updateMagazine ( Request::post ( 'magazine_id' ), Request::post ( 'magazine_title' ), Request::post ( 'status' ) );
		Redirect::to ( 'magazine/edit/' . Request::post ( 'magazine_id' ) );
	}

	/**
	 * This method controls what happens when you move to /magazine/delete(/XX) in your app.
	 * Deletes a magazine and its articles and components.
	 *
	 * @param int $magazine_id id of the magazine
	 */
	public function delete($magazine_id) {
		Auth::checkAuthentication ();

		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		MagazineModel::deleteMagazine ( $magazine_id );
		Redirect::to ( 'magazine' );
	}
}
