<?php

class PointController extends Controller {
	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct() {
		parent::__construct ();
		Auth::checkAuthentication ();
	}


	public function view($point_id)
	{
		$point = PointModel::getPoint ( $point_id );

		$context = array (
				'point' => $point,
				'licenses'=> LicenseModel::getLicenses(),
		);

		// if we have a search - perform the search
		if(null !==Request::post ( 'sources_search' ))
		{
			$context ['search_results'] = ComponentModel::searchComponents(
					Request::post ( 'sources_search' ),
					15,
					Request::post('language'),
					Request::post('reusability'));
		}

		$this->View->render ( 'timeline/point', $context);
	}

	/**
	 * This method controls what happens when you move to /point/create in your app.
	 * Creates a new timeline point.
	 */
	public function create() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		PointModel::create (
				Request::post( 'timeline_id' ),
				Request::post( 'date' ),
				Request::post( 'title' ),
				Request::post( 'position' ));

		Redirect::to ( 'timeline/edit/' . Request::post ( 'timeline_id' ) );
	}

	/**
	 * This method controls what happens when you move to /point/create in your app.
	 * Creates a new timeline point.
	 */
	public function update() {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		PointModel::update (
				Request::post( 'point_id' ),
				Request::post( 'timeline_id' ),
				Request::post( 'date' ),
				Request::post( 'title' ),
				Request::post( 'description' ),
				Request::post( 'position' ));

		Redirect::to ( 'timeline/edit/' . Request::post ( 'timeline_id' ) );

	}

	public function update_image()
	{
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		PointModel::updateImage(
				Request::post( 'point_id' ),
				Request::post( 'timeline_id' ),
				Request::post( 'source_type' ),
				Request::post( 'source_id' ),
				Request::post( 'external_source' ),
				Request::post( 'license_id' )
				);
		Redirect::to ( 'point/view/' . Request::post ( 'point_id' ) );
	}


	public function upload()
	{
		if(!UserModel::isCurator()) {
			Redirect::home();
		}

		PointModel::upload(Request::post('point_id'),
				Request::post( 'timeline_id' ),
				$_FILES,
				Request::post('license_id')
		);
		Redirect::to ( 'point/view/' . Request::post ( 'point_id' ) );
	}


	/**
	 * This method controls what happens when you move to /timeline/delete(/XX) in your app.
	 * Deletes a timeline.
	 *
	 * @param int $timeline_id id of the timeline
	 */
	public function delete($point_id, $timeline_id) {
		if(!UserModel::isCurator()) {
			Redirect::home();
		}
		PointModel::delete ( $point_id );
		Redirect::to ( 'timeline/edit/' . $timeline_id );
	}

}