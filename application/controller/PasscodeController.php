<?php
/**
* The passcode controller: Pulls passcodes from passcode table
*/
class PasscodeController extends Controller {

	/**
	 * Construct this object by extending the basic Controller class
	 */
	public function __construct() {
		parent::__construct ();

		Auth::checkAuthentication ();
	}



	/**
	 * This method controls what happens when you move to /passcode/create in your app.
	 * Adds a new passcode. This is usually the target of form submit actions.
	 * POST request.
	 */
	public function create() {
		PasscodeModel::createPasscode ( Request::post ( 'passcode' ) );
		Redirect::to ( 'exhibition' );
	}


	/**
	 * View all available passcodes. Should only be used by admin user to manage passcodes.
	 */
	public function index() {
		if(!UserModel::isAdmin()) {
			return;
		}

		$this->View->render('passcode/index', array(
				'passcodes' => PasscodeModel::getPasscodes(),
			)
		);
	}

	/**
	 * Delete a specific passcode. Should only be used by an admin user to manage passcodes.
	 * @param unknown $passcode_id
	 */
	public function delete($passcode_id){
		if(!UserModel::isAdmin()) {
			return;
		}

		PasscodeModel::delete($passcode_id);
		Redirect::to ( 'passcode' );
	}

}
